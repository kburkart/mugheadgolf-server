FROM gradle:6.8.3-jdk11 as build
WORKDIR /home/gradle/src
COPY . /home/gradle/src
RUN gradle build --no-daemon -x test

FROM openjdk:11-jre-slim
WORKDIR /home/gradle/src
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/*.jar /app/mugheadgolf-server.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/mugheadgolf-server.jar"]
CMD ["java -jar /app/mugheadgolf-server.jar"]
