# README #


### MugheadGolf-Server  ###

* This is the backend for the MugheadGolf League Website
* This will handle everything the league needs from scheduling, scoring stats email notices etc. 
* This project is built using Spring Boot, with Rest Api to deliver requests to a front end using JSON the backend Server is MySQL

### In progress - Still needed###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
* Junit Tests
* Exception Handling

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Contact ###

* Kevin Burkart
* kevin.burkart@gmail.com