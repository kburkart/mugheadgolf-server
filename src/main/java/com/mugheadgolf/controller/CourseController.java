package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.data.CourseData;
import com.mugheadgolf.model.Course;
import com.mugheadgolf.repository.CourseRepository;
import com.mugheadgolf.service.CourseService;


@RestController
@RequestMapping("course")
public class CourseController {

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private CourseService courseService;

	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<Course> list(){
		return courseRepository.findAll();
	}

	@RequestMapping(value="/{idcourse}" , method = RequestMethod.GET, produces = "application/json")
	public Course get(@PathVariable Integer idcourse){
		return courseRepository.getOne(idcourse);
	}

	@RequestMapping(value="/data/{idcourse}" , method = RequestMethod.GET, produces = "application/json")
	public CourseData getCourseData(@PathVariable Integer idcourse){
		return courseService.getCourseData(idcourse);
	}


}
