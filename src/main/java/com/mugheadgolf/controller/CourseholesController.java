package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Courseholes;
import com.mugheadgolf.repository.CourseRepository;
import com.mugheadgolf.repository.CourseholesRepository;


@RestController
@RequestMapping("courseholes")
public class CourseholesController {

	@Autowired
	private CourseholesRepository courseholesRepository;

	@Autowired
	private CourseRepository courseRepository;

	@RequestMapping(value="/courseholes" , method = RequestMethod.GET, produces = "application/json")
	public List<Courseholes> list(){
		return courseholesRepository.findAll();
	}

	@RequestMapping(value="/{idcourse}" , method = RequestMethod.GET, produces = "application/json")
	public List<Courseholes> get(@PathVariable Integer idcourse){
		Course course = courseRepository.getOne(idcourse);
		return courseholesRepository.findAllByIdcourseOrderByHole(course);
	}


}
