package com.mugheadgolf.controller;

import com.mugheadgolf.model.Division;
import com.mugheadgolf.repository.DivisionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("division/")
public class DivisionController {

	@Autowired
	private DivisionRepository divisionRepository;

	@RequestMapping(value="findall" , method = RequestMethod.GET, produces = "application/json")
	public List<Division> list(){
		return divisionRepository.findAll();
	}
	
	@RequestMapping(value="find/{iddivision}" , method = RequestMethod.GET, produces = "application/json")
	public Division getByIddivision(@PathVariable Integer iddivision){
		return divisionRepository.getOne(iddivision);
	}

	@RequestMapping(value="year/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<Division> getByYear(@PathVariable Integer year){
		return divisionRepository.findAllByYear(year);
	}

	@RequestMapping(value="delete/{iddivision}" , method = RequestMethod.DELETE, produces = "application/json")
	public void getDivision(@PathVariable Integer iddivision){
		divisionRepository.deleteById(iddivision);
	}
	
	// Create a new Note
	@RequestMapping(value="save" , method = RequestMethod.POST, produces = "application/json")
	public Division save(@RequestBody Division division) {		 
		return divisionRepository.save(division);
	}

}
