package com.mugheadgolf.controller;

import com.mugheadgolf.model.DivisionGolfer;
import com.mugheadgolf.repository.DivisionGolferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("divisionGolfer/")
public class DivisionGolferController {

	@Autowired
	private DivisionGolferRepository divisionGolferRepository;

	@RequestMapping(value="findall" , method = RequestMethod.GET, produces = "application/json")
	public List<DivisionGolfer> list(){
		return divisionGolferRepository.findAll();
	}
	
	@RequestMapping(value="find/{iddivisiongolfer}" , method = RequestMethod.GET, produces = "application/json")
	public DivisionGolfer getByIddivisiongolfer(@PathVariable Integer iddivisiongolfer){
		return divisionGolferRepository.getOne(iddivisiongolfer);
	}

	@RequestMapping(value="division/{iddivision}" , method = RequestMethod.GET, produces = "application/json")
	public List<DivisionGolfer> getByIddivision(@PathVariable Integer iddivision){
		return divisionGolferRepository.findAllByIddivision(iddivision);
	}

	@RequestMapping(value="delete/{iddivisiongolfer}" , method = RequestMethod.DELETE, produces = "application/json")
	public void getDivisionGolfer(@PathVariable Integer iddivisiongolfer){
		divisionGolferRepository.deleteById(iddivisiongolfer);
	}
	
	// Create a new Note
	@RequestMapping(value="save" , method = RequestMethod.POST, produces = "application/json")
	public DivisionGolfer save(@RequestBody DivisionGolfer divisiongolfer) {		 
		return divisionGolferRepository.save(divisiongolfer);
	}

	@RequestMapping(value="golfer/{idgolfer}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<DivisionGolfer> getByIdgolfer(@PathVariable Integer idgolfer, @PathVariable Integer year){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        DivisionGolfer divisionGolfer = divisionGolferRepository.findFirstByIdgolferAndYear(idgolfer,year);
        if(divisionGolfer == null) {
        	return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
        }
        else {        	
        	return new ResponseEntity<DivisionGolfer>(divisionGolfer, headers, HttpStatus.OK);
        }
	}
	
}
