package com.mugheadgolf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.data.EmailMessage;
import com.mugheadgolf.service.EmailService;




@RestController
@RequestMapping("email")
public class EmailController {


	@Autowired
	private EmailService emailService;
 
	@RequestMapping(value="" , method = RequestMethod.POST, produces = "application/xml")
	public  String send(@RequestBody EmailMessage email){
		if( emailService.send(email)) {
			return "Success";
		}
		else {
			return "Email send failed";
		}
	}

	@RequestMapping(value="/schedule" , method = RequestMethod.GET, produces = "application/xml")
	public  String send(){
		if( emailService.sendWeeklySchedule()) {
			return "Success";
		}
		else {
			return "Email send failed";
		}
	}

}
