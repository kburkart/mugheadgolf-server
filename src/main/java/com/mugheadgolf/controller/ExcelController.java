package com.mugheadgolf.controller;

import com.mugheadgolf.data.EmailMessage;
import com.mugheadgolf.service.ExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@RestController
@RequestMapping("excel")
public class ExcelController {


	@Autowired
	private ExcelService excelService;

	@RequestMapping(value="/generate" , method = RequestMethod.GET, produces = "application/xml")
	public  byte[] generate() throws IOException {
		return excelService.generateExcel();
	}
}
