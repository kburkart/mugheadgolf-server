package com.mugheadgolf.controller;

import com.mugheadgolf.data.WeeklyFoursomeData;
import com.mugheadgolf.model.Foursome;
import com.mugheadgolf.repository.FoursomeRepository;
import com.mugheadgolf.service.FoursomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("foursome")
public class FoursomeController {

	@Autowired
	private FoursomeRepository foursomeRepository;

	@Autowired
	private FoursomeService foursomeService;

	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<Foursome> list(){
		return foursomeRepository.findAll();
	}

	@RequestMapping(value="/{idfoursome}" , method = RequestMethod.GET, produces = "application/json")
	public Foursome get(@PathVariable Integer idfoursome){
		return foursomeRepository.getOne(idfoursome);
	}

	@RequestMapping(value="/week/{week}/year/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<Foursome> getFoursomeWeekYear(@PathVariable Integer week,@PathVariable Integer year){
		return foursomeRepository.findAllByIdweekAndYearOrderByTeetime(week, year);
	}

	@RequestMapping(value="/year/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<WeeklyFoursomeData> getFoursomeYear(@PathVariable Integer year){
		return foursomeService.getYear(year);
	}

	@RequestMapping(value="/saveAll", method = RequestMethod.POST, produces = "application/json")
	public void save(@RequestBody List<Foursome> foursomes) {
		foursomeRepository.saveAll(foursomes);
	}

	@RequestMapping(value="/save" ,method = RequestMethod.POST, produces = "application/json")
	public void save(@RequestBody Foursome foursome) {
		foursomeRepository.save(foursome);
	}

	@RequestMapping(value="/generate/week/{week}/year/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<Foursome> generateRandomFoursomes(@PathVariable Integer week,@PathVariable Integer year){
		return foursomeService.generateRandomFoursomes(week, year);
	}

	@RequestMapping(value="/generate/year/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<Foursome> generateRandomFoursomesYear(@PathVariable Integer year){
		return foursomeService.generateRandomFoursomesYear(year);
	}

	@RequestMapping(value="/swap/{week}/{year}/{schedule1}/{schedule2}" , method = RequestMethod.GET, produces = "application/json")
	public List<WeeklyFoursomeData> swapTeeTimes(@PathVariable Integer year, @PathVariable Integer week, @PathVariable Integer schedule1 , @PathVariable Integer schedule2){
		foursomeService.swapTeeTimes(year, week, schedule1, schedule2);
		return foursomeService.getYear(year);
	}

	@RequestMapping(value="saveBacknine" , method = RequestMethod.POST, produces = "application/json")
	public void saveBacknine(@RequestBody Foursome foresome) {
		foursomeRepository.save(foresome);
	}

}
