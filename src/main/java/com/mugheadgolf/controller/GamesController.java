package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.Games;
import com.mugheadgolf.repository.GamesRepository;


@RestController
@RequestMapping("games")
public class GamesController {

	@Autowired
	private GamesRepository gamesRepository;


	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<Games> list(){
		return gamesRepository.findAllByOrderByIdgame();
	}

	// Create a new Note
	@RequestMapping(value="/save" , method = RequestMethod.POST, produces = "application/json")
	public List<Games> save(@RequestBody List<Games> games) {		
		return gamesRepository.saveAll(games);
	}



}
