package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.service.GolferService;


@RestController
@RequestMapping("golfer/")
public class GolferController {

	@Autowired
    private GolferService golferService;

    
    private static boolean ACTIVE = true;
	
	@Autowired
	private GolferRepository golferRepository;

	@RequestMapping(value="findall" , method = RequestMethod.GET, produces = "application/json")
	public List<Golfer> list(){
		return golferRepository.findAll();
	}

	@RequestMapping(value="find/{idgolfer}" , method = RequestMethod.GET, produces = "application/json")
	public Golfer getGolfers(@PathVariable Integer idgolfer){
		return golferRepository.getOne(idgolfer);
	}
	
	@RequestMapping(value="autoRefresh/{idgolfer}" , method = RequestMethod.GET, produces = "application/json")
	public void toggleAutoRefresh(@PathVariable Integer idgolfer){
		Golfer golfer = golferRepository.getOne(idgolfer);
		golfer.setAutorefresh(!golfer.isAutorefresh());
		golferRepository.save(golfer);
	}

	@RequestMapping(value="findActive" , method = RequestMethod.GET, produces = "application/json")
	public List<Golfer> getActiveGolfers(){
		return golferRepository.findAllByActive(ACTIVE);
	}


	@RequestMapping(value="register" , method = RequestMethod.POST, produces = "application/json")
	public  Golfer register(@RequestBody Golfer golfer){
		
		return golferService.register(golfer);
	}

	@RequestMapping(value="login" , method = RequestMethod.POST, produces = "application/json")
	public  Golfer login(@RequestBody Golfer golfer){

		return golferService.login(golfer.getUsername(), golfer.getPassword());
	}

	@RequestMapping(value="add" , method = RequestMethod.POST, produces = "application/json")
	public  Golfer add(@RequestBody Golfer golfer){
		
		return golferService.add(golfer);
	}

	@RequestMapping(value="confirmemail/{token}" , method = RequestMethod.GET, produces = "application/json")
	public  void confirmEmail(@PathVariable String token){
		golferService.confirmEmail(token);
	}

	@RequestMapping(value="forgot/{phone}" , method = RequestMethod.GET, produces = "application/json")
	public  void forgot(@PathVariable String phone){
		golferService.forgot(phone);
	}
	

	


}
