package com.mugheadgolf.controller;

import java.util.List;

import com.mugheadgolf.model.Golfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.mugheadgolf.data.HandicapData;
import com.mugheadgolf.model.Handicap;
import com.mugheadgolf.repository.HandicapRepository;
import com.mugheadgolf.service.HandicapService;


@RestController
@RequestMapping("handicap")
public class HandicapController {

	@Autowired
	private HandicapRepository handicapRepository;

	@Autowired
	private HandicapService handicapService90Percent;

	@Autowired
	private HandicapService handicapService;

	@RequestMapping(value="/findall" , method = RequestMethod.GET, produces = "application/json")
	public List<Handicap> list(){
		return handicapRepository.findAll();
	}

	@RequestMapping(value="year/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<HandicapData> getAllHandicapsByYear(@PathVariable Integer year){
		return handicapService.getAllHandicapsByYear(year);
		//return handicapService90Percent.getAllHandicapsByYear(year);
	}

	@RequestMapping(value="calculate" , method = RequestMethod.GET, produces = "application/json")
	public String calculateHandicaps(){
		handicapService90Percent.calculateHandicaps();
		//handicapService.calculateHandicaps();
		return "Calculated";
	}

	@RequestMapping(value="calculateGolfer" , method = RequestMethod.POST, produces = "application/json")
	public String calculateGolferHandicap(@RequestBody Golfer golfer){
		handicapService90Percent.calculateHandicaps(golfer);
		//handicapService.calculateHandicaps();
		return "Calculated";
	}


}
