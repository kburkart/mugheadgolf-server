package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.GolferMoney;
import com.mugheadgolf.model.Menu;
import com.mugheadgolf.model.Money;
import com.mugheadgolf.repository.GolferMoneyRepository;
import com.mugheadgolf.repository.MenuRepository;
import com.mugheadgolf.repository.MoneyRepository;
import com.mugheadgolf.service.MoneyService;


@RestController
@RequestMapping("menu")
public class MenuController {

	@Autowired
	private MenuRepository menuRepository;


	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<Menu> getMenu(){
		return menuRepository.findAll();
	}

}
