package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.GolferMoney;
import com.mugheadgolf.model.Money;
import com.mugheadgolf.repository.GolferMoneyRepository;
import com.mugheadgolf.repository.MoneyRepository;
import com.mugheadgolf.service.MoneyService;


@RestController
@RequestMapping("money")
public class MoneyController {

	@Autowired
	private MoneyRepository moneyRepository;

	@Autowired
	private GolferMoneyRepository golferMoneyRepository;

	@Autowired
	private MoneyService moneyService;

	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<Money> list(){
		return moneyRepository.findAll();
	}

	@RequestMapping(value="/golfers/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<GolferMoney> getGolfersMoney(@PathVariable Integer year){
		return golferMoneyRepository.findAllByYear(year);
	}

	
	@RequestMapping(value="/golfer/{idgolfer}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public GolferMoney getGolferMoney(@PathVariable Integer idgolfer, @PathVariable Integer year){
		return golferMoneyRepository.findFirstByIdgolferAndYear(idgolfer, year);
	}

	// Create a new Note
	@RequestMapping(value="/save" , method = RequestMethod.POST, produces = "application/json")
	public Money save(@RequestBody Money money) {		 
		return moneyRepository.save(money);
	}

	@Transactional
	@RequestMapping(value="/saveWeek/{idweek}/{year}" , method = RequestMethod.POST, produces = "application/json")
	public List<Money> save(@RequestBody List<Money> money, @PathVariable Integer idweek, @PathVariable Integer year) {
		 money.removeIf((Money mon) -> mon.getIdgolfer() == null);
		long rowsDeleted = moneyRepository.deleteByIdweekAndYear(idweek, year);
		return moneyRepository.saveAll(money);
	}

	@RequestMapping(value="week/{idweek}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public  List<Money> findAllByWeek(@PathVariable Integer idweek, @PathVariable Integer year){
		return moneyRepository.findAllByIdweekAndYearOrderByIdgame(idweek, year);
	}

	@RequestMapping(value="lownet/{year}/{idweek}" , method = RequestMethod.GET, produces = "application/json")
	public  List<List<Golfer>> calculateLowNet(@PathVariable Integer idweek, @PathVariable Integer year){
		return moneyService.calculateLowNet(year, idweek);
	}

	@RequestMapping(value="skins/{year}/{idweek}" , method = RequestMethod.GET, produces = "application/json")
	public  List<Golfer> calculateSkins(@PathVariable Integer idweek, @PathVariable Integer year){
		return moneyService.calculateSkins(year, idweek);
	}


	@RequestMapping(value="year/{year}" , method = RequestMethod.GET, produces = "application/json")
	public  List<List<Money>> findWeeklyByYear(@PathVariable Integer year){
		return moneyService.getWeeklyWinners(year);
	}



}
