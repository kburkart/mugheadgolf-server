package com.mugheadgolf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.data.EmailMessage;
import com.mugheadgolf.model.Foodorder;
import com.mugheadgolf.service.EmailService;
import com.mugheadgolf.service.OrderService;




@RestController
@RequestMapping("order/")
public class OrderController {


	@Autowired
	private OrderService orderService;
 
	@RequestMapping(value="save/{placeorder}" , method = RequestMethod.POST, produces = "application/json")
	public Foodorder save(@RequestBody Foodorder order, @PathVariable boolean placeorder){
		return orderService.save(order, placeorder);
	}


}
