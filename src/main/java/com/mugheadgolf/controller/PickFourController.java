package com.mugheadgolf.controller;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.PickFour;
import com.mugheadgolf.repository.PickFourRepository;
import com.mugheadgolf.service.PickFourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@RestController
@RequestMapping("pickfour")
public class PickFourController {


	@Autowired
	private PickFourRepository pickFourRepository;
	
	@Autowired
	private PickFourService pickFourService;

	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<PickFour> list(){
		return pickFourRepository.findAll();
	}

	// Create a new Note
	@RequestMapping(value="/save" , method = RequestMethod.POST, produces = "application/json")
	public PickFour save(@RequestBody PickFour pickFour) {		 
		return pickFourRepository.save(pickFour);
	}


	@RequestMapping(value="/week/{idweek}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public  List<PickFour> findAllByWeek(@PathVariable Integer idweek, @PathVariable Integer year){
		return pickFourRepository.findAllByWeekAndYearOrderByTotal(idweek, year);
	}

	@RequestMapping(value="/golfer/{week}/{year}/{golfer}" , method = RequestMethod.GET, produces = "application/json")
	public  PickFour findFirstByWeekYearGolfer(@PathVariable Integer week, @PathVariable Integer year, @PathVariable Golfer golfer){
		return pickFourRepository.findFirstByWeekAndYearAndIdgolfer(week, year, golfer);
	}
	
	@RequestMapping(value="/calculate/{week}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public  Boolean calculate(@PathVariable Integer week, @PathVariable Integer year){
		 return pickFourService.calculateTotals(week, year);
	}
	



}
