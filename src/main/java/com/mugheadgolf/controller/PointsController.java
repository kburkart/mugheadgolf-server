package com.mugheadgolf.controller;

import java.math.BigDecimal;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.PointTotal;
import com.mugheadgolf.model.Points;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.repository.PointsRepository;
import com.mugheadgolf.repository.TotalpointsRepository;
import com.mugheadgolf.service.PointsService;
import com.mugheadgolf.service.ScheduleService;

@Slf4j
@RestController
@RequestMapping("points/")
public class PointsController {

	@Autowired
	private PointsRepository pointsRepository;
	
	@Autowired
	private PointsService pointsService;
	
	@Autowired
	private ScheduleService scheduleService;
	
	@Autowired
	private TotalpointsRepository totalpointsRepository;

	@RequestMapping(value="all" , method = RequestMethod.GET, produces = "application/json")
	public List<Points> list(){
		return pointsRepository.findAll();
	}
	
	@RequestMapping(value="golfer/{idgolfer}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<Points> getAllPointsByGolfer(@PathVariable Integer idgolfer, @PathVariable Integer year){
		return pointsRepository.findAllByIdgolferAndYear(idgolfer, year);
	}

	@RequestMapping(value="total/{idgolfer}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public BigDecimal getGolferTotal(@PathVariable Integer idgolfer, @PathVariable Integer year){
		BigDecimal golferPoints = pointsRepository.pointsForGolfer(idgolfer, year);
		return golferPoints;

	}
	
	@RequestMapping(value="golferWeek/{idgolfer}/{week}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public BigDecimal getGolferWeekPoints(@PathVariable Integer idgolfer, @PathVariable Integer week, @PathVariable Integer year){
		BigDecimal golferPoints = pointsRepository.findFirstByIdgolferAndWeekAndYear(idgolfer, week, year).getPoints();
		return golferPoints;
	}

	
	@RequestMapping(value="divisionleader/{iddivision}" , method = RequestMethod.GET, produces = "application/json")
	public PointTotal getDivisionLeader(@PathVariable Integer iddivision){
		return totalpointsRepository.findFirstByIddivision(iddivision);
	}
	
	@RequestMapping(value="calcPoints/{week}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public void calcPoints(@PathVariable Integer week, @PathVariable Integer year){
		List<Schedule> schedules = scheduleService.getWeek(year, week);
		schedules.forEach(schedule->{
			log.debug("Calculate points for " + schedule.getIdgolfer1().getFirstname() + ' ' + schedule.getIdgolfer1().getLastname() + " vs " + schedule.getIdgolfer2().getFirstname() + ' ' + schedule.getIdgolfer2().getLastname());
			pointsService.calculatePoints(schedule.getIdschedule(), year);
		});

	}

}
