package com.mugheadgolf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.service.SmsService;




@RestController
@RequestMapping("sms")
public class SMSController {


	@Autowired
	private SmsService smsService;
 
	@RequestMapping(value="" , method = RequestMethod.POST, produces = "application/xml")
	public  String register(@RequestBody String sms){
		return smsService.processSMS(sms);
	}

}
