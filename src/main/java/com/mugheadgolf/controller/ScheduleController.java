package com.mugheadgolf.controller;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.data.WeeklyScheduleData;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.repository.ScheduleRepository;
import com.mugheadgolf.service.ScheduleService;
import com.mugheadgolf.utilities.Communicate;


@RestController
@RequestMapping("schedule/")
public class ScheduleController {
	private static final Logger Logger = LoggerFactory.getLogger(ScheduleController.class);

	@Autowired
	private ScheduleRepository scheduleRepository;

	@Autowired
	private ScheduleService scheduleService;

	@RequestMapping(value="list" , method = RequestMethod.GET, produces = "application/json")
	public List<Schedule> list(){
		return scheduleRepository.findAll();
	}

	@RequestMapping(value="create/{year}" , method = RequestMethod.GET, produces = "application/json")
	public void create(@PathVariable Integer year){
		scheduleService.create(year);
	}

	@RequestMapping(value="year/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<WeeklyScheduleData> getYear(@PathVariable Integer year){
		return scheduleService.getYear(year);
	}

	@RequestMapping(value="golfer/{golfer}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<Schedule> getYear(@PathVariable Golfer golfer, @PathVariable Integer year){
		return scheduleRepository.getGolferSchedule(year, golfer);
	}

	
	@RequestMapping(value="week/{year}/{week}" , method = RequestMethod.GET, produces = "application/json")
	public List<Schedule> getWeek(@PathVariable Integer year, @PathVariable Integer week){
		return scheduleService.getWeek(year,week);
	}
	

	@RequestMapping(value="weekHC/{week}/{year}/{golfer}" , method = RequestMethod.GET, produces = "application/json")
	public BigDecimal getWeekHC(@PathVariable Integer year, @PathVariable Integer week, @PathVariable Golfer golfer){
		return scheduleService.getGolferWeekHandicap(golfer, week, year);
	}

	@RequestMapping(value="swap/{week}/{year}/{golfer1}/{golfer2}" , method = RequestMethod.GET, produces = "application/json")
	public List<WeeklyScheduleData> swapGolfers(@PathVariable Integer year, @PathVariable Integer week, @PathVariable Integer golfer1 , @PathVariable Integer golfer2){
		scheduleService.swapGolfers(year, week, golfer1, golfer2);
		return scheduleService.getYear(year);
	}

	@RequestMapping(value="toggleAbsent/{year}/{idgolfer}/{idschedule}" , method = RequestMethod.GET, produces = "application/json")
	public List<WeeklyScheduleData> toggleAbsent(@PathVariable Integer year, @PathVariable Integer idgolfer, @PathVariable Integer idschedule){
		scheduleService.toggleAbsent(idgolfer, idschedule);
		return scheduleService.getYear(year);
	}

	@RequestMapping(value="{idschedule}" , method = RequestMethod.GET, produces = "application/json")
	public Schedule getSchedule(@PathVariable Integer idschedule){
		return scheduleRepository.getOne(idschedule);
	}
	
	// Create a new Note
	@RequestMapping(value="saveBacknine" , method = RequestMethod.POST, produces = "application/json")
	public void saveBacknine(@RequestBody Schedule schedule) {
		 
	     scheduleRepository.save(schedule);
	}

	// Create a new Note
	@RequestMapping(value="save" , method = RequestMethod.POST, produces = "application/json")
	public void save(@RequestBody Schedule schedule) {
		Logger.debug("Save schedule called");
	     scheduleRepository.save(schedule);
		 if(schedule.isAbsent1() || schedule.isAbsent2()) {
			 Communicate.sendAbsenteeNotice(schedule);
		 }
	}

	@RequestMapping(value="currentWeek/{year}" , method = RequestMethod.GET, produces = "application/json")
	public Integer getCurrentWeek(@PathVariable Integer year){
		return scheduleService.getCurrentWeek(year);
	}


}
