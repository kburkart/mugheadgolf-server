package com.mugheadgolf.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.data.GolferScoreData;
import com.mugheadgolf.data.ScoreData;
import com.mugheadgolf.data.WeeklyScoreData;
import com.mugheadgolf.model.Score;
import com.mugheadgolf.repository.ScoreRepository;
import com.mugheadgolf.service.ScoreService;



@RestController
@RequestMapping("score")
public class ScoreController {

	@Autowired
	private ScoreRepository scoreRepository;
	@Autowired
	private ScoreService scoreService;

	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<Score> list(){
		return scoreRepository.findAll();
	}

	@RequestMapping(value="/hasGolfStarted/{idweek}/{year}"  , method = RequestMethod.GET, produces = "application/json")
	public Boolean hasScoringStarted(@PathVariable Integer idweek, @PathVariable Integer year){
		Boolean hasStarted = true;
		List<Score> scores = scoreRepository.findAllByYearAndIdweek(year, idweek);
		if(scores == null || scores.isEmpty()) {
			hasStarted = false;
		}
		return hasStarted;
	}

	@RequestMapping(value="/schedule/{idschedule}" , method = RequestMethod.GET, produces = "application/json")
	public List<ScoreData> getByIdschedule(@PathVariable Integer idschedule){
		return scoreService.getByIdschedule(idschedule);
	}

	@RequestMapping(value="/foursome/{idfoursome}" , method = RequestMethod.GET, produces = "application/json")
	public List<ScoreData> getByIdfoursome(@PathVariable Integer idfoursome){
		return scoreService.getByIdfoursome(idfoursome);
	}

	@RequestMapping(value="/golferWeek/{idschedule}/{idgolfer}" , method = RequestMethod.GET, produces = "application/json")
	public ScoreData getByIdscheduleAndIdgolfer(@PathVariable Integer idschedule, @PathVariable Integer idgolfer){
		return scoreService.getByIdscheduleAndIdGolfer(idschedule, idgolfer);
	}

	@RequestMapping(value="/data/{idscore}" , method = RequestMethod.GET, produces = "application/json")
	public ScoreData getScoreData(@PathVariable Integer idscore){
		return scoreService.getScoreData(idscore);
	}
	
	@RequestMapping(value="/golfersAll/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<GolferScoreData> getGolfersScores(@PathVariable Integer year){
		return scoreService.getGolfersScores(year);
	}

	// Create a new Note
	@RequestMapping(value="/saveScores" , params = "hc", method = RequestMethod.POST, produces = "application/json")
	public void save(@RequestBody List<Score> scores, @RequestParam(value="hc", required = true) boolean hc) {
		 
	     scoreService.saveScores(scores, hc);
	}

	@RequestMapping(value="/week/{idweek}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<WeeklyScoreData> getScoreData(@PathVariable Integer idweek, @PathVariable Integer year){
		return scoreService.getWeeklyScores(idweek, year);
	}

	@RequestMapping(value="/avg/{idgolfer}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public BigDecimal getGolferAvg(@PathVariable Integer idgolfer, @PathVariable Integer year){
		return scoreService.getGolferAvg(idgolfer, year);
	}


	@RequestMapping(value="/avgAll/{year}" , method = RequestMethod.GET, produces = "application/json")
	public BigDecimal getLeagueAvg(@PathVariable Integer year){
		return scoreService.getLeagueAvg(year);
	}

	

}
