package com.mugheadgolf.controller;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mugheadgolf.service.ScoreholeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.mugheadgolf.model.Score;
import com.mugheadgolf.model.Scorehole;
import com.mugheadgolf.repository.ScoreRepository;
import com.mugheadgolf.repository.ScoreholeRepository;

@RestController
@RequestMapping("scorehole")
public class ScoreholeController {

	@Autowired
	ScoreholeService scoreholeService;

	@Autowired
	private ScoreholeRepository scoreholeRepository;

	@Autowired
	private ScoreRepository scoreRepository;

	@RequestMapping(value="/{idscore}" , method = RequestMethod.GET, produces = "application/json")
	public List<Scorehole> getByIdscore(@PathVariable Integer idscore){
		Score score = scoreRepository.getOne(idscore);
		return scoreholeRepository.findAllByIdscoreOrderByHole(score);
	}

	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<Scorehole> list(){
		return scoreholeRepository.findAll();
	}
	
	// Create a new Note
	@RequestMapping(value="/save" , method = RequestMethod.POST, produces = "application/json")
	public Scorehole save(@RequestBody Scorehole scorehole) {
	    return scoreholeRepository.save(scorehole);
	}

	@RequestMapping(value="/saveWithPar/{par}" , method = RequestMethod.POST, produces = "application/json")
	public Scorehole save(@RequestBody Scorehole scorehole, @PathVariable int par) throws JsonProcessingException {
		return scoreholeService.saveWithStablefordCalc(scorehole, par);
	}

}
