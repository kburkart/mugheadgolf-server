package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.SeedHigh;
import com.mugheadgolf.repository.SeedHighRepository;


@RestController
@RequestMapping("seedHigh")
public class SeedHighController {

	@Autowired
	private SeedHighRepository seedHighRepository;

	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<SeedHigh> list(){
		return seedHighRepository.findAll();
	}

	


}
