package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.SeedLow;
import com.mugheadgolf.repository.SeedLowRepository;


@RestController
@RequestMapping("seedLow")
public class SeedLowController {

	@Autowired
	private SeedLowRepository seedLowRepository;

	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<SeedLow> list(){
		return seedLowRepository.findAll();
	}

	


}
