package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.Scorehole;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.SettingsRepository;


@RestController
@RequestMapping("settings/")
public class SettingsController {

	@Autowired
	private SettingsRepository settingsRepository;

	@RequestMapping(value="find/{year}" , method = RequestMethod.GET, produces = "application/json")
	public  Settings getSettings(@PathVariable Integer year){
		Settings settings = settingsRepository.findFirstByYear(year);
		return settings;
	}

	@RequestMapping(value="save" , method = RequestMethod.POST, produces = "application/json")
	public Settings save(@RequestBody Settings settings) {
	    return settingsRepository.save(settings);
	}


}
