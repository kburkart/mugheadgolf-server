package com.mugheadgolf.controller;

import com.mugheadgolf.data.HandicapData;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Handicap;
import com.mugheadgolf.repository.HandicapRepository;
import com.mugheadgolf.service.HandicapService;
import com.mugheadgolf.service.StablefordScoringService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("stableford")
public class StablefordController {

	private StablefordScoringService stablefordScoringService;

	public StablefordController(StablefordScoringService stablefordScoringService) {
		this.stablefordScoringService = stablefordScoringService;
	}

	@RequestMapping(value="calculate" , method = RequestMethod.GET, produces = "application/json")
	public String calculateStablefordScoring(){
		stablefordScoringService.calculateStablefordScoring();
		return "Calculated";
	}

	@RequestMapping(value="calculate/{year}" , method = RequestMethod.GET, produces = "application/json")
	public String calculateStablefordScoringYear(@PathVariable int year){
		stablefordScoringService.calculateStablefordScoring(year);
		return "Calculated";
	}

	@RequestMapping(value="calculateGolfer" , method = RequestMethod.POST, produces = "application/json")
	public String calculateGolferStableford(@RequestBody Golfer golfer){
		stablefordScoringService.calculateGolferStablefordScoring(golfer);
		return "Calculated";
	}

	@RequestMapping(value="calculateGolfer/{year}" , method = RequestMethod.POST, produces = "application/json")
	public String calculateGolferStablefordYear(@RequestBody Golfer golfer, @PathVariable int year){
		stablefordScoringService.calculateGolferStablefordScoring(golfer, year);
		return "Calculated";
	}

	@RequestMapping(value="getYearlyTotals/{year}/{isHC}" , method = RequestMethod.GET, produces = "application/json")
	public Map<Integer, List<Map<String, Object>>> calculateGolferStableford(@PathVariable int year, @PathVariable boolean isHC){
		return stablefordScoringService.getStablefordYearlyTotals(year, isHC);
	}

	@RequestMapping(value="getTeamYearlyTotals/{year}/{isHC}" , method = RequestMethod.GET, produces = "application/json")
	public Map<String, Map<String, Object>> getGolferYearlyStableford(@PathVariable int year, @PathVariable boolean isHC){
		return stablefordScoringService.getTeamStablefordYearlyTotals(year, isHC);
	}
}
