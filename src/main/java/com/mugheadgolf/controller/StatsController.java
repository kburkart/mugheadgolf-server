package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.Stats;
import com.mugheadgolf.model.WeeklyStats;
import com.mugheadgolf.repository.StatsRepository;
import com.mugheadgolf.repository.WeeklyStatsRepository;


@RestController
@RequestMapping("stats/")
public class StatsController {

	@Autowired
	private StatsRepository statsRepository;

	@Autowired
	private WeeklyStatsRepository weeklyStatsRepository;

	@RequestMapping(value="find/{year}" , method = RequestMethod.GET, produces = "application/json")
	public  List<Stats> getStats(@PathVariable Integer year){
		return statsRepository.findAllByYear(year);
	}

	@RequestMapping(value="golfer/{idgolfer}/{year}" , method = RequestMethod.GET, produces = "application/json")
	public List<WeeklyStats> getGolfersWeeklyStats(@PathVariable Integer idgolfer, @PathVariable Integer year) {
	    return weeklyStatsRepository.findAllByIdgolferAndYearOrderByIdweek(idgolfer, year);
	}


}
