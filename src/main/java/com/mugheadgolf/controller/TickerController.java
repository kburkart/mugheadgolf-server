package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.service.TickerService;


@RestController
@RequestMapping("ticker")
public class TickerController {

	@Autowired
	private TickerService tickerService;

	@RequestMapping(value="/messages" , method = RequestMethod.GET, produces = "application/json")
	public List<String> list(){
		return tickerService.getMessages();
	}

	


}
