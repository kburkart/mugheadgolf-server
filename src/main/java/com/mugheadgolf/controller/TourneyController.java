package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.Score;
import com.mugheadgolf.model.Tourney;
import com.mugheadgolf.repository.TourneyRepository;


@RestController
@RequestMapping("tourney")
public class TourneyController {

	@Autowired
	private TourneyRepository tourneyRepository;

	@RequestMapping(value="/bracket/{highHandicap}" , method = RequestMethod.GET, produces = "application/json")
	public List<Tourney> list(@PathVariable Boolean highHandicap){
		return tourneyRepository.findAllByHighHandicapOrderByBranch(highHandicap);
	}

	// Create a new Note
	@RequestMapping(value="/saveBranch" , method = RequestMethod.POST, produces = "application/json")
	public void save(@RequestBody Tourney branch) {
		 
	     tourneyRepository.save(branch);
	}

	


}
