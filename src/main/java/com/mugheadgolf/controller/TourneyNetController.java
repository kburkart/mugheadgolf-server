package com.mugheadgolf.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mugheadgolf.model.Tourney;
import com.mugheadgolf.model.TourneyNet;
import com.mugheadgolf.repository.TourneyNetRepository;


@RestController
@RequestMapping("tourneynet")
public class TourneyNetController {

	@Autowired
	private TourneyNetRepository tourneyNetRepository;

	@RequestMapping(value="/all" , method = RequestMethod.GET, produces = "application/json")
	public List<TourneyNet> list(){
		return tourneyNetRepository.findAllByOrderByToPar();
	}

}
