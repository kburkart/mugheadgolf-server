package com.mugheadgolf.data;

import java.io.Serializable;
import java.util.List;

import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Courseholes;

public class CourseData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 601151081331957819L;
	private Course course;
	private List<Courseholes> courseholes;
	
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	public List<Courseholes> getCourseholes() {
		return courseholes;
	}
	public void setCourseholes(List<Courseholes> courseholes) {
		this.courseholes = courseholes;
	}
	
	
}
