package com.mugheadgolf.data;

import java.io.Serializable;
import java.util.List;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Score;

public class GolferScoreData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5960063532555097685L;
	private Golfer golfer;
	private List<Score> scores;
	public Golfer getGolfer() {
		return golfer;
	}
	public void setGolfer(Golfer golfer) {
		this.golfer = golfer;
	}
	public List<Score> getScores() {
		return scores;
	}
	public void setScores(List<Score> scores) {
		this.scores = scores;
	}
	
	
	

}
