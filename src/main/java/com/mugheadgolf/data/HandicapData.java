package com.mugheadgolf.data;

import java.io.Serializable;
import java.util.List;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Handicap;

public class HandicapData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 601151081331957819L;
	private Golfer golfer;
	private List<Handicap> handicaps;
	
	public Golfer getGolfer() {
		return golfer;
	}
	public void setGolfer(Golfer golfer) {
		this.golfer = golfer;
	}
	public List<Handicap> getHandicaps() {
		return handicaps;
	}
	public void setHandicaps(List<Handicap> handicaps) {
		this.handicaps = handicaps;
	}
	
	
	
	
}
