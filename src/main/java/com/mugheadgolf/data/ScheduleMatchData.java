package com.mugheadgolf.data;

import java.io.Serializable;

import com.mugheadgolf.model.Golfer;

public class ScheduleMatchData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8369717103675346575L;
	Golfer golfer1;
	Golfer golfer2;
	Integer points1;
	Integer points2;
	Integer handicap1;
	Integer handicap2;
	Integer idschedule;
	
	
	public Golfer getGolfer1() {
		return golfer1;
	}
	public void setGolfer1(Golfer golfer1) {
		this.golfer1 = golfer1;
	}
	public Golfer getGolfer2() {
		return golfer2;
	}
	public void setGolfer2(Golfer golfer2) {
		this.golfer2 = golfer2;
	}
	public Integer getPoints1() {
		return points1;
	}
	public void setPoints1(Integer points1) {
		this.points1 = points1;
	}
	public Integer getPoints2() {
		return points2;
	}
	public void setPoints2(Integer points2) {
		this.points2 = points2;
	}
	public Integer getHandicap1() {
		return handicap1;
	}
	public void setHandicap1(Integer handicap1) {
		this.handicap1 = handicap1;
	}
	public Integer getHandicap2() {
		return handicap2;
	}
	public void setHandicap2(Integer handicap2) {
		this.handicap2 = handicap2;
	}

}
