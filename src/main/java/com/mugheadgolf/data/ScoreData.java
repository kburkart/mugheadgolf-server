package com.mugheadgolf.data;

import java.io.Serializable;
import java.util.List;

import com.mugheadgolf.model.Score;
import com.mugheadgolf.model.Scorehole;

public class ScoreData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5960063532555097685L;
	private Score score;
	private List<Scorehole> scoreholes;
	
	public Score getScore() {
		return score;
	}
	public void setScore(Score score) {
		this.score = score;
	}
	public List<Scorehole> getScoreholes() {
		return scoreholes;
	}
	public void setScoreholes(List<Scorehole> scoreholes) {
		this.scoreholes = scoreholes;
	}
	

}
