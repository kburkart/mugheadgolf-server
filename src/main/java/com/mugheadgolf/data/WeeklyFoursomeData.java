package com.mugheadgolf.data;

import com.mugheadgolf.model.Foursome;

import java.io.Serializable;
import java.util.List;

public class WeeklyFoursomeData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2231688574807679290L;
	
	private int week;
	String course1;
	private List<Foursome> foursomes1;

	String course2;
	private List<Foursome> foursomes2;


	public int getWeek() {
		return week;
	}
	public void setWeek(int week) {
		this.week = week;
	}

	public String getCourse1() {
		return course1;
	}

	public void setCourse1(String course1) {
		this.course1 = course1;
	}

	public List<Foursome> getFoursomes1() {
		return foursomes1;
	}

	public void setFoursomes1(List<Foursome> foursomes1) {
		this.foursomes1 = foursomes1;
	}

	public String getCourse2() {
		return course2;
	}

	public void setCourse2(String course2) {
		this.course2 = course2;
	}

	public List<Foursome> getFoursomes2() {
		return foursomes2;
	}

	public void setFoursomes2(List<Foursome> foursomes2) {
		this.foursomes2 = foursomes2;
	}
}
