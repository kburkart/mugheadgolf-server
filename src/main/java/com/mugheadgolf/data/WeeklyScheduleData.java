package com.mugheadgolf.data;

import java.io.Serializable;
import java.util.List;

import com.mugheadgolf.model.Schedule;

public class WeeklyScheduleData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2231688574807679290L;
	
	private int week;
	private List<Schedule> schedules;
	
	public int getWeek() {
		return week;
	}
	public void setWeek(int week) {
		this.week = week;
	}
	public List<Schedule> getSchedules() {
		return schedules;
	}
	public void setSchedules(List<Schedule> schedules) {
		this.schedules = schedules;
	}

}
