package com.mugheadgolf.data;

import java.io.Serializable;
import java.util.List;

import com.mugheadgolf.model.Schedule;

public class WeeklyScoreData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2231688574807679290L;
	
	private int index;
	private List<ScoreData> scores;
	
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public List<ScoreData> getScores() {
		return scores;
	}
	public void setScores(List<ScoreData> scores) {
		this.scores = scores;
	}
	
}
