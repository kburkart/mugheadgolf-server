package com.mugheadgolf.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Foursome;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Score;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleDto {

    @Id
    private Integer idschedule;
    private Integer year;
    private Integer idweek;
    private BigDecimal points1;
    private BigDecimal points2;
    private boolean absent1;
    private boolean absent2;
    private BigDecimal handicap1;
    private BigDecimal handicap2;
    private Integer active;
    private String courseName;
    private Integer backnine;
    private String firstname1;
    private String lastname1;
    private String firstname2;
    private String lastname2;

}
