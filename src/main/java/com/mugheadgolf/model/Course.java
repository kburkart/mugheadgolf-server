/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.*;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "course")
public class Course implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcourse")
    private Integer idcourse;
    @Size(max = 45)
    @Column(name = "name")
    private String courseName;
    @Size(max = 45)
    @Column(name = "tee")
    private String tee;
    @Column(name = "slope")
    private Integer slope;
    @Column(name = "par")
    private Integer par;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "rating")
    private BigDecimal rating;
    @Column(name = "email")
    private String email;

    @Column(name = "front")
    private Integer front;

    @Column(name = "back")
    private Integer back;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcourse")
    @JsonIgnore
    private Collection<Schedule> scheduleCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcourse")
    @JsonIgnore
    private Collection<Courseholes> courseCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcourse")
    @JsonIgnore
    private Collection<Settings> settingsCollection;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcourse2")
    @JsonIgnore
    private Collection<Settings> settingsCollection2;


    public Course() {
    }

    public Course(Integer idcourse) {
        this.idcourse = idcourse;
    }

    public Integer getIdcourse() {
        return idcourse;
    }

    public void setIdcourse(Integer idcourse) {
        this.idcourse = idcourse;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String name) {
        this.courseName = name;
    }

    public String getTee() {
        return tee;
    }

    public void setTee(String tee) {
        this.tee = tee;
    }

    public Integer getSlope() {
        return slope;
    }

    public void setSlope(Integer slope) {
        this.slope = slope;
    }

    public BigDecimal getRating() {
        return rating;
    }

    public void setRating(BigDecimal rating) {
        this.rating = rating;
    }

    public Integer getPar() {
		return par;
	}

	public void setPar(Integer par) {
		this.par = par;
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @XmlTransient
    public Collection<Courseholes> getCourseCollection() {
        return courseCollection;
    }

    public void setCourseholesCollection(Collection<Courseholes> courseCollection) {
        this.courseCollection = courseCollection;
    }

    public Collection<Settings> getSettingsCollection() {
        return settingsCollection;
    }

    public void setSettingsCollection(Collection<Settings> settingsCollection) {
        this.settingsCollection = settingsCollection;
    }

    public Collection<Settings> getSettingsCollection2() {
        return settingsCollection2;
    }

    public void setSettingsCollection2(Collection<Settings> settingsCollection2) {
        this.settingsCollection2 = settingsCollection2;
    }

    public Integer getFront() {
        return front;
    }

    public void setFront(Integer front) {
        this.front = front;
    }

    public Integer getBack() {
        return back;
    }

    public void setBack(Integer back) {
        this.back = back;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcourse != null ? idcourse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Course)) {
            return false;
        }
        Course other = (Course) object;
        if ((this.idcourse == null && other.idcourse != null) || (this.idcourse != null && !this.idcourse.equals(other.idcourse))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Course[ idcourse=" + idcourse + " ]";
    }
    
}
