/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "courseholes")
public class Courseholes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcourseholes")
    private Integer idcourseholes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hole")
    private int hole;
    @Column(name = "handicap")
    private Integer handicap;
    @Column(name = "length")
    private Integer length;
    @Column(name = "par")
    private Integer par;
    @JoinColumn(name = "idcourse", referencedColumnName = "idcourse")
    @ManyToOne
    private Course idcourse;

    public Courseholes() {
    }

    public Courseholes(Integer idcourseholes) {
        this.idcourseholes = idcourseholes;
    }

    public Courseholes(Integer idcourseholes, int hole) {
        this.idcourseholes = idcourseholes;
        this.hole = hole;
    }

    public Integer getIdcourseholes() {
        return idcourseholes;
    }

    public void setIdcourseholes(Integer idcourseholes) {
        this.idcourseholes = idcourseholes;
    }

    public int getHole() {
        return hole;
    }

    public void setHole(int hole) {
        this.hole = hole;
    }

    public Integer getHandicap() {
        return handicap;
    }

    public void setHandicap(Integer handicap) {
        this.handicap = handicap;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getPar() {
        return par;
    }

    public void setPar(Integer par) {
        this.par = par;
    }

    public Course getIdcourse() {
        return idcourse;
    }

    public void setIdcourse(Course idcourse) {
        this.idcourse = idcourse;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcourseholes != null ? idcourseholes.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Courseholes)) {
            return false;
        }
        Courseholes other = (Courseholes) object;
        if ((this.idcourseholes == null && other.idcourseholes != null) || (this.idcourseholes != null && !this.idcourseholes.equals(other.idcourseholes))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Courseholes[ idcourseholes=" + idcourseholes + " ]";
    }
    
}
