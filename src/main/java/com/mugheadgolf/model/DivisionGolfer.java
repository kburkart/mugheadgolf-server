/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "divisiongolfer")
public class DivisionGolfer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddivisiongolfer")
    private Integer iddivisiongolfer;
    @Column(name = "iddivision")
    private Integer iddivision;
    @Column(name = "idgolfer")
    private Integer idgolfer;
    @Column(name = "year")
    private Integer year;

    public DivisionGolfer() {
    }

    
    public Integer getYear() {
		return year;
	}


	public void setYear(Integer year) {
		this.year = year;
	}


	public DivisionGolfer(Integer iddivisiongolfer) {
        this.iddivisiongolfer = iddivisiongolfer;
    }

    public Integer getIddivisiongolfer() {
        return iddivisiongolfer;
    }

    public void setIddivisiongolfer(Integer iddivisiongolfer) {
        this.iddivisiongolfer = iddivisiongolfer;
    }

    public Integer getIddivision() {
        return iddivision;
    }

    public void setIddivision(Integer iddivision) {
        this.iddivision = iddivision;
    }

    public Integer getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(Integer idgolfer) {
        this.idgolfer = idgolfer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddivisiongolfer != null ? iddivisiongolfer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DivisionGolfer)) {
            return false;
        }
        DivisionGolfer other = (DivisionGolfer) object;
        if ((this.iddivisiongolfer == null && other.iddivisiongolfer != null) || (this.iddivisiongolfer != null && !this.iddivisiongolfer.equals(other.iddivisiongolfer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.DivisionGolfer[ iddivisiongolfer=" + iddivisiongolfer + " ]";
    }
    
}
