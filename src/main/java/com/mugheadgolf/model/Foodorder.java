/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "foodorder")

public class Foodorder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idorder")
    private Long idorder;
    @Column(name = "ordertime")
    @Temporal(TemporalType.DATE)
    private Date ordertime;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "totalcost")
    private BigDecimal totalcost;
    @Basic(optional = false)
    @NotNull
    @Column(name = "confirmed")
    private boolean confirmed;
    @JoinColumn(name = "idgolfer", referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer idgolfer;
    @Column(name = "description")
    private String description;
    

    public Foodorder() {
    }

    public Foodorder(Long idorder) {
        this.idorder = idorder;
    }

    public Foodorder(Long idorder, boolean confirmed) {
        this.idorder = idorder;
        this.confirmed = confirmed;
    }

    public Long getIdorder() {
        return idorder;
    }

    public void setIdorder(Long idorder) {
        this.idorder = idorder;
    }

    public Date getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(Date ordertime) {
        this.ordertime = ordertime;
    }

    public BigDecimal getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(BigDecimal totalcost) {
        this.totalcost = totalcost;
    }

    public boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Golfer getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(Golfer idgolfer) {
        this.idgolfer = idgolfer;
    }

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idorder != null ? idorder.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Foodorder)) {
            return false;
        }
        Foodorder other = (Foodorder) object;
        if ((this.idorder == null && other.idorder != null) || (this.idorder != null && !this.idorder.equals(other.idorder))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Foodorder[ idorder=" + idorder + " ]";
    }

    
}
