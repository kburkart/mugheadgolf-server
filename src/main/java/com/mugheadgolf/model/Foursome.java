/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalTime;

/**
 * @author kburkart
 */
@Cacheable(false)
@Entity
@Table(name = "foursome")
public class Foursome implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idfoursome")
    private Integer idfoursome;
    @Column(name = "year")
    private Integer year;
    @Column(name = "idweek")
    private Integer idweek;

    @JsonFormat(pattern = "hh:mm")
    @Column(name = "teetime")
    private LocalTime teetime;

    @JsonManagedReference
    @JoinColumn(name = "idschedule1", referencedColumnName = "idschedule")
    @OneToOne(fetch = FetchType.EAGER)
    private Schedule idschedule1;
    @JsonManagedReference
    @JoinColumn(name = "idschedule2", referencedColumnName = "idschedule")
    @OneToOne
    private Schedule idschedule2;

    public Foursome() {
    }

    public Foursome(Integer idfoursome) {
        this.idfoursome = idfoursome;
    }

    public Integer getIdfoursome() {
        return idfoursome;
    }

    public void setIdfoursome(Integer idfoursome) {
        this.idfoursome = idfoursome;
    }

    public Schedule setIdschedule1() {
        return idschedule1;
    }

    public void setIdschedule1(Schedule idschedule1) {
        this.idschedule1 = idschedule1;
    }

    public Schedule setIdschedule2() {
        return idschedule2;
    }

    public void setIdschedule2(Schedule idschedule2) {
        this.idschedule2 = idschedule2;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getIdweek() {
        return idweek;
    }

    public void setIdweek(Integer idweek) {
        this.idweek = idweek;
    }

    public LocalTime getTeetime() {
        return teetime;
    }

    public void setTeetime(LocalTime teetime) {
        this.teetime = teetime;
    }

    public Schedule getIdschedule1() {
        return idschedule1;
    }

    public Schedule getIdschedule2() {
        return idschedule2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfoursome != null ? idfoursome.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Foursome)) {
            return false;
        }
        Foursome other = (Foursome) object;
        if ((this.idfoursome == null && other.idfoursome != null) || (this.idfoursome != null && !this.idfoursome.equals(other.idfoursome))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Foursome[ idfoursome=" + idfoursome + " ]";
    }

}
