/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "games")
public class Games implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idgame")
    private Integer idgame;
    @Size(max = 45)
    @Column(name = "name")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private BigDecimal amount;
    @JsonIgnore
    @OneToMany(mappedBy = "idgame")
    private Collection<Money> moneyCollection;

    public Games() {
    }

    public Games(Integer idgame) {
        this.idgame = idgame;
    }

    public Integer getIdgame() {
        return idgame;
    }

    public void setIdgame(Integer idgame) {
        this.idgame = idgame;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @XmlTransient
    public Collection<Money> getMoneyCollection() {
        return moneyCollection;
    }

    public void setMoneyCollection(Collection<Money> moneyCollection) {
        this.moneyCollection = moneyCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgame != null ? idgame.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Games)) {
            return false;
        }
        Games other = (Games) object;
        if ((this.idgame == null && other.idgame != null) || (this.idgame != null && !this.idgame.equals(other.idgame))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Games[ idgame=" + idgame + " ]";
    }
    
}
