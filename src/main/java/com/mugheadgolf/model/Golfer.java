/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.Collection;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "golfer")

public class Golfer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idgolfer")
    private Integer idgolfer;
    @Size(max = 45)
    @Column(name = "firstname")
    private String firstname;
    @Size(max = 45)
    @Column(name = "lastname")
    private String lastname;
    @Size(max = 45)
    @Column(name = "username")
    private String username;
    @Size(max = 45)
    @Column(name = "password")
    private String password;
    @Size(max = 45)
    @Column(name = "address")
    private String address;
    @Size(max = 45)
    @Column(name = "city")
    private String city;
    @Size(max = 2)
    @Column(name = "state")
    private String state;
    @Size(max = 5)
    @Column(name = "zip")
    private String zip;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 15)
    @Column(name = "phone")
    private String phone;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "email")
    private String email;
    @Size(max = 45)
    @Column(name = "spouse", nullable = true)
    private String spouse;
    @Column(name = "active")
    private boolean active;
    @Column(name = "admin")
    private boolean admin;
    @Column(name = "autorefresh")
    private boolean autorefresh;
    @Size(max = 20)
    @Column(name = "token")
    private String token;
    @Column(name = "currenthandicap")
    private Integer currenthandicap;
    @OneToMany(mappedBy = "idgolfer1")
    @JsonIgnore
    private Collection<Schedule> scheduleCollection;
    @OneToMany(mappedBy = "idgolfer2")
    @JsonIgnore
    private Collection<Schedule> scheduleCollection1;
    @OneToMany(mappedBy = "idgolfer")
    @JsonIgnore
    private Collection<Score> scoreCollection;
    @JsonIgnore
    @OneToMany(mappedBy = "idgolfer")
    private Collection<Money> moneyCollection;
    @JsonIgnore
    @OneToMany(mappedBy = "idgolfer")
    private Collection<Foodorder> foodorderCollection;

    public Collection<Foodorder> getFoodorderCollection() {
		return foodorderCollection;
	}

	public void setFoodorderCollection(Collection<Foodorder> foodorderCollection) {
		this.foodorderCollection = foodorderCollection;
	}

	public Collection<Money> getMoneyCollection() {
		return moneyCollection;
	}

	public void setMoneyCollection(Collection<Money> moneyCollection) {
		this.moneyCollection = moneyCollection;
	}

	public Golfer() {
    }

    public Golfer(Integer idgolfer) {
        this.idgolfer = idgolfer;
    }

    public Integer getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(Integer idgolfer) {
        this.idgolfer = idgolfer;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public boolean isAutorefresh() {
		return autorefresh;
	}

	public void setAutorefresh(boolean autorefresh) {
		this.autorefresh = autorefresh;
	}

	public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getCurrenthandicap() {
        return currenthandicap;
    }

    public void setCurrenthandicap(Integer currenthandicap) {
        this.currenthandicap = currenthandicap;
    }

    @XmlTransient
    public Collection<Schedule> getScheduleCollection() {
        return scheduleCollection;
    }

    public void setScheduleCollection(Collection<Schedule> scheduleCollection) {
        this.scheduleCollection = scheduleCollection;
    }

    @XmlTransient
    public Collection<Schedule> getScheduleCollection1() {
        return scheduleCollection1;
    }

    public void setScheduleCollection1(Collection<Schedule> scheduleCollection1) {
        this.scheduleCollection1 = scheduleCollection1;
    }

    @XmlTransient
    public Collection<Score> getScoreCollection() {
        return scoreCollection;
    }

    public void setScoreCollection(Collection<Score> scoreCollection) {
        this.scoreCollection = scoreCollection;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgolfer != null ? idgolfer.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Golfer)) {
            return false;
        }
        Golfer other = (Golfer) object;
        if ((this.idgolfer == null && other.idgolfer != null) || (this.idgolfer != null && !this.idgolfer.equals(other.idgolfer))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Golfer[ idgolfer=" + idgolfer + " ]";
    }
    
}
