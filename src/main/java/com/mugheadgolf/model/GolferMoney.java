/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "golfer_money_view")

public class GolferMoney implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idgolfer")
    private int idgolfer;
    @Size(max = 91)
    @Column(name = "golfername")
    private String golfername;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Skins")
    private BigDecimal skins;
    @Column(name = "pin1_1")
    private BigDecimal pin1_1;
    @Column(name = "pin2_1")
    private BigDecimal pin2_1;
    @Column(name = "pin1_2")
    private BigDecimal pin1_2;
    @Column(name = "pin2_2")
    private BigDecimal pin2_2;
    @Column(name = "lownet1")
    private BigDecimal lownet1;
    @Column(name = "lownet2")
    private BigDecimal lownet2;
    @Column(name = "longdrive")
    private BigDecimal longdrive;
    @Column(name = "longputt")
    private BigDecimal longputt;
    @Column(name = "total")
    private BigDecimal total;
    @Column(name = "year")
    private Integer year;

    public GolferMoney() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    
    public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public int getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(int idgolfer) {
        this.idgolfer = idgolfer;
    }

    public String getGolfername() {
        return golfername;
    }

    public void setGolfername(String golfername) {
        this.golfername = golfername;
    }

    public BigDecimal getSkins() {
        return skins;
    }

    public void setSkins(BigDecimal skins) {
        this.skins = skins;
    }

    public BigDecimal getPin1_1() {
        return pin1_1;
    }

    public void setPin1_1(BigDecimal pin1_1) {
        this.pin1_1 = pin1_1;
    }

    public BigDecimal getPin2_1() {
        return pin2_1;
    }

    public void setPin2_1(BigDecimal pin2_1) {
        this.pin2_1 = pin2_1;
    }

    public BigDecimal getPin1_2() {
        return pin1_2;
    }

    public void setPin1_2(BigDecimal pin1_2) {
        this.pin1_2 = pin1_2;
    }

    public BigDecimal getPin2_2() {
        return pin2_2;
    }

    public void setPin2_2(BigDecimal pin2_2) {
        this.pin2_2 = pin2_2;
    }

    public BigDecimal getLownet1() {
        return lownet1;
    }

    public void setLownet1(BigDecimal lownet1) {
        this.lownet1 = lownet1;
    }

    public BigDecimal getLownet2() {
        return lownet2;
    }

    public void setLownet2(BigDecimal lownet2) {
        this.lownet2 = lownet2;
    }

    public BigDecimal getLongdrive() {
        return longdrive;
    }

    public void setLongdrive(BigDecimal longdrive) {
        this.longdrive = longdrive;
    }

    public BigDecimal getLongputt() {
        return longputt;
    }

    public void setLongputt(BigDecimal longputt) {
        this.longputt = longputt;
    }

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
    
}
