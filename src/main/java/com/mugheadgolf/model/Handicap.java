/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "handicap")
public class Handicap implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idhandicap")
    private Integer idhandicap;
    @Column(name = "year")
    private Integer year;
    @Column(name = "idweek")
    private Integer idweek;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "handicap")
    private BigDecimal handicap;
    @JoinColumn(name = "idgolfer", referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer idgolfer;

    public Handicap() {
    }

    public Handicap(Integer idhandicap) {
        this.idhandicap = idhandicap;
    }

    public Integer getIdhandicap() {
        return idhandicap;
    }

    public void setIdhandicap(Integer idhandicap) {
        this.idhandicap = idhandicap;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getIdweek() {
        return idweek;
    }

    public void setIdweek(Integer idweek) {
        this.idweek = idweek;
    }

    public BigDecimal getHandicap() {
        return handicap;
    }

    public void setHandicap(BigDecimal handicap) {
        this.handicap = handicap;
    }

    public Golfer getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(Golfer idgolfer) {
        this.idgolfer = idgolfer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idhandicap != null ? idhandicap.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Handicap)) {
            return false;
        }
        Handicap other = (Handicap) object;
        if ((this.idhandicap == null && other.idhandicap != null) || (this.idhandicap != null && !this.idhandicap.equals(other.idhandicap))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Handicap[ idhandicap=" + idhandicap + " ]";
    }
    
}
