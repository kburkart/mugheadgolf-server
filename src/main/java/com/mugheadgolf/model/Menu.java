/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "menu")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmenu")
    private Long idmenu;
    @Size(max = 100)
    @Column(name = "menuitem")
    private String menuitem;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cost")
    private BigDecimal cost;
    @OneToMany(mappedBy = "idmenu")
    @JsonManagedReference
    private Collection<Menuextras> menuextrasCollection;
    @JsonManagedReference
    @OneToMany(mappedBy = "idmenu")
    private Collection<Menuinstruction> menuinstructionCollection;

    public Menu() {
    }

    public Menu(Long idmenu) {
        this.idmenu = idmenu;
    }

    public Long getIdmenu() {
        return idmenu;
    }

    public void setIdmenu(Long idmenu) {
        this.idmenu = idmenu;
    }

    public String getMenuitem() {
        return menuitem;
    }

    public void setMenuitem(String menuitem) {
        this.menuitem = menuitem;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @XmlTransient
    public Collection<Menuextras> getMenuextrasCollection() {
        return menuextrasCollection;
    }

    public void setMenuextrasCollection(Collection<Menuextras> menuextrasCollection) {
        this.menuextrasCollection = menuextrasCollection;
    }

    @XmlTransient
    public Collection<Menuinstruction> getMenuinstructionCollection() {
        return menuinstructionCollection;
    }

    public void setMenuinstructionCollection(Collection<Menuinstruction> menuinstructionCollection) {
        this.menuinstructionCollection = menuinstructionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmenu != null ? idmenu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.idmenu == null && other.idmenu != null) || (this.idmenu != null && !this.idmenu.equals(other.idmenu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Menu[ idmenu=" + idmenu + " ]";
    }
    
}
