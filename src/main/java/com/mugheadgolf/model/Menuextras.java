/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "menuextras")
public class Menuextras implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmenuextra")
    private Long idmenuextra;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cost")
    private BigDecimal cost;
    @JsonBackReference 
    @JoinColumn(name = "idmenu", referencedColumnName = "idmenu")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Menu idmenu;

    public Menuextras() {
    }

    public Menuextras(Long idmenuextra) {
        this.idmenuextra = idmenuextra;
    }

    public Long getIdmenuextra() {
        return idmenuextra;
    }

    public void setIdmenuextra(Long idmenuextra) {
        this.idmenuextra = idmenuextra;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public Menu getIdmenu() {
        return idmenu;
    }

    public void setIdmenu(Menu idmenu) {
        this.idmenu = idmenu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmenuextra != null ? idmenuextra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menuextras)) {
            return false;
        }
        Menuextras other = (Menuextras) object;
        if ((this.idmenuextra == null && other.idmenuextra != null) || (this.idmenuextra != null && !this.idmenuextra.equals(other.idmenuextra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Menuextras[ idmenuextra=" + idmenuextra + " ]";
    }
    
}
