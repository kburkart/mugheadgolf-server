/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "menuinstruction")
public class Menuinstruction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmenuinstruction")
    private Long idmenuinstruction;
    @Size(max = 20)
    @Column(name = "name")
    private String name;
    @JsonBackReference
    @JoinColumn(name = "idmenu", referencedColumnName = "idmenu")
    @ManyToOne
    private Menu idmenu;
    @Column(name = "cost")
    private BigDecimal cost;

    public Menuinstruction() {
    }

    public Menuinstruction(Long idmenuinstruction) {
        this.idmenuinstruction = idmenuinstruction;
    }

    public Long getIdmenuinstruction() {
        return idmenuinstruction;
    }

    public void setIdmenuinstruction(Long idmenuinstruction) {
        this.idmenuinstruction = idmenuinstruction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Menu getIdmenu() {
        return idmenu;
    }

    public void setIdmenu(Menu idmenu) {
        this.idmenu = idmenu;
    }

    public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idmenuinstruction != null ? idmenuinstruction.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menuinstruction)) {
            return false;
        }
        Menuinstruction other = (Menuinstruction) object;
        if ((this.idmenuinstruction == null && other.idmenuinstruction != null) || (this.idmenuinstruction != null && !this.idmenuinstruction.equals(other.idmenuinstruction))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Menuinstruction[ idmenuinstruction=" + idmenuinstruction + " ]";
    }
    
}
