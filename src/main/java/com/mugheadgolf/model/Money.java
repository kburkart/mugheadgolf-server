/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "money")

public class Money implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmoney")
    private Integer idmoney;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "amount")
    private BigDecimal amount;
    @JoinColumn(name = "idgame", referencedColumnName = "idgame")
    @ManyToOne
    private Games idgame;
    @JoinColumn(name = "idgolfer", referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer idgolfer;
    @Column(name = "idweek")
    private Integer idweek;
    @Column(name = "year")
    private Integer year;

    
    public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getIdweek() {
		return idweek;
	}

	public void setIdweek(Integer idweek) {
		this.idweek = idweek;
	}

	public Money() {
    }

    public Money(Integer idmoney) {
        this.idmoney = idmoney;
    }

    public Integer getIdmoney() {
        return idmoney;
    }

    public void setIdmoney(Integer idmoney) {
        this.idmoney = idmoney;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Games getIdgame() {
        return idgame;
    }

    public void setIdgame(Games idgame) {
        this.idgame = idgame;
    }

    public Golfer getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(Golfer idgolfer) {
        this.idgolfer = idgolfer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmoney != null ? idmoney.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Money)) {
            return false;
        }
        Money other = (Money) object;
        if ((this.idmoney == null && other.idmoney != null) || (this.idmoney != null && !this.idmoney.equals(other.idmoney))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Money[ idmoney=" + idmoney + " ]";
    }
    
}
