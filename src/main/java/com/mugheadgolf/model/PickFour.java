/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "pickfour")

public class PickFour implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idpickfour")
    private Integer idpickfour;

    @JoinColumn(name = "idgolfer", referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer idgolfer;

    @JoinColumn(name = "pick1",referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer pick1;
    
    @JoinColumn(name = "pick2",referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer pick2;
    
    @JoinColumn(name = "pick3",referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer pick3;
    
    @JoinColumn(name = "pick4",referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer pick4;
    
    @Column(name = "total")
    private String total;

    @Column(name = "year")
    private Integer year;
    
    @Column(name = "score1")
    private Integer score1;
    
    @Column(name = "score2")
    private Integer score2;
    
    @Column(name = "score3")
    private Integer score3;
    
    @Column(name = "score4")
    private Integer score4;
    
    @Column(name = "week")
    private Integer week;
    
    public Integer getIdpickfour() {
		return idpickfour;
	}

	public void setIdpickfour(Integer idpickfour) {
		this.idpickfour = idpickfour;
	}

	public Golfer getPick1() {
		return pick1;
	}

	public void setPick1(Golfer pick1) {
		this.pick1 = pick1;
	}

	public Golfer getPick2() {
		return pick2;
	}

	public void setPick2(Golfer pick2) {
		this.pick2 = pick2;
	}

	public Golfer getPick3() {
		return pick3;
	}

	public void setPick3(Golfer pick3) {
		this.pick3 = pick3;
	}

	public Golfer getPick4() {
		return pick4;
	}

	public void setPick4(Golfer pick4) {
		this.pick4 = pick4;
	}
	
	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Integer getScore1() {
		return score1;
	}

	public void setScore1(Integer score1) {
		this.score1 = score1;
	}

	public Integer getScore2() {
		return score2;
	}

	public void setScore2(Integer score2) {
		this.score2 = score2;
	}

	public Integer getScore3() {
		return score3;
	}

	public void setScore3(Integer score3) {
		this.score3 = score3;
	}

	public Integer getScore4() {
		return score4;
	}

	public void setScore4(Integer score4) {
		this.score4 = score4;
	}

	public PickFour() {
    }


    public Golfer getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(Golfer idgolfer) {
        this.idgolfer = idgolfer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpickfour != null ? idpickfour.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PickFour)) {
            return false;
        }
        PickFour other = (PickFour) object;
        if ((this.idpickfour == null && other.idpickfour != null) || (this.idpickfour != null && !this.idpickfour.equals(other.idpickfour))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.PickFour[ idpickfour=" + idpickfour + " ]";
    }
    
}
