/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "totalpoints_view")
public class PointTotal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "iddivision")
    private Integer iddivision;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idgolfer")
    private int idgolfer;
    @Size(max = 91)
    @Column(name = "name")
    private String name;
    @Column(name = "year")
    private Integer year;

	@Column(name = "totalpoints")
    private BigDecimal totalpoints;

    public PointTotal() {
    }

    
    public Integer getYear() {
		return year;
	}


	public void setYear(Integer year) {
		this.year = year;
	}


	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIddivision() {
        return iddivision;
    }

    public void setIddivision(Integer iddivision) {
        this.iddivision = iddivision;
    }

    public int getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(int idgolfer) {
        this.idgolfer = idgolfer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotalpoints() {
		return totalpoints;
	}

	public void setTotalpoints(BigDecimal totalpoints) {
		this.totalpoints = totalpoints;
	}

   
    
}
