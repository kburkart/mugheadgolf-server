/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author kburkart
 */
@Entity
@Table(name = "points_view")
public class Points implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "iddivision")
    private Integer iddivision;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idgolfer")
    private int idgolfer;
    @Size(max = 91)
    @Column(name = "name")
    private String name;
    @Column(name = "week")
    private Integer week;
    @Column(name = "year")
    private Integer year;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "points")
    private BigDecimal points;

    public Points() {
    }


    public Integer getYear() {
        return year;
    }


    public void setYear(Integer year) {
        this.year = year;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIddivision() {
        return iddivision;
    }

    public void setIddivision(Integer iddivision) {
        this.iddivision = iddivision;
    }

    public int getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(int idgolfer) {
        this.idgolfer = idgolfer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

}
