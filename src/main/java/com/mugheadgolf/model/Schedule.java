/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.*;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "schedule")
public class Schedule implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idschedule")
    private Integer idschedule;
    @Column(name = "year")
    private Integer year;
    @Column(name = "idweek")
    private Integer idweek;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "points1")
    private BigDecimal points1;
    @Column(name = "points2")
    private BigDecimal points2;
    @Column(name = "absent1")
    private boolean absent1;
    @Column(name = "absent2")
    private boolean absent2;
    @Column(name = "handicap1")
    private BigDecimal handicap1;
    @Column(name = "handicap2")
    private BigDecimal handicap2;
    @Column(name = "active")
    private Integer active;
    @JoinColumn(name = "idcourse", referencedColumnName = "idcourse")
    @ManyToOne
    private Course idcourse;
    @Column(name = "backnine")
    private Integer backnine;
    @JoinColumn(name = "idgolfer1", referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer idgolfer1;
    @JoinColumn(name = "idgolfer2", referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer idgolfer2;
    @OneToMany(mappedBy = "idschedule")
    @JsonIgnore
    private Collection<Score> scoreCollection;
    @OneToOne(mappedBy = "idschedule1")
    @JsonIgnore
    private Foursome foursome;
    @OneToOne(mappedBy = "idschedule2")
    @JsonIgnore
    private Foursome foursome2;

    public Schedule() {
    }

    public Schedule(Integer idschedule) {
        this.idschedule = idschedule;
    }

    public Integer getIdschedule() {
        return idschedule;
    }

    public void setIdschedule(Integer idschedule) {
        this.idschedule = idschedule;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getIdweek() {
        return idweek;
    }

    public void setIdweek(Integer idweek) {
        this.idweek = idweek;
    }

    public BigDecimal getPoints1() {
        return points1;
    }

    public void setPoints1(BigDecimal points1) {
        this.points1 = points1;
    }

    public BigDecimal getPoints2() {
        return points2;
    }

    public void setPoints2(BigDecimal points2) {
        this.points2 = points2;
    }

    public BigDecimal getHandicap1() {
		return handicap1;
	}

	public void setHandicap1(BigDecimal handicap1) {
		this.handicap1 = handicap1;
	}

	public BigDecimal getHandicap2() {
		return handicap2;
	}

	public void setHandicap2(BigDecimal handicap2) {
		this.handicap2 = handicap2;
	}

	public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Course getIdcourse() {
        return idcourse;
    }

    public void setIdcourse(Course idcourse) {
        this.idcourse = idcourse;
    }

    public Integer getBacknine() {
		return backnine;
	}

	public void setBacknine(Integer backnine) {
		this.backnine = backnine;
	}

	public Golfer getIdgolfer1() {
        return idgolfer1;
    }

    public void setIdgolfer1(Golfer idgolfer1) {
        this.idgolfer1 = idgolfer1;
    }

    public Golfer getIdgolfer2() {
        return idgolfer2;
    }

    public void setIdgolfer2(Golfer idgolfer2) {
        this.idgolfer2 = idgolfer2;
    }

    public boolean isAbsent1() {
		return absent1;
	}

	public void setAbsent1(boolean absent1) {
		this.absent1 = absent1;
	}

	public boolean isAbsent2() {
		return absent2;
	}

	public void setAbsent2(boolean absent2) {
		this.absent2 = absent2;
	}

    @XmlTransient
    public Collection<Score> getScoreCollection() {
        return scoreCollection;
    }

    public void setScoreCollection(Collection<Score> scoreCollection) {
        this.scoreCollection = scoreCollection;
    }

    @XmlTransient
    public Foursome getFoursome() {
        return foursome;
    }
    public void setFoursome(Foursome foursome) {
        this.foursome = foursome;
    }

    @XmlTransient
    public Foursome getFoursome2() {
        return foursome2;
    }
    public void setFoursome2(Foursome foursome2) {
        this.foursome2 = foursome2;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idschedule != null ? idschedule.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Schedule)) {
            return false;
        }
        Schedule other = (Schedule) object;
        if ((this.idschedule == null && other.idschedule != null) || (this.idschedule != null && !this.idschedule.equals(other.idschedule))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Schedule[ idschedule=" + idschedule + " ]";
    }
    
}
