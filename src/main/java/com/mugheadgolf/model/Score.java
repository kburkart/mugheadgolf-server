/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "score")
public class Score implements Serializable, Comparable<Object> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idscore")
    private Integer idscore;
    @Column(name = "score")
    private Integer score;
    @Column(name = "net")
    private Integer net;
    @Column(name = "adjustedscore")
    private Integer adjustedscore;
    @Column(name = "useforhandicap")
    private Integer useforhandicap;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "handicapdifferential")
    private BigDecimal handicapdifferential;
    @JoinColumn(name = "idgolfer", referencedColumnName = "idgolfer")
    @ManyToOne(fetch=FetchType.LAZY)
    private Golfer idgolfer;
    @JoinColumn(name = "idschedule", referencedColumnName = "idschedule")
    @ManyToOne(fetch=FetchType.LAZY)
    private Schedule idschedule;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idscore")
    @JsonIgnore
    private Collection<Scorehole> scoreholeCollection;

    public Score() {
    }

    public Score(Integer idscore) {
        this.idscore = idscore;
    }

    public Integer getIdscore() {
        return idscore;
    }

    public void setIdscore(Integer idscore) {
        this.idscore = idscore;
    }


    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getAdjustedscore() {
        return adjustedscore;
    }

    public void setAdjustedscore(Integer adjustedscore) {
        this.adjustedscore = adjustedscore;
    }

    public Integer getUseforhandicap() {
        return useforhandicap;
    }

    public void setUseforhandicap(Integer useforhandicap) {
        this.useforhandicap = useforhandicap;
    }

    public BigDecimal getHandicapdifferential() {
        return handicapdifferential;
    }

    public void setHandicapdifferential(BigDecimal handicapdifferential) {
        this.handicapdifferential = handicapdifferential;
    }

    public Golfer getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(Golfer idgolfer) {
        this.idgolfer = idgolfer;
    }

    public Schedule getIdschedule() {
        return idschedule;
    }

    public void setIdschedule(Schedule idschedule) {
        this.idschedule = idschedule;
    }

    @XmlTransient
    public Collection<Scorehole> getScoreholeCollection() {
        return scoreholeCollection;
    }

    public void setScoreholeCollection(Collection<Scorehole> scoreholeCollection) {
        this.scoreholeCollection = scoreholeCollection;
    }

    
	public Integer getNet() {
		return net;
	}

	public void setNet(Integer net) {
		this.net = net;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idscore != null ? idscore.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Score)) {
            return false;
        }
        Score other = (Score) object;
        if ((this.idscore == null && other.idscore != null) || (this.idscore != null && !this.idscore.equals(other.idscore))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.webservices.Score[ idscore=" + idscore + " ]";
    }


    @Override
    public int compareTo(Object o) {
         Score other = (Score) o;
          
                 return this.getHandicapdifferential().compareTo(other.getHandicapdifferential());
                
    }

    public static Comparator<Score> OrderByHandicap = new Comparator<Score>() {
    
        // Used for sorting in ascending order of
        // handicap by subtracting net from score
        public int compare(Score a, Score b)
        {
            return ((a.score.intValue() - a.getNet().intValue()) - (b.score.intValue() - b.getNet().intValue()));
        }
    };

}
