/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "scorehole")
public class Scorehole implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idscorehole")
    private Integer idscorehole;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hole")
    private int hole;
    @Column(name = "score")
    private Integer score;
    @Column(name = "net")
    private Integer net;
    @Column(name = "stableford")
    private Integer stableford;
    @Column(name = "stableford_hc")
    private Integer stablefordHC;
    @Column(name = "adjustedscore")
    private Integer adjustedscore;
    // Type explains what the type of score
    // 1. Albatros
    // 2. Eagle
    // 3. Birdie
    // 4. Par
    // 5. Bogey
    // 6. Double Bogey
    // 7. Others
    //
    @Column(name = "type")
    private Integer type;
    @JoinColumn(name = "idscore", referencedColumnName = "idscore")
    @ManyToOne
    private Score idscore;

    public Scorehole() {
    }

    public Scorehole(Integer idscoreholes) {
        this.idscorehole = idscoreholes;
    }

    public Scorehole(Integer idscorehole, int hole) {
        this.idscorehole = idscorehole;
        this.hole = hole;
    }

    public Integer getIdscoreholes() {
        return idscorehole;
    }

    public void setIdscoreholes(Integer idscorehole) {
        this.idscorehole = idscorehole;
    }

    public int getHole() {
        return hole;
    }

    public void setHole(int hole) {
        this.hole = hole;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getAdjustedscore() {
        return adjustedscore;
    }

    public void setAdjustedscore(Integer adjustedscore) {
        this.adjustedscore = adjustedscore;
    }

    public Score getIdscore() {
        return idscore;
    }

    public void setIdscore(Score idscore) {
        this.idscore = idscore;
    }
    

    public Integer getIdscorehole() {
		return idscorehole;
	}

	public void setIdscorehole(Integer idscorehole) {
		this.idscorehole = idscorehole;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	

	public Integer getNet() {
		return net;
	}

	public void setNet(Integer net) {
		this.net = net;
	}

    public Integer getStableford() {
        return stableford;
    }

    public void setStableford(Integer stableford) {
        this.stableford = stableford;
    }

    public Integer getStablefordHC() {
        return stablefordHC;
    }

    public void setStablefordHC(Integer stablefordHC) {
        this.stablefordHC = stablefordHC;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idscorehole != null ? idscorehole.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Scorehole)) {
            return false;
        }
        Scorehole other = (Scorehole) object;
        if ((this.idscorehole == null && other.idscorehole != null) || (this.idscorehole != null && !this.idscorehole.equals(other.idscorehole))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Scorehole[ idscoreholes=" + idscorehole + " ]";
    }
    
}
