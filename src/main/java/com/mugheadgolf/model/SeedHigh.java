/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "tourney_hc")
public class SeedHigh implements Serializable {

    
	private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "golfer")
    private String golfer;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idgolfer")
    private int idgolfer;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "handicap")
    private BigDecimal handicap; 
    @Column(name = "totalpoints")
    private BigDecimal totalpoints;

    public SeedHigh() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGolfer() {
		return golfer;
	}

	public void setGolfer(String golfer) {
		this.golfer = golfer;
	}

	public int getIdgolfer() {
		return idgolfer;
	}

	public void setIdgolfer(int idgolfer) {
		this.idgolfer = idgolfer;
	}

	public BigDecimal getHandicap() {
		return handicap;
	}

	public void setHandicap(BigDecimal handicap) {
		this.handicap = handicap;
	}

	public BigDecimal getTotalpoints() {
		return totalpoints;
	}

	public void setTotalpoints(BigDecimal totalpoints) {
		this.totalpoints = totalpoints;
	}

    
}
