/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalTime;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "settings")
public class Settings implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idsettings")
    private Integer idsettings;
    @Column(name = "useghin")
    private Integer useghin;
    @Column(name = "numofrounds")
    private Integer numofrounds;
    @Column(name = "useallfortourney")
    private Integer useallfortourney;
    @Column(name = "maxallowedhc")
    private Integer maxallowedhc;
    @Column(name = "maxallowedpar3")
    private Integer maxallowedpar3;
    @Column(name = "maxallowedpar4")
    private Integer maxallowedpar4;
    @Column(name = "maxallowedpar5")
    private Integer maxallowedpar5;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "percentage")
    private BigDecimal percentage;
    @Column(name = "year")
    private Integer year;
    @Column(name = "totalweeks")
    private Integer totalweeks;
    @JoinColumn(name = "idcourse", referencedColumnName = "idcourse")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Course idcourse;
    @JoinColumn(name = "idcourse2", referencedColumnName = "idcourse")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Course idcourse2;

    @Column(name = "message")
    private String message;
    @Column(name = "showmessage")
    private boolean showMessage;

    @Column(name = "foodemail")
    private String foodemail;

	@Column(name = "foodphone")
    private String foodphone;
    @Column(name = "orderday")
    private Integer orderday;
    @Column(name = "orderstart")
    private Integer orderstart;
    @Column(name = "orderend")
    private Integer orderend;

    @Column(name = "courseemail")
    private String courseemail;

    @Column(name = "numteetimes1")
    private int numTeeTimes1;

    @Column(name = "numteetimes2")
    private int numTeeTimes2;

    @Column(name = "teetime1")
    private LocalTime teeTime1;
    @Column(name = "teetimegap1")
    private Integer teeTimeGap1;
    @Column(name = "teetime2")
    private LocalTime teeTime2;
    @Column(name = "teetimegap2")
    private Integer teeTimeGap2;


    public String getCourseemail() {
		return courseemail;
	}

	public void setCourseemail(String courseemail) {
		this.courseemail = courseemail;
	}

    public String getFoodemail() {
		return foodemail;
	}

	public void setFoodemail(String foodemail) {
		this.foodemail = foodemail;
	}

	public String getFoodphone() {
		return foodphone;
	}

	public void setFoodphone(String foodphone) {
		this.foodphone = foodphone;
	}

	public Settings() {
    }

    public Settings(Integer idsettings) {
        this.idsettings = idsettings;
    }

    public Integer getIdsettings() {
        return idsettings;
    }

    public void setIdsettings(Integer idsettings) {
        this.idsettings = idsettings;
    }

    public Integer getUseghin() {
        return useghin;
    }

    public void setUseghin(Integer useghin) {
        this.useghin = useghin;
    }

    public Integer getNumofrounds() {
        return numofrounds;
    }

    public void setNumofrounds(Integer numofrounds) {
        this.numofrounds = numofrounds;
    }


    public Integer getUseallfortourney() {
        return useallfortourney;
    }

    public void setUseallfortourney(Integer useallfortourney) {
        this.useallfortourney = useallfortourney;
    }

    public Integer getMaxallowedhc() {
        return maxallowedhc;
    }

    public void setMaxallowedhc(Integer maxallowedhc) {
        this.maxallowedhc = maxallowedhc;
    }

    public Integer getMaxallowedpar3() {
        return maxallowedpar3;
    }

    public void setMaxallowedpar3(Integer maxallowedpar3) {
        this.maxallowedpar3 = maxallowedpar3;
    }

    public Integer getMaxallowedpar4() {
        return maxallowedpar4;
    }

    public void setMaxallowedpar4(Integer maxallowedpar4) {
        this.maxallowedpar4 = maxallowedpar4;
    }

    public Integer getMaxallowedpar5() {
        return maxallowedpar5;
    }

    public void setMaxallowedpar5(Integer maxallowedpar5) {
        this.maxallowedpar5 = maxallowedpar5;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getTotalweeks() {
        return totalweeks;
    }

    public void setTotalweeks(Integer totalweeks) {
        this.totalweeks = totalweeks;
    }

    public Course getIdcourse() {
        return idcourse;
    }

    public void setIdcourse(Course idcourse) {
        this.idcourse = idcourse;
    }

    public Course getIdcourse2() {
        return idcourse2;
    }

    public void setIdcourse2(Course idcourse2) {
        this.idcourse2 = idcourse2;
    }

    public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isShowMessage() {
		return showMessage;
	}

	public void setShowMessage(boolean showMessage) {
		this.showMessage = showMessage;
	}

	public Integer getOrderday() {
		return orderday;
	}

	public void setOrderday(Integer orderday) {
		this.orderday = orderday;
	}

	public Integer getOrderstart() {
		return orderstart;
	}

	public void setOrderstart(Integer orderstart) {
		this.orderstart = orderstart;
	}

	public Integer getOrderend() {
		return orderend;
	}

	public void setOrderend(Integer orderend) {
		this.orderend = orderend;
	}

    public Integer getNumTeeTimes1() {
        return numTeeTimes1;
    }

    public void setNumTeeTimes1(Integer numTeeTimes1) {
        this.numTeeTimes1 = numTeeTimes1;
    }

    public Integer getNumTeeTimes2() {
        return numTeeTimes2;
    }

    public void setNumTeeTimes2(Integer numTeeTimes2) {
        this.numTeeTimes2 = numTeeTimes2;
    }

    public LocalTime getTeeTime1() {
        return teeTime1;
    }

    public void setTeeTime1(LocalTime teeTime1) {
        this.teeTime1 = teeTime1;
    }

    public int getTeeTimeGap1() {
        return teeTimeGap1;
    }

    public void setTeeTimeGap1(int teeTimeGap1) {
        this.teeTimeGap1 = teeTimeGap1;
    }

    public LocalTime getTeeTime2() {
        return teeTime2;
    }

    public void setTeeTime2(LocalTime teeTime2) {
        this.teeTime2 = teeTime2;
    }

    public int getTeeTimeGap2() {
        return teeTimeGap2;
    }

    public void setTeeTimeGap2(int teeTimeGap2) {
        this.teeTimeGap2 = teeTimeGap2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsettings != null ? idsettings.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Settings)) {
            return false;
        }
        Settings other = (Settings) object;
        if ((this.idsettings == null && other.idsettings != null) || (this.idsettings != null && !this.idsettings.equals(other.idsettings))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Settings[ idsettings=" + idsettings + " ]";
    }
    
}
