/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author kburkart
 */
@Entity
@Immutable
@Table(name = "stats_view")
public class Stats implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "id")
    private int id;
    @NotNull
    @Column(name = "idgolfer")
    private int idgolfer;
    @Column(name = "name")
    private String name;
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "year")
    private int year;
    @NotNull
    @Column(name = "Albatros")
    private int albatros;
    @NotNull
    @Column(name = "Eagle")
    private int eagle;
    @NotNull
    @Column(name = "Birdie")
    private int birdie;
    @NotNull
    @Column(name = "Par")
    private int par;
    @NotNull
    @Column(name = "Bogey")
    private int bogey;
    @NotNull
    @Column(name = "Double")
    private int doubleBogey;
    @NotNull
    @Column(name = "Other")
    private int other;

    public Stats() {
    }

    public int getIdgolfer() {
        return idgolfer;
    }

    public void setIdgolfer(int idgolfer) {
        this.idgolfer = idgolfer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getAlbatros() {
        return albatros;
    }

    public void setAlbatros(int albatros) {
        this.albatros = albatros;
    }

    public int getEagle() {
        return eagle;
    }

    public void setEagle(int eagle) {
        this.eagle = eagle;
    }

    public int getBirdie() {
        return birdie;
    }

    public void setBirdie(int birdie) {
        this.birdie = birdie;
    }

    public int getPar() {
        return par;
    }

    public void setPar(int par) {
        this.par = par;
    }

    public int getBogey() {
        return bogey;
    }

    public void setBogey(int bogey) {
        this.bogey = bogey;
    }

    public int getDoubleBogey() {
        return doubleBogey;
    }

    public void setDoubleBogey(int doubleBogey) {
        this.doubleBogey = doubleBogey;
    }

    public int getOther() {
        return other;
    }

    public void setOther(int other) {
        this.other = other;
    }
    
}
