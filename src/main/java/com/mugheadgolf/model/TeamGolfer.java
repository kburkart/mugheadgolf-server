package com.mugheadgolf.model;

import javax.persistence.*;

@Entity
@Table(name = "teamgolfer")
public class TeamGolfer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "team_golfer_id")
    private Integer teamGolferId;

    @JoinColumn(name = "team_id", referencedColumnName = "team_id")
    @ManyToOne(fetch=FetchType.LAZY)
    private Team teamId;

    @JoinColumn(name = "idgolfer", referencedColumnName = "idgolfer")
    @ManyToOne(fetch=FetchType.LAZY)
    private Golfer idgolfer;

    public Integer getTeamGolferId() {
        return this.teamGolferId;
    }

    public void setTeamGolferId(Integer teamGolferId) {
        this.teamGolferId = teamGolferId;
    }

    public Team getTeamId() {
        return this.teamId;
    }

    public void setTeamId(Team teamId) {
        this.teamId = teamId;
    }

    public Golfer getIdgolfer() {
        return this.idgolfer;
    }

    public void setIdgolfer(Golfer idgolfer) {
        this.idgolfer = idgolfer;
    }
}
