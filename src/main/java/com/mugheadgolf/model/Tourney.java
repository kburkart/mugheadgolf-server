/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "tourney")
public class Tourney implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtourney")
    private Integer idtourney;
    @JoinColumn(name = "idgolfer", referencedColumnName = "idgolfer")
    @ManyToOne
    private Golfer idgolfer;
    @Column(name = "year")
    private Integer year;
    @Column(name = "highhandicap")
    private boolean highHandicap;
    @Column(name = "branch")
    private Integer branch; 
    @Column(name = "points")
    private BigDecimal points;
    @Column(name = "handicap")
    private BigDecimal handicap;
    @Column(name = "nextbranch")
    private Integer nextBranch;

    public Tourney() {
    }

    public Tourney(Integer idtourney) {
        this.idtourney = idtourney;
    }


    public Integer getIdtourney() {
		return idtourney;
	}

	public void setIdtourney(Integer idtourney) {
		this.idtourney = idtourney;
	}

	public Golfer getIdgolfer() {
		return idgolfer;
	}

	public void setIdgolfer(Golfer idgolfer) {
		this.idgolfer = idgolfer;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public boolean isHighHandicap() {
		return highHandicap;
	}

	public void setHighHandicap(boolean highHandicap) {
		this.highHandicap = highHandicap;
	}

	public Integer getBranch() {
		return branch;
	}

	public void setBranch(Integer branch) {
		this.branch = branch;
	}

	public BigDecimal getPoints() {
		return points;
	}

	public void setPoints(BigDecimal points) {
		this.points = points;
	}


    public BigDecimal getHandicap() {
		return handicap;
	}

	public void setHandicap(BigDecimal handicap) {
		this.handicap = handicap;
	}

	public Integer getNextBranch() {
		return nextBranch;
	}

	public void setNextBranch(Integer nextBranch) {
		this.nextBranch = nextBranch;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idtourney != null ? idtourney.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tourney)) {
            return false;
        }
        Tourney other = (Tourney) object;
        if ((this.idtourney == null && other.idtourney != null) || (this.idtourney != null && !this.idtourney.equals(other.idtourney))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Games[ idtourney=" + idtourney + " ]";
    }
    
}
