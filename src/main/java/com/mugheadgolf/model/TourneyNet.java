/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mugheadgolf.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author kburkart
 */
@Entity
@Table(name = "tourneynet")
public class TourneyNet implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "golfer")
    private String golfer;
    @Column(name = "round1")
    private Integer round1;
    @Column(name = "round2")
    private Integer round2;
    @Column(name = "round3")
    private Integer round3;
    @Column(name = "round4")
    private Integer round4;
    @Column(name = "topar")
    private Integer toPar;
    

    public TourneyNet() {
    }

    public TourneyNet(Integer id) {
        this.id = id;
    }


    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGolfer() {
		return golfer;
	}

	public void setGolfer(String golfer) {
		this.golfer = golfer;
	}

	public Integer getRound1() {
		return round1;
	}

	public void setRound1(Integer round1) {
		this.round1 = round1;
	}

	public Integer getRound2() {
		return round2;
	}

	public void setRound2(Integer round2) {
		this.round2 = round2;
	}

    public Integer getRound3() {
        return round3;
    }

    public void setRound3(Integer round3) {
        this.round3 = round3;
    }

    public Integer getRound4() {
        return round4;
    }

    public void setRound4(Integer round4) {
        this.round4 = round4;
    }

	public Integer getToPar() {
		return toPar;
	}

	public void setToPar(Integer toPar) {
		this.toPar = toPar;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TourneyNet)) {
            return false;
        }
        TourneyNet other = (TourneyNet) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mugheadgolf.model.Games[ idtourney=" + id + " ]";
    }
    
}
