package com.mugheadgolf.repository;

import com.mugheadgolf.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Integer> {
}
