package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Courseholes;

public interface CourseholesRepository extends JpaRepository<Courseholes, Integer> {
	
    public List<Courseholes> findAllByIdcourse(Course idcourse);
    public List<Courseholes> findAllByIdcourseOrderByHole(Course idcourse);


}
