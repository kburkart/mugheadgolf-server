package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.DivisionGolfer;

public interface DivisionGolferRepository extends JpaRepository<DivisionGolfer, Integer> {
	List<DivisionGolfer> findAllByIddivision(Integer iddivision);

	DivisionGolfer findFirstByIdgolferAndYear(Integer idgolfer, Integer year);
}
