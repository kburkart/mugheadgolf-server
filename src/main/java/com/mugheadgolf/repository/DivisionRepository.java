package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.Division;

public interface DivisionRepository extends JpaRepository<Division, Integer> {
	List<Division> findAllByYear(Integer year);
}
