package com.mugheadgolf.repository;

import com.mugheadgolf.model.Foursome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FoursomeRepository extends JpaRepository<Foursome, Integer> {
    List<Foursome> findAllByIdweekAndYearOrderByTeetime(Integer week, Integer year);
    List<Foursome> findAllByYear(Integer year);
    @Query("SELECT f FROM Foursome f, Schedule s, Course c WHERE f.year = :year and f.idweek = :week AND c.courseName =:name AND f.idschedule1 = s.idschedule AND s.idcourse = c.idcourse order by f.teetime")
    List<Foursome> findAllByIdweekAndYearAndNameOrderByTeetime(Integer week, Integer year, String name);

}
