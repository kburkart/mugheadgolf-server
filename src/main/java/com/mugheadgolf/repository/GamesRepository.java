package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.Games;

public interface GamesRepository extends JpaRepository<Games, Integer> {

	public  List<Games> findAllByOrderByIdgame();
}
