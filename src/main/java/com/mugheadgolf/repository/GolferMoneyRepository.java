package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.GolferMoney;

public interface GolferMoneyRepository extends JpaRepository<GolferMoney, Integer> {
	
	GolferMoney findFirstByIdgolferAndYear(Integer idgolfer, Integer year);
	List<GolferMoney> findAllByYear(Integer year);

}
