package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mugheadgolf.model.Golfer;

public interface GolferRepository extends JpaRepository<Golfer, Integer> {

    public Golfer findFirstByUsernameAndPassword(String username, String password);
    public Golfer findFirstByToken(String token);
    public List<Golfer> findAllByActive(boolean active);
    public Golfer findFirstByPhone(String phone);
    public Golfer findFirstByLastname(String lastname);
    public Golfer findFirstByFirstnameAndLastname(String firstname, String lastname);

    @Query("SELECT g.email FROM Golfer g WHERE g.active = true") 
    public List<String> getActiveGolferEmails();

}
