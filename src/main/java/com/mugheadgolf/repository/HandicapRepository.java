package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Handicap;

public interface HandicapRepository extends JpaRepository<Handicap, Integer> {

	public Handicap findFirstByIdweekAndIdgolferAndYear(Integer idweek, Golfer idgolfer, Integer year);
	public List<Handicap> findAllByYearAndIdgolferOrderByIdweek(Integer year, Golfer idgolfer);

}
