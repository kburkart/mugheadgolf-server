package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Menu;
import com.mugheadgolf.model.Money;
import com.mugheadgolf.model.Score;

public interface MenuRepository extends JpaRepository<Menu, Integer> {
	
}
