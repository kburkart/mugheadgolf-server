package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Money;
import com.mugheadgolf.model.Score;

public interface MoneyRepository extends JpaRepository<Money, Integer> {
	
	List<Money> findAllByIdweekAndYearOrderByIdgame(Integer idweek, Integer year);
	Long deleteByIdweekAndYear(Integer idweek, Integer year);
	
	@Query("SELECT coalesce(max(m.idweek) + 1, 1) FROM Money m where m.year = :year")
	Integer getCurrentWeek(@Param("year")Integer year);
	
}
