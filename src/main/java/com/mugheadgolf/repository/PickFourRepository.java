package com.mugheadgolf.repository;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.PickFour;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PickFourRepository extends JpaRepository<PickFour, Integer> {

    List<PickFour> findAllByWeekAndYearOrderByTotal(Integer week, Integer year);

    PickFour findFirstByWeekAndYearAndIdgolfer(Integer week, Integer year, Golfer golfer);

    void deleteByIdgolferAndYearAndWeek(Golfer golfer, Integer year, Integer week);


}
