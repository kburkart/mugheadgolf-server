package com.mugheadgolf.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Points;
import com.mugheadgolf.model.Score;

public interface PointsRepository extends JpaRepository<Points, Integer> {
	List<Points> findAllByIdgolferAndYear(Integer idgolfer, Integer year);
	
	Points findFirstByIdgolferAndWeekAndYear(Integer idgolfer, Integer week, Integer year);
	

	@Query(nativeQuery = true, value = "SELECT SUM(pv.points) from points_view pv WHERE pv.idgolfer = :idgolfer AND pv.year = :year")
	BigDecimal pointsForGolfer(@Param("idgolfer")Integer idgolfer, @Param("year")Integer year);
	
}
