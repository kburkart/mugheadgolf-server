package com.mugheadgolf.repository;

import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {
    public Schedule getOne(Integer idschedule);

    public Schedule findFirstByIdschedule(Integer idschedule);

    public List<Schedule> findAllByYear(Integer year);

    public List<Schedule> findAllByIdweekAndYear(Integer idweek, Integer year);

    public List<Schedule> findAllByIdweekAndYearAndIdcourse(Integer idweek, Integer year, Course course);

    @Query("SELECT s FROM Schedule s WHERE s.year = :year and (s.idgolfer1 = :golfer or s.idgolfer2 = :golfer) order by s.idweek")
    public List<Schedule> getGolferSchedule(@Param("year") Integer year, @Param("golfer") Golfer golfer);

    @Query("SELECT s FROM Schedule s WHERE s.year = :year and s.idweek = :week and (s.idgolfer1 = :golfer or s.idgolfer2 = :golfer)")
    public Schedule getGolferScheduleByWeek(@Param("year") Integer year, @Param("week") Integer week, @Param("golfer") Golfer golfer);

}

