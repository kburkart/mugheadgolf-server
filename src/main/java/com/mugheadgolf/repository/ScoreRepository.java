package com.mugheadgolf.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.model.Score;

public interface ScoreRepository extends JpaRepository<Score, Integer> {

	@Query(nativeQuery = true, value = "SELECT s FROM Score s, Scorehole sc WHERE s.idscore = sc.idscore and s.idscore = :idscore")
	public Score findByIdscore(Integer idscore);

//	@Query("SELECT s FROM Score s, Schedule sc WHERE s.idgolfer = :golfer and s.useforhandicap = :useforhandicap and s.idschedule = sc.idschedule and s.score > 0 and ((sc.year = 2020 and sc.idweek > :week) or(sc.year = 2021 and sc.idweek <= :week)) order by sc.year desc, sc.idweek desc")
//	public List<Score> getHandicapScores(@Param("golfer")Golfer golfer, @Param("useforhandicap")int useforhandicap,  @Param("week")int week);

	@Query("SELECT s FROM Score s, Schedule sc WHERE s.idgolfer = :golfer and s.useforhandicap = :useforhandicap and s.idschedule = sc.idschedule and s.score > 0 order by sc.year desc, sc.idweek desc")
	public List<Score> getHandicapScores(@Param("golfer")Golfer golfer, @Param("useforhandicap")int useforhandicap);

	@Query("SELECT s FROM Score s, Schedule sc WHERE s.idgolfer = :golfer and s.useforhandicap = :useforhandicap and s.idschedule = sc.idschedule and s.score > 0 and sc.year >= :year order by sc.year desc, sc.idweek desc")
	public List<Score> getHandicapScoresYear(@Param("golfer")Golfer golfer, @Param("useforhandicap")int useforhandicap,  @Param("year")int year);

	public Score findFirstByIdscheduleAndIdgolfer(Schedule idschedule, Golfer idgolfer);

	public List<Score> findAllByIdscheduleOrderByIdscore(Schedule idschedule);

	@Query("SELECT s FROM Score s, Schedule sc WHERE s.idschedule = sc.idschedule and sc.year = :year and sc.idweek = :idweek and s.score > 0")
	public List<Score> findAllByYearAndIdweek(@Param("year")Integer year, @Param("idweek")Integer idweek);

	@Query("SELECT s FROM Score s, Schedule sc WHERE s.idschedule = sc.idschedule and sc.year = :year and s.idgolfer = :golfer order by sc.idweek")
	public List<Score> getGolferScores(@Param("year")int year, @Param("golfer")Golfer golfer);

	@Query("SELECT s FROM Score s, Schedule sc WHERE s.idschedule = sc.idschedule and s.useforhandicap = 1 and s.score > 0 and sc.year = :year and s.idgolfer = :golfer order by sc.idweek")
	public List<Score> getGolferScoresForHC(@Param("year")int year, @Param("golfer")Golfer golfer);

	@Query("SELECT s FROM Score s, Schedule sc WHERE s.idschedule = sc.idschedule and s.useforhandicap = 1 and s.score > 0 and s.idgolfer = :golfer order by sc.year, sc.idweek")
	public List<Score> getAllGolferScores(@Param("golfer")Golfer golfer);

	@Query("SELECT s FROM Score s, Schedule sc WHERE s.idschedule = sc.idschedule and s.useforhandicap = 1 and s.score > 0 and s.idgolfer = :golfer and sc.year = :year order by sc.idweek")
	public List<Score> getAllGolferScoresByYear(@Param("golfer")Golfer golfer, @Param("year")int year);


	@Query("SELECT coalesce(AVG(s.score), 0) FROM Score s, Schedule sc WHERE s.useforhandicap = 1 and s.idschedule = sc.idschedule and sc.year = :year and s.score > 0")
    public BigDecimal getLeaqueAvg(@Param("year")int year);
	
	@Query("SELECT coalesce(AVG(s.score), 0) FROM Score s, Schedule sc WHERE s.idgolfer.idgolfer = :id and s.useforhandicap = 1 and s.idschedule = sc.idschedule and sc.year = :year  and s.score > 0")
    public BigDecimal getGolferAvg(@Param("year")int year, @Param("id")int id);
	
	@Query("select coalesce(max(sch.idweek), 0) from Schedule sch, Score s, Scorehole sh where s.idschedule = sch.idschedule and s.idscore = sh.idscore and sch.year = :year")
	public int latestScoreWeek(@Param("year")int year);
	
}

