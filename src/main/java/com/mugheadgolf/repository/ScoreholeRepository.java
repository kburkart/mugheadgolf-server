package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.Score;
import com.mugheadgolf.model.Scorehole;


public interface ScoreholeRepository extends JpaRepository<Scorehole, Integer> {
    List<Scorehole> findAllByIdscoreOrderByHole(Score idscore);
}

