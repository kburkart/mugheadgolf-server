package com.mugheadgolf.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.SeedHigh;

public interface SeedHighRepository extends JpaRepository<SeedHigh, Integer> {

	
}
