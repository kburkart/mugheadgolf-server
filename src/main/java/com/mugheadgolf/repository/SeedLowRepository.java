package com.mugheadgolf.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.SeedLow;

public interface SeedLowRepository extends JpaRepository<SeedLow, Integer> {

	
}
