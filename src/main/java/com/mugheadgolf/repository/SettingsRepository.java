package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mugheadgolf.model.Settings;

public interface SettingsRepository extends JpaRepository<Settings, Integer> {

    public Settings findFirstByYear(Integer year);


}
