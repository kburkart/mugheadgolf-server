package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.Stats;

public interface StatsRepository extends JpaRepository<Stats, Integer> {
	
    public List<Stats> findAllByYear( Integer year);
   
}
