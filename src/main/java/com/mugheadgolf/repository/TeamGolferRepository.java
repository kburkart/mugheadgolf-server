package com.mugheadgolf.repository;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Team;
import com.mugheadgolf.model.TeamGolfer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeamGolferRepository extends JpaRepository<TeamGolfer, Integer> {
    List<TeamGolfer> findAllByTeamId(Team team);
}
