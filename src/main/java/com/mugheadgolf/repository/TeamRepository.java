package com.mugheadgolf.repository;

import com.mugheadgolf.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TeamRepository extends JpaRepository<Team, Integer> {
    List<Team> findAllByYear(Integer year);
}
