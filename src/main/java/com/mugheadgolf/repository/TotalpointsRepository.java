package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.PointTotal;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Points;

public interface TotalpointsRepository extends JpaRepository<PointTotal, Integer> {
	List<PointTotal> findAllByIdgolferAndYear(Integer idgolfer, Integer year);
	PointTotal findFirstByIddivision(Integer iddivision);
	
}
