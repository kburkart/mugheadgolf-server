package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.TourneyNet;


public interface TourneyNetRepository extends JpaRepository<TourneyNet, Integer> {

	public List<TourneyNet> findAllByOrderByToPar();

}
