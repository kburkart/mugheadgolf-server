package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.Tourney;


public interface TourneyRepository extends JpaRepository<Tourney, Integer> {

	public List<Tourney> findAllByHighHandicapOrderByBranch(Boolean highHandicap);

}
