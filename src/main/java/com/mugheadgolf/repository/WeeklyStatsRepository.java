package com.mugheadgolf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mugheadgolf.model.WeeklyStats;

public interface WeeklyStatsRepository extends JpaRepository<WeeklyStats, Integer> {
	
    public List<WeeklyStats> findAllByIdgolferAndYearOrderByIdweek(Integer idgolfer, Integer year);


}
