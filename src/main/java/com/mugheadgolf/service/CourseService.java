package com.mugheadgolf.service;

import com.mugheadgolf.data.CourseData;

public interface CourseService {
	
	public CourseData getCourseData(Integer idcourse);
	
}
