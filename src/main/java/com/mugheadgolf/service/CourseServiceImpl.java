package com.mugheadgolf.service;

import com.mugheadgolf.data.CourseData;
import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Courseholes;
import com.mugheadgolf.repository.CourseRepository;
import com.mugheadgolf.repository.CourseholesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toCollection;

@Service("courseService")
public class CourseServiceImpl implements CourseService{

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private CourseholesRepository courseholesRepository;



	@Override
	public CourseData getCourseData(Integer idcourse) {
		CourseData data = new CourseData();
		Course course = courseRepository.getOne(idcourse);
		List<Courseholes> courseholes = courseholesRepository.findAllByIdcourseOrderByHole(course);
		data.setCourse(course);
		data.setCourseholes(courseholes);
		return data;
	}


}

	


