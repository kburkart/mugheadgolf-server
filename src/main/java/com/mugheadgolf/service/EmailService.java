package com.mugheadgolf.service;

import com.mugheadgolf.data.EmailMessage;

public interface EmailService {
	public boolean send(EmailMessage message);

	public boolean sendWeeklySchedule();
	public boolean sendGolfersWeeklySchedule();
	public void sendUpdatedWeeklySchedule();
}
