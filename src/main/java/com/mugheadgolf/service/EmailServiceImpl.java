package com.mugheadgolf.service;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;

import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Foursome;
import com.mugheadgolf.repository.FoursomeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.mugheadgolf.data.EmailMessage;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.repository.SettingsRepository;
import com.mugheadgolf.utilities.Communicate;

@Slf4j
@Service("emailService")
public class EmailServiceImpl implements EmailService {

	Timer timer = null;

	@Autowired
	Environment env;

	@Autowired
	private GolferRepository golferRepository;

	@Autowired
	private ScheduleService scheduleService;

	@Autowired
	private FoursomeRepository foursomeRepository;

	@Autowired
	private SettingsRepository settingsRepository;

	@Override
	public boolean send(EmailMessage message) {

		List<String> golfers = golferRepository.getActiveGolferEmails();
		return Communicate.sendEmail(golfers, message.getSubject(), message.getMessage(), false);
	
	}

	@Override
	public boolean sendWeeklySchedule() {
		LocalDate now = LocalDate.now();
		int year = now.getYear();
		Settings settings = settingsRepository.findFirstByYear(year);
		String weeklySchedule = generateSchedule();
		List<String> recipients = golferRepository.getActiveGolferEmails();
		recipients.add("kevin.burkart@gmail.com");
		//recipients.add(settings.getCourseemail());
		String subject = "Mughead Golf Weekly Schedule";
		return Communicate.sendEmail(recipients,subject, weeklySchedule, true);

	}

	@Override
	public boolean sendGolfersWeeklySchedule() {
		log.debug("sending golfer weekly schedule");
		LocalDate now = LocalDate.now();
		int year = now.getYear();
		Settings settings = settingsRepository.findFirstByYear(year);
		String weeklySchedule = generateSchedule();
		List<String> recipients = golferRepository.getActiveGolferEmails();
		if(!Arrays.stream(env.getActiveProfiles()).anyMatch(
				env -> (env.equalsIgnoreCase("prod")) )) {
			recipients.clear();
			log.debug("dev environment only send to kevin burkart");
			recipients.add("kevin.burkart@gmail.com");
		}
		String subject = "Updated Mughead Golf Weekly Schedule";
		return Communicate.sendEmail(recipients,subject, weeklySchedule, true);

	}


	@Override
	public void sendUpdatedWeeklySchedule(){
		log.debug("Send Update started");
		if(timer != null){
			log.debug("old timer canceled");

			timer.cancel();
		}
		timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				sendGolfersWeeklySchedule();
			}
		}, 1000 * 60 * 5 );  //5 minute email delay
	}
	
	private String generateSchedule() {


		final StringBuilder weeklySchedule = new StringBuilder();
		weeklySchedule.append("<font size=\"5\"><div <div style=\"text-align:center; line-height: 150%;\"><H1>MUGHEAD GOLF</H1><br><br>");
		LocalDate now = LocalDate.now();
		int year = now.getYear();

		Settings settings = settingsRepository.findFirstByYear(year);
		Course course1 = settings.getIdcourse();
		Course course2 = settings.getIdcourse2();

		List<Schedule> schedules = scheduleService.getWeek(year, scheduleService.getCurrentWeek(year));
		int week = scheduleService.getCurrentWeek(year);
		List<Foursome> foursomes1 = foursomeRepository.findAllByIdweekAndYearAndNameOrderByTeetime(week,year, course1.getCourseName());
		List<Foursome> foursomes2 = foursomeRepository.findAllByIdweekAndYearAndNameOrderByTeetime(week,year, course2.getCourseName());

		weeklySchedule.append("<div><H2>" + course1.getCourseName() + "</H2></div>" );
		foursomes1.forEach(foursome -> {
			Schedule schedule1 = foursome.getIdschedule1();
			Schedule schedule2 = foursome.getIdschedule2();
			if(schedule1 != null){
				if(schedule2 == null){
					if(!(schedule1.isAbsent1() && schedule1.isAbsent2())){
						weeklySchedule.append("<div><b>" + foursome.getTeetime() + "</b></div>");
					}
				}
				else{
					if(!(schedule1.isAbsent1() && schedule1.isAbsent2() && schedule2.isAbsent1() && schedule2.isAbsent2())){
						weeklySchedule.append("<div><b>" + foursome.getTeetime() + "</b></div>");
					}
				}
			}
			else{
				if(schedule2 != null)
					if(!(schedule2.isAbsent1() && schedule2.isAbsent2())){
						weeklySchedule.append("<div><b>" + foursome.getTeetime() + "</b></div>");
					}
			}
			if(schedule1 != null) {
				if(!schedule1.isAbsent1()) {
					weeklySchedule.append("(" + schedule1.getIdgolfer1().getCurrenthandicap() + ")" + schedule1.getIdgolfer1().getFirstname() + " " + schedule1.getIdgolfer1().getLastname());
				}
				if(!(schedule1.isAbsent1() || schedule1.isAbsent2())) {
					weeklySchedule.append(" VS. ");
				}
				if(!schedule1.isAbsent2()) {
					weeklySchedule.append(schedule1.getIdgolfer2().getFirstname() + " " + schedule1.getIdgolfer2().getLastname() + "(" + schedule1.getIdgolfer2().getCurrenthandicap() + ")");
				}
				if(!((schedule1.isAbsent1() && schedule1.isAbsent2()) || (schedule2.isAbsent1() && schedule2.isAbsent2()))) {
					weeklySchedule.append("<br><div>AND</div>");
				}
			}
			if(schedule2 != null) {
				if(!schedule2.isAbsent1()) {
					weeklySchedule.append("(" + schedule2.getIdgolfer1().getCurrenthandicap() + ")" + schedule2.getIdgolfer1().getFirstname() + " " + schedule2.getIdgolfer1().getLastname());
				}

				if(!(schedule2.isAbsent1() || schedule2.isAbsent2())) {
					weeklySchedule.append(" VS. ");
				}
				if(!schedule2.isAbsent2()) {
					weeklySchedule.append(schedule2.getIdgolfer2().getFirstname() + " " + schedule2.getIdgolfer2().getLastname() + "(" + schedule2.getIdgolfer2().getCurrenthandicap() + ")<br><br>");
				}
			}
		});
		weeklySchedule.append("</br></br><div><H2>" + course2.getCourseName() + "</H2></div>" );
		foursomes2.forEach(foursome -> {
			Schedule schedule1 = foursome.getIdschedule1();
			Schedule schedule2 = foursome.getIdschedule2();
			if(schedule1 != null){
				if(schedule2 == null){
					if(!(schedule1.isAbsent1() && schedule1.isAbsent2())){
						weeklySchedule.append("<div><b>" + foursome.getTeetime() + "</b></div>");
					}
				}
				else{
					if(!(schedule1.isAbsent1() && schedule1.isAbsent2() && schedule2.isAbsent1() && schedule2.isAbsent2())){
						weeklySchedule.append("<div><b>" + foursome.getTeetime() + "</b></div>");
					}
				}
			}
			else{
				if(schedule2 != null)
					if(!(schedule2.isAbsent1() && schedule2.isAbsent2())){
						weeklySchedule.append("<div><b>" + foursome.getTeetime() + "</b></div>");
					}
			}
			if(schedule1 != null) {
				if(!schedule1.isAbsent1()) {
					weeklySchedule.append("(" + schedule1.getIdgolfer1().getCurrenthandicap() + ")" + schedule1.getIdgolfer1().getFirstname() + " " + schedule1.getIdgolfer1().getLastname());
				}
				if(!(schedule1.isAbsent1() || schedule1.isAbsent2())) {
					weeklySchedule.append(" VS. ");
				}
				if(!schedule1.isAbsent2()) {
					weeklySchedule.append(schedule1.getIdgolfer2().getFirstname() + " " + schedule1.getIdgolfer2().getLastname() + "(" + schedule1.getIdgolfer2().getCurrenthandicap() + ")");
				}
				if(!((schedule1.isAbsent1() && schedule1.isAbsent2()) || (schedule2.isAbsent1() && schedule2.isAbsent2()))) {
					weeklySchedule.append("<br><div>AND</div>");
				}
			}
			if(schedule2 != null) {
				if(!schedule2.isAbsent1()) {
					weeklySchedule.append("(" + schedule2.getIdgolfer1().getCurrenthandicap() + ")" + schedule2.getIdgolfer1().getFirstname() + " " + schedule2.getIdgolfer1().getLastname());
				}

				if(!(schedule2.isAbsent1() || schedule2.isAbsent2())) {
					weeklySchedule.append(" VS. ");
				}
				if(!schedule2.isAbsent2()) {
					weeklySchedule.append(schedule2.getIdgolfer2().getFirstname() + " " + schedule2.getIdgolfer2().getLastname() + "(" + schedule2.getIdgolfer2().getCurrenthandicap() + ")<br><br>");
				}
			}
		});

		weeklySchedule.append("</div><br><br><b>OUT:</b><br>");
		schedules.forEach(schedule -> {
			if(schedule.isAbsent1()) {
				weeklySchedule.append(schedule.getIdgolfer1().getFirstname() + " " + schedule.getIdgolfer1().getLastname() + "<br>");
			}
			if(schedule.isAbsent2()) {
				weeklySchedule.append(schedule.getIdgolfer2().getFirstname() + " " + schedule.getIdgolfer2().getLastname() + "<br>");
			}
		});		
		weeklySchedule.append("</font>");
		return weeklySchedule.toString();
		
	}

}
