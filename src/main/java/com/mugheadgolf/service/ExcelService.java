package com.mugheadgolf.service;

import java.io.IOException;

public interface ExcelService {
    public byte[] generateExcel() throws IOException;
}
