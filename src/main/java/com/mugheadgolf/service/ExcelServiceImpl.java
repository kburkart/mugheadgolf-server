package com.mugheadgolf.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@Slf4j
@Service
public class ExcelServiceImpl implements ExcelService {

    public byte[] generateExcel() throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet standings = workbook.createSheet("Standings 2022");
        Sheet money = workbook.createSheet("Money 2022");
        Sheet handicap = workbook.createSheet("Handicaps 2022");

        XSSFFont headerFont = buildFont(workbook, "Calibri", 11, true);
        XSSFFont cellFont = buildFont(workbook, "Calibri", 11, false);
        XSSFFont cellBoldFont = buildFont(workbook, "Calibri", 11, true);

        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFont(headerFont);
        headerStyle.setWrapText(true);
        headerStyle.setVerticalAlignment(VerticalAlignment.TOP);

        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(cellFont);
        cellStyle.setWrapText(true);
        cellStyle.setVerticalAlignment(VerticalAlignment.TOP);

        // Add initial search criteria
//        Row row = standings.createRow(0);
//        Cell cell = row.createCell(0);
//        cell.setCellValue("Initial Search Criteria");
//        cell.setCellStyle(headerStyle);
//        cell = row.createCell(1);
//        XSSFRichTextString cellValue = buildCriteriaCellValue(excelRequest.getInitialSearchCriteria(), cellBoldFont, cellFont);
//        cell.setCellValue(cellValue);
//        cell.setCellStyle(cellStyle);
//        row.setHeight((short) -1);
//        sheet.addMergedRegion(new CellRangeAddress(0,0,1,10));

        // Add refined search criteria, if any are applied
//        boolean hasRefinedSearchCriteria = excelRequest.getRefinedSearchCriteria() != null && !excelRequest.getRefinedSearchCriteria().isEmpty();
//        if (hasRefinedSearchCriteria) {
//            row = sheet.createRow(1);
//            cell = row.createCell(0);
//            cell.setCellValue("Refined Search Criteria");
//            cell.setCellStyle(headerStyle);
//            cell = row.createCell(1);
//            cellValue = buildCriteriaCellValue(excelRequest.getRefinedSearchCriteria(), cellBoldFont, cellFont);
//            cell.setCellValue(cellValue);
//            cell.setCellStyle(cellStyle);
//            row.setHeight((short) -1);
//            sheet.addMergedRegion(new CellRangeAddress(1,1,1,10));
//        }

        // Add headers and rows
//            FormsExcelRequest formsExcelRequest = (FormsExcelRequest) excelRequest;
//            int headerRowNumber = hasRefinedSearchCriteria ? 3 : 2;
//            createFormsHeaders(headerRowNumber, sheet, headerStyle);
//            if (formsExcelRequest.getPdcFlatForms() != null) {
//                int rowNumber = headerRowNumber + 1;
//                for(PdcFlatForm form : formsExcelRequest.getPdcFlatForms()) {
//                    createFormsRow(rowNumber++, form, sheet, cellStyle, headerFont, cellFont);
//                }
//            }
//        }
//        // Adjust each column width to fit the content
//        IntStream.range(0, 11).forEach(sheet::autoSizeColumn);
//
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        workbook.write(byteArrayOutputStream);
        workbook.close();
        return byteArrayOutputStream.toByteArray();
    }

    private XSSFFont buildFont(Workbook workbook, String fontName, int fontHeight, boolean isBold) {
        XSSFFont font = ((XSSFWorkbook) workbook).createFont();
        font.setFontName(fontName);
        font.setFontHeightInPoints((short) fontHeight);
        font.setBold(isBold);
        return font;
    }

    /**
     * Given a search criteria map, return the rich text representation for search criteria.
     * @param searchCriteria map of search criteria, where the key is the filter and the value is the list of filter values
     * @param criteriaKeyFont font to apply to each filter
     * @param criteriaValueFont font to apply to each filter value
     * @return the rich text representation for search criteria
     */
    private XSSFRichTextString buildCriteriaCellValue(Map<String, List<String>> searchCriteria, XSSFFont criteriaKeyFont, XSSFFont criteriaValueFont) {
        XSSFRichTextString searchCriteriaString = new XSSFRichTextString();
        searchCriteria.keySet()
                      .forEach(key -> {
                          searchCriteriaString.append(key + ": ", criteriaKeyFont);
                          searchCriteriaString.append(String.join(", ", searchCriteria.get(key)) + " ", criteriaValueFont);
                      });

       return searchCriteriaString;
    }

    /**
     * Given a string containing the product/class combinations, return the rich text representation for products.
     * Currently, each product is bolded and contains each of its matching product classes beneath it.
     * @param combinedProductClasses formatted as "product.class|product.class", with "." and '|' separators
     * @param productFont the font for each product
     * @param productClassFont the font for each product class
     * @return the rich text representation for products
     */
    private XSSFRichTextString buildProductCellValue(String combinedProductClasses, XSSFFont productFont, XSSFFont productClassFont) {
        XSSFRichTextString productCellValue = new XSSFRichTextString();
        if(isEmpty(combinedProductClasses)) {
            return productCellValue;
        }
        Map<String, Set<String>> productClassMap = new HashMap<>();
        Arrays.stream(combinedProductClasses.split("\\|"))
              .forEach(s -> {
                  String[] parts = s.split("\\.");
                  if (productClassMap.containsKey(parts[0])) {
                      productClassMap.get(parts[0]).add(parts[1]);
                  } else {
                      Set<String> productClasses = new HashSet<>();
                      productClasses.add(parts[1]);
                      productClassMap.put(parts[0], productClasses);
                  }
              });
        productClassMap.keySet()
              .forEach(p -> {
                  productCellValue.append(p + "\n", productFont);
                  productClassMap.get(p).forEach(pc -> productCellValue.append(pc + "\n", productClassFont));
              });

        return productCellValue;
    }

    private String separateByNewline(String value) {
        return isEmpty(value) ? "" : value.replaceAll("\\|", "\n");
    }

    private String formatEditionDate(String editionDate) {
        return isEmpty(editionDate) ? "" : editionDate.substring(0, 2) + "/" + editionDate.substring(2);
    }

    private String formatEffectiveDate(String effectiveDate) {
        return isEmpty(effectiveDate) ? "" : effectiveDate.replaceAll("-", "/");
    }

    /**
     * Get the formatted end date.
     * @param endDate the end date formatted as "yyyy-mm-dd"
     * @return the formatted end date in "mm/dd/yy" format, where "yy" is the last 2 digits of the year
     */
    private String formatEndDate(String endDate) {
        if (isEmpty(endDate)) {
            return "";
        }
        String[] parts = endDate.split("-");
        String year = parts[0].substring(2);
        return parts[1] + "/" + parts[2] + "/" + year;
    }

    private void createFormsHeaders(int rowNumber, Sheet sheet, CellStyle style) {
        Row row = sheet.createRow(rowNumber);
        Cell cell = row.createCell(0);
        cell.setCellValue("Company");
        cell = row.createCell(1);
        cell.setCellValue("Product");
        cell = row.createCell(2);
        cell.setCellValue("State");
        cell = row.createCell(3);
        cell.setCellValue("Form Number");
        cell = row.createCell(4);
        cell.setCellValue("Edition Date");
        cell = row.createCell(5);
        cell.setCellValue("Title");
        cell = row.createCell(6);
        cell.setCellValue("As of Date");
        cell = row.createCell(7);
        cell.setCellValue("Form Requirement");
        cell = row.createCell(8);
        cell.setCellValue("Form Requirement Notes");
        cell = row.createCell(9);
        cell.setCellValue("Non-Admitted Status");
        cell = row.createCell(10);
        cell.setCellValue("End Date");
        row.setHeight((short) -1);
        setRowCellStyle(row, style);
    }


//    private void createFormsRow(int rowNumber, PdcFlatForm pdcFlatForm, Sheet sheet, CellStyle cellStyle, XSSFFont cellBoldFont, XSSFFont cellFont) {
//        Row row = sheet.createRow(rowNumber);
//        Cell cell = row.createCell(0);
//        cell.setCellValue(separateByNewline(pdcFlatForm.getCompanyCode()));
//        cell = row.createCell(1);
//        cell.setCellValue(buildProductCellValue(pdcFlatForm.getProductClassCode(), cellBoldFont, cellFont));
//        cell = row.createCell(2);
//        cell.setCellValue(separateByNewline(pdcFlatForm.getStateCode()));
//        cell = row.createCell(3);
//        cell.setCellValue(pdcFlatForm.getFormCode());
//        cell = row.createCell(4);
//        cell.setCellValue(formatEditionDate(pdcFlatForm.getFormMmyy()));
//        cell = row.createCell(5);
//        cell.setCellValue(pdcFlatForm.getTitle());
//        cell = row.createCell(6);
//        cell.setCellValue(formatEffectiveDate(pdcFlatForm.getEffectiveDt()));
//        cell = row.createCell(7);
//        cell.setCellValue(pdcFlatForm.getMandatoryFlagString());
//        cell = row.createCell(8);
//        cell.setCellValue(pdcFlatForm.getFormRequirementsNotes());
//        cell = row.createCell(9);
//        cell.setCellValue(pdcFlatForm.getNonAdmittedForm());
//        cell = row.createCell(10);
//        cell.setCellValue(formatEndDate(pdcFlatForm.getEndDt()));
//        row.setHeight((short) -1);
//        setRowCellStyle(row, cellStyle);
//    }


    /**
     * Set the cell style for each cell in the specified row.
     * @param row the specified row
     * @param style the cell style to apply to each cell
     */
    private void setRowCellStyle(Row row, CellStyle style) {
        Iterator<Cell> cellIterator = row.cellIterator();
        while(cellIterator.hasNext()) {
            cellIterator.next().setCellStyle(style);
        }
    }

}
