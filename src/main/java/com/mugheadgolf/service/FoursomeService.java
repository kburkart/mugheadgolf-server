package com.mugheadgolf.service;

import com.mugheadgolf.data.WeeklyFoursomeData;
import com.mugheadgolf.model.Foursome;

import java.util.List;

public interface FoursomeService {
    public List<Foursome> generateRandomFoursomes(int week, int year);
    public List<Foursome> generateRandomFoursomesYear(int year);
    public List<WeeklyFoursomeData> getYear(Integer year);
    void swapTeeTimes(Integer year, Integer week, Integer schedule1, Integer schedule2);

}
