package com.mugheadgolf.service;

import com.mugheadgolf.data.WeeklyFoursomeData;
import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Foursome;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.FoursomeRepository;
import com.mugheadgolf.repository.ScheduleRepository;
import com.mugheadgolf.repository.SettingsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service("foursomeService")
public class FoursomeServiceImpl implements FoursomeService {

    @Autowired
    private FoursomeRepository foursomeRepository;

    @Autowired
    private SettingsRepository settingsRepository;


    @Autowired
    private ScheduleRepository scheduleRepository;

    @Override
    public List<Foursome> generateRandomFoursomes(int week, int year) {

        List<Foursome> foursomes = foursomeRepository.findAllByIdweekAndYearOrderByTeetime(week, year);
        if(foursomes.isEmpty()) {
            Settings settings = settingsRepository.findFirstByYear(year);
            List<Schedule> schedulesCourse1 = scheduleRepository.findAllByIdweekAndYearAndIdcourse(week, year, settings.getIdcourse());
            Collections.shuffle(schedulesCourse1);
            LocalTime teetime = settings.getTeeTime1();
            int x = 0;
            Foursome foursome = null;
            int scheduleCount = schedulesCourse1.size();
            for (Schedule schedule : schedulesCourse1) {
                scheduleCount--;
                if (x == 0) {
                    foursome = new Foursome();
                    foursome.setIdschedule1(schedule);
                    foursome.setIdweek(week);
                    foursome.setYear(year);
                    foursome.setTeetime(teetime);
                    if (scheduleCount == 0) {
                        foursomeRepository.save(foursome); //only 2 golfers available
                    }
                    x++;
                } else {
                    foursome.setIdschedule2(schedule);
                    foursomes.add(foursome);
                    foursomeRepository.save(foursome);
                    teetime = teetime.plusMinutes(settings.getTeeTimeGap1());
                    x = 0;
                }
            }
            if (settings.getIdcourse2() != null) {
                List<Schedule> schedulesCourse2 = scheduleRepository.findAllByIdweekAndYearAndIdcourse(week, year, settings.getIdcourse());
                Collections.shuffle(schedulesCourse2);
                teetime = settings.getTeeTime2();
                x = 0;
                scheduleCount = schedulesCourse2.size();
                for (Schedule schedule : schedulesCourse2) {
                    if (x == 0) {
                        scheduleCount--;
                        foursome = new Foursome();
                        foursome.setIdschedule1(schedule);
                        foursome.setIdweek(week);
                        foursome.setYear(year);
                        foursome.setTeetime(teetime);
                        log.debug("course2 sc " + scheduleCount);
                        if (scheduleCount == 0) {
                            foursomeRepository.save(foursome); //only 2 golfers available
                        }
                        x++;
                    } else {
                        foursome.setIdschedule2(schedule);
                        foursomes.add(foursome);
                        foursomeRepository.save(foursome);
                        teetime = teetime.plusMinutes(settings.getTeeTimeGap2());
                        x = 0;
                    }
                }
            }
        }
        return foursomes;
    }

    @Override
    public List<Foursome> generateRandomFoursomesYear(int year) {
        List<Foursome> foursomes = foursomeRepository.findAllByYear(year);
        if(foursomes.isEmpty()){
        for (int week = 1; week <= 16; week++) {
            Settings settings = settingsRepository.findFirstByYear(year);
            List<Schedule> schedulesCourse1 = scheduleRepository.findAllByIdweekAndYearAndIdcourse(week, year, settings.getIdcourse());
            Collections.shuffle(schedulesCourse1);
            LocalTime teetime = settings.getTeeTime1();
            int x = 0;
            Foursome foursome = null;
            int scheduleCount = schedulesCourse1.size();
            for (Schedule schedule : schedulesCourse1) {
                scheduleCount--;
                if (x == 0) {
                    foursome = new Foursome();
                    foursome.setIdschedule1(schedule);
                    foursome.setIdweek(week);
                    foursome.setYear(year);
                    foursome.setTeetime(teetime);
                    if (scheduleCount == 0) {
                        foursomeRepository.save(foursome); //only 2 golfers available
                    }
                    x++;
                } else {
                    foursome.setIdschedule2(schedule);
                    foursomes.add(foursome);
                    foursomeRepository.save(foursome);
                    teetime = teetime.plusMinutes(settings.getTeeTimeGap1());
                    x = 0;
                }
            }
            if (settings.getIdcourse2() != null) {
                List<Schedule> schedulesCourse2 = scheduleRepository.findAllByIdweekAndYearAndIdcourse(week, year, settings.getIdcourse2());
                Collections.shuffle(schedulesCourse2);
                teetime = settings.getTeeTime2();
                x = 0;
                scheduleCount = schedulesCourse2.size();
                for (Schedule schedule : schedulesCourse2) {
                    scheduleCount--;
                    if (x == 0) {
                        foursome = new Foursome();
                        foursome.setIdschedule1(schedule);
                        foursome.setIdweek(week);
                        foursome.setYear(year);
                        foursome.setTeetime(teetime);
                        log.debug("course2 sc " + scheduleCount);
                        if (scheduleCount == 0) {
                            foursomeRepository.save(foursome); //only 2 golfers available
                            foursomes.add(foursome);
                        }
                        x++;
                    } else {
                        foursome.setIdschedule2(schedule);
                        foursomes.add(foursome);
                        foursomeRepository.save(foursome);
                        teetime = teetime.plusMinutes(settings.getTeeTimeGap2());
                        x = 0;
                    }
                }
            }
        }
        }
        return foursomes;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void swapTeeTimes(Integer year, Integer week, Integer idschedule1, Integer idschedule2) {
        log.debug("Swapping Tee Times");
        List<Foursome> foursomes = foursomeRepository.findAllByIdweekAndYearOrderByTeetime(week, year);
        Schedule schedule1 = scheduleRepository.getOne(idschedule1);
        Schedule schedule2 = scheduleRepository.getOne(idschedule2);
        if (schedule1.getIdcourse().getIdcourse() != schedule2.getIdcourse().getIdcourse()) {
            Course course = schedule1.getIdcourse();
            schedule1.setIdcourse(schedule2.getIdcourse());
            schedule2.setIdcourse(course);
        }
        Foursome foursome1 = null;
        Foursome foursome2 = null;
        boolean schedule1isFoursome1Schedule1 = true;
        boolean schedule2isFoursome2Schedule1 = true;

        for (Foursome foursome : foursomes) {
            if (foursome.getIdschedule1().getIdschedule().intValue() == idschedule1.intValue()) {
                foursome1 = foursome;
                schedule1isFoursome1Schedule1 = true;
                break;
            } else if (foursome.getIdschedule2().getIdschedule().intValue() == idschedule1.intValue()) {
                foursome1 = foursome;
                schedule1isFoursome1Schedule1 = false;
                break;
            }
        }
        for (Foursome foursome : foursomes) {
            if (foursome.getIdschedule1().getIdschedule().intValue() == idschedule2.intValue()) {
                foursome2 = foursome;
                schedule2isFoursome2Schedule1 = true;
                break;
            } else if (foursome.getIdschedule2().getIdschedule().intValue() == idschedule2.intValue()) {
                foursome2 = foursome;
                schedule2isFoursome2Schedule1 = false;
                break;
            }
        }
        updateFoursome(foursome1, schedule2, schedule1isFoursome1Schedule1);
        updateFoursome(foursome2, schedule1, schedule2isFoursome2Schedule1);

    }

    public void updateFoursome(Foursome foursome, Schedule schedule, boolean isSchedule1) {


        if (isSchedule1) {
            foursome.setIdschedule1(schedule);
            foursomeRepository.save(foursome);
        } else {
            foursome.setIdschedule2(schedule);
            foursomeRepository.save(foursome);
        }

    }

    @Override
    public List<WeeklyFoursomeData> getYear(Integer year) {
        List<WeeklyFoursomeData> weeklyFoursomes = new ArrayList<>();
        try {
            Settings settings = settingsRepository.findFirstByYear(year);
            int totalWeeks = settings.getTotalweeks();
            Course course1 = settings.getIdcourse();
            Course course2 = settings.getIdcourse2();
            for (int x = 1; x <= totalWeeks; x++) {
                WeeklyFoursomeData weeklyFoursome = new WeeklyFoursomeData();
                weeklyFoursome.setCourse1(course1.getCourseName());
                List<Foursome> foursomes = foursomeRepository.findAllByIdweekAndYearAndNameOrderByTeetime(x, year, course1.getCourseName());
                weeklyFoursome.setFoursomes1(foursomes);

                if (course2 != null) {
                    weeklyFoursome.setCourse2(course2.getCourseName());
                    foursomes = foursomeRepository.findAllByIdweekAndYearAndNameOrderByTeetime(x, year, course2.getCourseName());
                    weeklyFoursome.setFoursomes2(foursomes);

                }
                weeklyFoursome.setWeek(x);
                weeklyFoursomes.add(weeklyFoursome);
            }
        } catch (NullPointerException x) {

        }

        return weeklyFoursomes;

    }


}

	


