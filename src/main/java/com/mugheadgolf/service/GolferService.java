package com.mugheadgolf.service;

import java.math.BigDecimal;

import com.mugheadgolf.model.Golfer;

public interface GolferService {
	
	public Golfer login(String username, String password);
	
	public void confirmEmail(String token);
	public void forgot(String phone);
	
	public Golfer register(Golfer golfer);
	public Golfer add(Golfer golfer);
	
}
