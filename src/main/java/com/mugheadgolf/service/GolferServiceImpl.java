package com.mugheadgolf.service;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.utilities.Communicate;

@Service("golferService")
public class GolferServiceImpl implements GolferService {
 
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	private static final Logger Logger = LoggerFactory.getLogger(GolferServiceImpl.class);

	
    public GolferServiceImpl()
    {
    }
    
	@Autowired
    private SmsService smsService;

	@Autowired
	private GolferRepository golferRepository;


    @Override
    public Golfer login(String username, String password) {
    	
    	Logger.info("Login from user name is " + username);

    	Golfer golfer = golferRepository.findFirstByUsernameAndPassword(username, password);
    	return golfer;
    }


	@Override
	public void confirmEmail(String token) {
		
		// May want to throw an exception if golfer is null;
		 Golfer golfer = golferRepository.findFirstByToken(token);;
	        if(golfer != null){
	        	golfer.setActive(true);
	        	golferRepository.save(golfer);
	        }
	        
	}


	@Override
	public Golfer register(Golfer golfer) {
		   String token = generateToken(20);
	        golfer.setToken(token);
        	golferRepository.save(golfer);
	        Communicate.sendConfirmationEmail(golfer.getEmail(), token);
	        return golfer;
	}

	@Override
	public Golfer add(Golfer golfer) {
			if(!golferRepository.existsById(golfer.getIdgolfer())) {
				String token = generateToken(20);
		        golfer.setToken(token);
		        golfer.setActive(true);			
			}
        	golferRepository.save(golfer);
	        return golfer;
	}

	
	public String generateToken(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }


	@Override
	public void forgot(String phone) {
		Logger.info("Password request from web from " + phone );
		Golfer golfer = golferRepository.findFirstByPhone(phone);
		smsService.forgot(golfer);
		
	}

    
}