package com.mugheadgolf.service;

import java.math.BigDecimal;
import java.util.List;

import com.mugheadgolf.data.HandicapData;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Score;

public interface HandicapService {
	

	public BigDecimal calculateHandicap(Golfer golfer, List<Score> scores);
	public List<HandicapData> getAllHandicapsByYear(int year);
	public void calculateHandicaps();
	public void calculateHandicaps(Golfer golfer);

}
