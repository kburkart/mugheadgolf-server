package com.mugheadgolf.service;

import com.mugheadgolf.data.HandicapData;
import com.mugheadgolf.model.*;
import com.mugheadgolf.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service("handicapService90Percent")
public class HandicapService90PercentImpl implements HandicapService {

    private int par[][] = {{36, 35}, {36, 36}};

    public HandicapService90PercentImpl() {
    }

    @Autowired
    private GolferRepository golferRepository;

    @Autowired
    private ScoreRepository scoreRepository;

    @Autowired
    private SettingsRepository settingsRepository;

    @Autowired
    private ScoreholeRepository scoreholeRepository;

    @Autowired
    private CourseholesRepository courseholesRepository;

    @Autowired
    private HandicapRepository handicapRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private ScheduleService scheduleService;


    @Override
    public BigDecimal calculateHandicap(Golfer golfer, List<Score> scoreList) {
        log.debug("Calculating Handicap for " + golfer.getFirstname() + " " + golfer.getLastname());
//        scoreList.forEach( s -> log.debug(s.getScore() + " " + s.getAdjustedscore()));
        int totalScores = scoreList.size();
        Score score = scoreList.get(0);  //score handicap calculated for
        int year = LocalDate.now().getYear();
        int scoreYear = score.getIdschedule().getYear();
        List<Score> scores = scoreList;

        Course course = score.getIdschedule().getIdcourse();
        List<Courseholes> courseholes = courseholesRepository.findAllByIdcourseOrderByHole(course);
        List<Scorehole> scoreholes = scoreholeRepository.findAllByIdscoreOrderByHole(score);

        boolean backnine = true;
        if ((score.getIdschedule().getBacknine() == null) || (score.getIdschedule().getBacknine().intValue() == 0)) {
            backnine = false;
        }

        int adjustedScore = adjustScore(courseholes, scoreholes, backnine);

        score.setAdjustedscore(adjustedScore);
        scoreRepository.save(score);

        if (totalScores < 1) {
            return new BigDecimal(golfer.getCurrenthandicap());
        }

        BigDecimal hc = getAvgScoreHC(scores);

        List<Schedule> schedules = scheduleRepository.getGolferSchedule(year, golfer);
        for (Schedule schedule : schedules){
            if(scoreYear != year){
                if (schedule.getIdgolfer1().getIdgolfer() == golfer.getIdgolfer()) {
                    schedule.setHandicap1(hc);
                } else {
                    schedule.setHandicap2(hc);
                }
                scheduleRepository.save(schedule);
            }
            else if(totalScores < 4){
                if (schedule.getIdweek() >= score.getIdschedule().getIdweek()) {
                    if (schedule.getIdgolfer1().getIdgolfer() == golfer.getIdgolfer()) {
                        schedule.setHandicap1(hc);
                    } else {
                        schedule.setHandicap2(hc);
                    }
                    scheduleRepository.save(schedule);
                }
            }
            else {
                if (schedule.getIdweek() > score.getIdschedule().getIdweek()) {
                    if (schedule.getIdgolfer1().getIdgolfer() == golfer.getIdgolfer()) {
                        schedule.setHandicap1(hc);
                    } else {
                        schedule.setHandicap2(hc);
                    }
                    scheduleRepository.save(schedule);
                }
            }
        }
        hc = hc.setScale(0, RoundingMode.HALF_UP);
        golfer.setCurrenthandicap(hc.intValueExact());
        golferRepository.save(golfer);
        log.debug("Handicap for " + golfer.getFirstname() + " " + golfer.getLastname() + " is " + hc);
        return hc;
    }


    public BigDecimal getAvgScoreHC(List<Score> scores) {
        int numberOfScores = scores.size();
        BigDecimal total = new BigDecimal(0);
        int x = 1;
        int y = 0;
        for (Score score : scores) {
            int par = 36;
            if (score.getIdschedule().getBacknine() == 1) {
                par = score.getIdschedule().getIdcourse().getBack();
            }
            else{
                par = score.getIdschedule().getIdcourse().getFront();
            }
            if(score.getAdjustedscore() == null){
                score.setAdjustedscore(score.getScore());
            }
            total = total.add(new BigDecimal(score.getAdjustedscore() - par));
        }

        total = total.divide(new BigDecimal(numberOfScores), 5, RoundingMode.HALF_UP);
        total = total.multiply(new BigDecimal(.9));
        total = total.setScale(1, RoundingMode.DOWN);
        return total;
    }

    public int adjustScore(List<Courseholes> courseholes, List<Scorehole> scores,
                           boolean backnine) {

        int maxPar3 = 6;
        int maxPar4 = 8;
        int maxPar5 = 10;


        int x = 0;
        int adjustedScore = 0;
        int start = 0;
        int end = 9;
        if (backnine) {
            start = 9;
            end = 18;
        }

        for (int y = start; y < end; y++) {
            Courseholes hole = courseholes.get(y);
            int strokes = scores.get(x).getScore();
            x++;
            int adjustedStrokes = strokes;

            switch (hole.getPar()) {
                case 3:
                    if (strokes > maxPar3) {
                        adjustedStrokes = maxPar3;
                    }
                    break;
                case 4:
                    if (strokes > maxPar4) {
                        adjustedStrokes = maxPar4;
                    }
                    break;
                case 5:
                    if (strokes > maxPar5) {
                        adjustedStrokes = maxPar5;
                    }
                    break;
                default:
                    break;

            }
            adjustedScore += adjustedStrokes;
        }
        return adjustedScore;
    }

    @Override
    public List<HandicapData> getAllHandicapsByYear(int year) {
        List<HandicapData> handicapData = new ArrayList<>();
        List<Golfer> golfers = golferRepository.findAllByActive(true);
        for (Golfer golfer : golfers) {
            HandicapData hd = new HandicapData();
            hd.setGolfer(golfer);
            List<Schedule> schedules = scheduleRepository.getGolferSchedule(year, golfer);
            List<Handicap> handicaps = new ArrayList<>();
            for (Schedule schedule : schedules) {
                Handicap hc = scheduleToHandicap(schedule, golfer);
                handicaps.add(hc);
            }
            hd.setHandicaps(handicaps);
            handicapData.add(hd);
        }
        return handicapData;
    }

    public Handicap scheduleToHandicap(Schedule schedule, Golfer golfer) {
        Handicap hc = new Handicap();
        hc.setIdgolfer(golfer);
        hc.setIdweek(schedule.getIdweek());
        hc.setYear(schedule.getYear());
        if (schedule.getIdgolfer1().getIdgolfer() == golfer.getIdgolfer()) {
            hc.setHandicap(schedule.getHandicap1());
        } else {
            hc.setHandicap(schedule.getHandicap2());
        }
        return hc;

    }

//	Old method
//	@Override
//	public void calculateHandicaps(int year, int week) {
//		List<Schedule> schedules = scheduleRepository.findAllByIdweekAndYear(week, year);
//		schedules.forEach(schedule -> {
//			if (!schedule.isAbsent1()) {
//				Score score = scoreRepository.findFirstByIdscheduleAndIdgolfer(schedule, schedule.getIdgolfer1());
//				calculateHandicap(score, year);
//			}
//			if (!schedule.isAbsent2()) {
//				Score score = scoreRepository.findFirstByIdscheduleAndIdgolfer(schedule, schedule.getIdgolfer2());
//				calculateHandicap(score, year);
//			}
//		});
//
//	}

    @Override
    public void calculateHandicaps() {
        List<Golfer> golfers = golferRepository.findAllByActive(true);
        log.debug("number of golfers {}", golfers.size());
        golfers.forEach(golfer -> {
            if (golfer.getIdgolfer() != 0) { //golfer id zero is the BYE golfer
                calculateHandicaps(golfer);
            }
        });

    }

    @Override
    public void calculateHandicaps(Golfer golfer) {
        int currentYear = LocalDate.now().getYear();
        int week = scheduleService.getCurrentWeek(currentYear) - 1;

        List<Score> scores = scoreRepository.getHandicapScoresYear(golfer, 1, currentYear);
        if(scores.size() < 3){
            scores = scoreRepository.getHandicapScoresYear(golfer, 1, currentYear-1);
            if(scores.size() > 3){
                scores = scores.subList(0,3);
            }
        }

        if (scores != null && scores.size() > 0) {
            calculateHandicap(golfer, scores);
        }
    }

//    @Override
//    public void calculateHandicaps(Golfer golfer) {
//        int currentYear = LocalDate.now().getYear();
//        int week = scheduleService.getCurrentWeek(currentYear) - 1;
//
//        List<Score> scoresCurrentYear = scoreRepository.getHandicapScoresYear(golfer, 1, currentYear);
//        List<Score> scoresLastYear = scoreRepository.getHandicapScoresYear(golfer, 1, currentYear-1).stream()
//                .filter(score -> score.getIdschedule().getIdweek() >= week).collect(Collectors.toList());
//        List<Score> scores = Stream.concat(scoresLastYear.stream(), scoresCurrentYear.stream())
//                .collect(Collectors.toList());
//        if(scores.size() > 16) {
//            scores = scores.subList(0, 16);
//        }
//        if (scores != null && scores.size() > 0) {
//            calculateHandicap(golfer, scores);
//        }
//    }
}
