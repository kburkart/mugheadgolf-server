package com.mugheadgolf.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mugheadgolf.data.HandicapData;
import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Courseholes;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Handicap;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.model.Score;
import com.mugheadgolf.model.Scorehole;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.CourseholesRepository;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.repository.HandicapRepository;
import com.mugheadgolf.repository.ScheduleRepository;
import com.mugheadgolf.repository.ScoreRepository;
import com.mugheadgolf.repository.ScoreholeRepository;
import com.mugheadgolf.repository.SettingsRepository;

@Service("handicapService")
public class HandicapServiceImpl implements HandicapService {
	private static final Logger Logger = LoggerFactory.getLogger(HandicapServiceImpl.class);

	public HandicapServiceImpl() {
	}

	@Autowired
	private GolferRepository golferRepository;

	@Autowired
	private ScoreRepository scoreRepository;

	@Autowired
	private SettingsRepository settingsRepository;

	@Autowired
	private ScoreholeRepository scoreholeRepository;

	@Autowired
	private CourseholesRepository courseholesRepository;

	@Autowired
	private HandicapRepository handicapRepository;

	@Autowired
	private ScheduleRepository scheduleRepository;

	@Override
	public BigDecimal calculateHandicap(Golfer golfer, List<Score> scoreList) {

		int totalScores = scoreList.size();
		int last_index = totalScores-1;
		int first_index = 0;
		if(totalScores > 20) {
			first_index = totalScores - 20;
		}
		Score score = scoreList.get(last_index);  //score handicap calculated for
		int year = score.getIdschedule().getYear();
		List<Score> scores = scoreList.subList(first_index, totalScores); ///only use last 20 scores
		Settings settings = settingsRepository.findFirstByYear(year);

		Logger.debug("Calculating Handicap for " + golfer.getFirstname() + " " + golfer.getLastname());
		Course course = score.getIdschedule().getIdcourse();
		List<Courseholes> courseholes = courseholesRepository.findAllByIdcourseOrderByHole(course);

		List<Scorehole> scoreholes = scoreholeRepository.findAllByIdscoreOrderByHole(score);

		boolean backnine = true;
		if ((score.getIdschedule().getBacknine() == null) || (score.getIdschedule().getBacknine().intValue() == 0)) {
			backnine = false;
		}

		BigDecimal currentHC = null;

		Schedule sched = score.getIdschedule();
		if (sched.getIdgolfer1().getIdgolfer() == golfer.getIdgolfer()) {
			currentHC = sched.getHandicap1();
		} else {
			currentHC = sched.getHandicap2();
		}

		int adjustedScore = adjustScore(settings, courseholes, scoreholes,
				currentHC.setScale(0, RoundingMode.HALF_UP).intValueExact(), backnine);

		BigDecimal hcDiff = gethandicapdifferential(course.getSlope(), course.getRating(), adjustedScore, true);
		score.setAdjustedscore(adjustedScore);
		score.setHandicapdifferential(hcDiff);
		scoreRepository.save(score);
		BigDecimal hc = getAvgHCDifferentials(settings, scores);

		List<Schedule> schedules = scheduleRepository.getGolferSchedule(year, golfer);
		for (Schedule schedule : schedules)
			if (schedule.getIdweek() > score.getIdschedule().getIdweek()) {
				if (schedule.getIdgolfer1().getIdgolfer() == golfer.getIdgolfer()) {
					schedule.setHandicap1(hc);
				} else {
					schedule.setHandicap2(hc);
				}
				scheduleRepository.save(schedule);
			}
		hc = hc.setScale(0, RoundingMode.HALF_UP);
		golfer.setCurrenthandicap(hc.intValueExact());
		golferRepository.save(golfer);
		return hc;
	}

	
	public BigDecimal gethandicapdifferential(int slope, BigDecimal rating, int adjustedScore,
			boolean isNineHoleScore) {

		if (isNineHoleScore) {
			rating = rating.divide(new BigDecimal(2), 1, RoundingMode.DOWN);
		}

		BigDecimal ag = new BigDecimal(adjustedScore);
		BigDecimal hcDiff = ag.subtract(rating).multiply(new BigDecimal(113)).divide(new BigDecimal(slope), 1,
				RoundingMode.DOWN);

		return hcDiff;
	}

	public BigDecimal getAvgHCDifferentials(Settings handicapInfo, List<Score> scores) {
		int numberOfScores = scores.size();
		int lastYearHCNeeded = 0;
		int numberOfScoresUsed = 3;
		if (numberOfScores > 16) {
			numberOfScoresUsed = numberOfScores - 6;
		} else if (numberOfScores > 15) {
			numberOfScoresUsed = numberOfScores - 5;
		} else if (numberOfScores > 12) {
			numberOfScoresUsed = numberOfScores - 4;
		} else if (numberOfScores > 9) {
			numberOfScoresUsed = numberOfScores - 3;
		} else if (numberOfScores > 6) {
			numberOfScoresUsed = numberOfScores - 2;
		} else if (numberOfScores > 3) {
			numberOfScoresUsed = numberOfScores - 1;
		} else if (numberOfScores < 3) {
			lastYearHCNeeded = (3 - numberOfScores);
		}

		Collections.sort(scores);
		int lastRealScore = numberOfScores - 1;
		BigDecimal week1HC = null;
		Golfer golfer = scores.get(lastRealScore).getIdgolfer();
		Schedule schedule = scores.get(lastRealScore).getIdschedule();
		if (schedule.getIdgolfer1().getIdgolfer() == golfer.getIdgolfer()) {
			week1HC = schedule.getHandicap1();
		} else {
			week1HC = schedule.getHandicap2();
		}
		for (int x = 0; x < lastYearHCNeeded; x++) {
			Score score = new Score();
			score.setHandicapdifferential(week1HC);
			scores.add(score);
			numberOfScores++;
		}

		if (numberOfScores < numberOfScoresUsed) {
			numberOfScoresUsed = numberOfScores;
		}

		BigDecimal total = new BigDecimal(0);
		for (int x = 0; x < numberOfScoresUsed; x++) {
			Score score = scores.get(x);
			Logger.debug("HCdiff " + score.getHandicapdifferential());
			total = total.add(score.getHandicapdifferential());
		}

		total = total.divide(new BigDecimal(numberOfScoresUsed), 4, RoundingMode.HALF_UP);
		total = total.multiply(handicapInfo.getPercentage());
		return total.setScale(1, RoundingMode.FLOOR);

	}

	public int adjustScore(Settings settings, List<Courseholes> courseholes, List<Scorehole> scores, int handicap,
			boolean backnine) {

		int maxAllowed = settings.getMaxallowedhc();
		int maxPar3 = settings.getMaxallowedpar3();
		int maxPar4 = settings.getMaxallowedpar4();
		int maxPar5 = settings.getMaxallowedpar5();

		if (settings.getUseghin() == 1) {
			if (handicap < 5) {
				maxPar3 = 5;
				maxPar4 = 6;
				maxPar5 = 7;
			} else if (handicap < 10) {
				maxPar3 = 7;
				maxPar4 = 7;
				maxPar5 = 7;
			} else if (handicap < 15) {
				maxPar3 = 8;
				maxPar4 = 8;
				maxPar5 = 8;
			} else if (handicap < 20) {
				maxPar3 = 9;
				maxPar4 = 9;
				maxPar5 = 9;
			} else {
				maxPar3 = 10;
				maxPar4 = 10;
				maxPar5 = 10;
			}

		} else if (maxAllowed > 0) {
			maxPar3 = maxAllowed;
			maxPar4 = maxAllowed;
			maxPar5 = maxAllowed;
		}

		int x = 0;
		int adjustedScore = 0;
		int start = 0;
		int end = 9;
		if (backnine) {
			start = 9;
			end = 18;
		}

		for (int y = start; y < end; y++) {
			Courseholes hole = courseholes.get(y);
			int strokes = scores.get(x).getScore();
			x++;
			int adjustedStrokes = strokes;

			switch (hole.getPar()) {
			case 3:
				if (strokes > maxPar3) {
					adjustedStrokes = maxPar3;
				}
				break;
			case 4:
				if (strokes > maxPar4) {
					adjustedStrokes = maxPar4;
				}
				break;
			case 5:
				if (strokes > maxPar5) {
					adjustedStrokes = maxPar5;
				}
				break;
			default:
				break;

			}
			adjustedScore += adjustedStrokes;
		}
		return adjustedScore;
	}

	@Override
	public List<HandicapData> getAllHandicapsByYear(int year) {
		List<HandicapData> handicapData = new ArrayList<>();
		List<Golfer> golfers = golferRepository.findAllByActive(true);
		for (Golfer golfer : golfers) {
			HandicapData hd = new HandicapData();
			hd.setGolfer(golfer);
			List<Schedule> schedules = scheduleRepository.getGolferSchedule(year, golfer);
			List<Handicap> handicaps = new ArrayList<>();
			for (Schedule schedule : schedules) {
				Handicap hc = scheduleToHandicap(schedule, golfer);
				handicaps.add(hc);
			}
			hd.setHandicaps(handicaps);
			handicapData.add(hd);
		}
		return handicapData;
	}

	public Handicap scheduleToHandicap(Schedule schedule, Golfer golfer) {
		Handicap hc = new Handicap();
		hc.setIdgolfer(golfer);
		hc.setIdweek(schedule.getIdweek());
		hc.setYear(schedule.getYear());
		if (schedule.getIdgolfer1().getIdgolfer() == golfer.getIdgolfer()) {
			hc.setHandicap(schedule.getHandicap1());
		} else {
			hc.setHandicap(schedule.getHandicap2());
		}
		return hc;

	}

//	Old method
//	@Override
//	public void calculateHandicaps(int year, int week) {
//		List<Schedule> schedules = scheduleRepository.findAllByIdweekAndYear(week, year);
//		schedules.forEach(schedule -> {
//			if (!schedule.isAbsent1()) {
//				Score score = scoreRepository.findFirstByIdscheduleAndIdgolfer(schedule, schedule.getIdgolfer1());
//				calculateHandicap(score, year);
//			}
//			if (!schedule.isAbsent2()) {
//				Score score = scoreRepository.findFirstByIdscheduleAndIdgolfer(schedule, schedule.getIdgolfer2());
//				calculateHandicap(score, year);
//			}
//		});
//
//	}

	@Override
	public void calculateHandicaps() {

		List<Golfer> golfers = golferRepository.findAllByActive(true);
		golfers.forEach(golfer -> {
			if(golfer.getIdgolfer() != 0) { //golfer id zero is the BYE golfer
				calculateHandicaps(golfer);
			}
		});
		
	}

	@Override
	public void calculateHandicaps(Golfer golfer) {
		int currentYear = LocalDate.now().getYear();
		List<Score>	scores = scoreRepository.getAllGolferScoresByYear(golfer, currentYear);
		int index = 1;
		for(Score score: scores) {
			//if((score.getAdjustedscore() == null) || (score.getHandicapdifferential() == null)) {
				List<Score> scoresForCalc = new ArrayList<Score>();
				scoresForCalc.addAll(scores.subList(0, index));					
				calculateHandicap(golfer, scoresForCalc);
			//}
			index++;
		}	
	}

}