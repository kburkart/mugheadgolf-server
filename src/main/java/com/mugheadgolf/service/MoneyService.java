package com.mugheadgolf.service;

import java.util.List;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Money;

public interface MoneyService {
	
	List<List<Money>> getWeeklyWinners(Integer year);
	List<Golfer> calculateSkins(Integer year, Integer idweek);
	List<List<Golfer>> calculateLowNet(Integer year, Integer idweek);

}
