package com.mugheadgolf.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mugheadgolf.model.Games;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Money;
import com.mugheadgolf.model.Score;
import com.mugheadgolf.model.Scorehole;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.CourseholesRepository;
import com.mugheadgolf.repository.GamesRepository;
import com.mugheadgolf.repository.MoneyRepository;
import com.mugheadgolf.repository.ScoreRepository;
import com.mugheadgolf.repository.ScoreholeRepository;
import com.mugheadgolf.repository.SettingsRepository;

@Slf4j
@Service("moneyService")
public class MoneyServiceImpl implements MoneyService {

	private static final Logger Logger = LoggerFactory.getLogger(MoneyServiceImpl.class);

	@Autowired
	private SettingsRepository settingsRepository;

	@Autowired
	private GamesRepository gamesRepository;

	@Autowired
	private MoneyRepository moneyRepository;

	@Autowired
	private ScoreholeRepository scoreholeRepository;

	@Autowired
	private ScoreRepository scoreRepository;

	@Autowired
	private CourseholesRepository courseholesRepository;

	private static int LOW_NET_2 = 5;

	@Override
	public List<List<Money>> getWeeklyWinners(Integer year) {
		List<List<Money>> weeklyWinners = new ArrayList<>();
		Settings setting = settingsRepository.findFirstByYear(year);
		int totalWeeks = setting.getTotalweeks();
		for (int x = 1; x <= totalWeeks; x++) {
			weeklyWinners.add(moneyRepository.findAllByIdweekAndYearOrderByIdgame(x, year));

		}
		return weeklyWinners;
	}

	@Override
	public List<Golfer> calculateSkins(Integer year, Integer idweek) {
		List<Score> scores = scoreRepository.findAllByYearAndIdweek(year, idweek);
		List<Golfer> skins = new ArrayList<>();
		for (int x = 0; x < 9; x++) {
			int lowscore = 99;
			Golfer skin = null;
			for (Score score : scores) {
				List<Scorehole> scoreholes = new ArrayList<>(score.getScoreholeCollection());
				if ((scoreholes != null) && !scoreholes.isEmpty()) {
					if (scoreholes.get(x).getNet() == lowscore) {
						skin = null;
					} else if (scoreholes.get(x).getNet() < lowscore) {
						lowscore = scoreholes.get(x).getNet();
						Logger.debug("hole " + x + "skins goes to " + score.getIdgolfer().getLastname() + "with a net of " + lowscore);
						skin = score.getIdgolfer();
					}
				}
			}
			if (skin != null) {
				skins.add(skin);
			}

		}
		return skins;
	}

	@Override
	public List<List<Golfer>> calculateLowNet(Integer year, Integer idweek) {
		List<Score> scores1 = scoreRepository.findAllByYearAndIdweek(year, idweek);
		List<Golfer> lowNet1 = new ArrayList<>();
		List<Golfer> lowNet2 = new ArrayList<>();
		List<List<Golfer>> lowNet = new ArrayList<>();

		scores1.forEach(score -> {
			int handicap = 0;
			if(score.getIdgolfer().getIdgolfer() == score.getIdschedule().getIdgolfer1().getIdgolfer()){
				handicap = score.getIdschedule().getHandicap1().setScale(0, RoundingMode.HALF_UP).intValue();
			}
			else{
				handicap = score.getIdschedule().getHandicap2().setScale(0, RoundingMode.HALF_UP).intValue();
			}
			score.setNet(score.getScore() - handicap);
			//log.debug("Net is {} for  {} {}", score.getNet(), score.getIdgolfer().getFirstname(), score.getIdgolfer().getLastname());
			scoreRepository.save(score);
		});
		Collections.sort(scores1, Score.OrderByHandicap);
		Games gamesLowNet = gamesRepository.getOne(LOW_NET_2);
		int low = 99;
		if (gamesLowNet.getAmount().compareTo(new BigDecimal(0)) > 0) {
			int low2 = 99;
			int size = scores1.size();
			int half = size / 2;
			List<Score> scores2 = scores1.subList(half, size);
			scores1 = scores1.subList(0, half);
			for (Score score : scores2) {
//				log.debug("handicap {} score of {} net of {} for golfer {} {}",score.getIdgolfer().getCurrenthandicap(), score.getScore(), score.getNet(), score.getIdgolfer().getFirstname(), score.getIdgolfer().getLastname());
				if (score.getNet() < low2) {
					low2 = score.getNet();
					lowNet2.clear();
					lowNet2.add(score.getIdgolfer());
				} else if (score.getNet() == low2) {
					lowNet2.add(score.getIdgolfer());
				}
			}

		}
		for (Score score : scores1) {
//			log.debug("handicap {} score of {} net of {} for golfer {} {}",score.getIdgolfer().getCurrenthandicap(), score.getScore(), score.getNet(), score.getIdgolfer().getFirstname(), score.getIdgolfer().getLastname());
			if (score.getNet() < low) {
				low = score.getNet();
				lowNet1.clear();
				lowNet1.add(score.getIdgolfer());
			} else if (score.getNet() == low) {
				lowNet1.add(score.getIdgolfer());
			}
		}
		lowNet.add(lowNet1);
		lowNet.add(lowNet2);
		return lowNet;
	}

}
