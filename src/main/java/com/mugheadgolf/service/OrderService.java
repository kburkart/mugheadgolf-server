package com.mugheadgolf.service;

import com.mugheadgolf.data.EmailMessage;
import com.mugheadgolf.model.Foodorder;

public interface OrderService {
	public Foodorder save(Foodorder order, boolean placeOrder);
	public void confirmOrder(int orderId);
}
