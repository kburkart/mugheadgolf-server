package com.mugheadgolf.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mugheadgolf.data.EmailMessage;
import com.mugheadgolf.model.Foodorder;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.repository.OrderRepository;
import com.mugheadgolf.repository.SettingsRepository;
import com.mugheadgolf.utilities.Communicate;

@Service("orderService")
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private SettingsRepository settingsRepository;

	
	@Override
	public Foodorder save(Foodorder order, boolean placeOrder) {
		if(placeOrder) {
			LocalDate currentDate = LocalDate.now();
			int year = currentDate.getYear();
			Settings settings = settingsRepository.findFirstByYear(year);
			order.setOrdertime(new Date());
			Communicate.sendFoodOrder(order, settings.getFoodphone(), settings.getFoodemail());
		}
		Foodorder Foodorder = orderRepository.save(order); 
		return Foodorder;
	}
	
	@Override
	public void confirmOrder(int orderId) {

	}

}
