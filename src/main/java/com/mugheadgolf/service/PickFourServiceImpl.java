package com.mugheadgolf.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.Math;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mugheadgolf.model.Courseholes;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Handicap;
import com.mugheadgolf.model.PickFour;
import com.mugheadgolf.model.Points;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.model.Score;
import com.mugheadgolf.model.Scorehole;
import com.mugheadgolf.repository.CourseholesRepository;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.repository.HandicapRepository;
import com.mugheadgolf.repository.PickFourRepository;
import com.mugheadgolf.repository.PointsRepository;
import com.mugheadgolf.repository.ScheduleRepository;
import com.mugheadgolf.repository.ScoreRepository;
import com.mugheadgolf.repository.ScoreholeRepository;

@Service("pickFourService")
public class PickFourServiceImpl implements PickFourService {

	private static final Logger Logger = LoggerFactory.getLogger(PickFourServiceImpl.class);
	private static final int BYE = 0;

	@Autowired
	private ScheduleRepository scheduleRepository;

	@Autowired
	private ScoreRepository scoreRepository;
	
	@Autowired
	private PickFourRepository pickFourRepository;

	@Autowired
	private GolferRepository golferRepository;

	
	public Boolean  calculateTotals(int week, int year) {

		try {
			List<PickFour> bets = pickFourRepository.findAllByWeekAndYearOrderByTotal(week, year);
			List<Score> scores = scoreRepository.findAllByYearAndIdweek(year, week);
			Map<Integer,Integer> scoreMap = new HashMap<>();
			scores.forEach( score -> scoreMap.put(score.getIdgolfer().getIdgolfer(), score.getNet()));
			
			for(PickFour bet : bets) {
				Integer score1 = scoreMap.get(bet.getPick1().getIdgolfer());
				Integer score2 = scoreMap.get(bet.getPick2().getIdgolfer());
				Integer score3 = scoreMap.get(bet.getPick3().getIdgolfer());
				Integer score4 = scoreMap.get(bet.getPick4().getIdgolfer());
				
				bet.setScore1( score1 == null ? 0 : score1);
				bet.setScore2( score2 == null ? 0 : score2);
				bet.setScore3( score3 == null ? 0 : score3);
				bet.setScore4( score4 == null ? 0 : score4);

				if( score1 == null || score2 == null || score3 == null || score4 == null){
					bet.setTotal("Money Back");
				}
				else {
					bet.setTotal(score1 + score2 + score3 + score4 + "");
				}
				pickFourRepository.save(bet);
						
			}
		}
		catch(Exception x) {
			return false;
		}
		return true;
	}

}
