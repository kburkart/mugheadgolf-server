package com.mugheadgolf.service;

public interface PointsService {
	public void calculatePoints(int match, int year);
}
