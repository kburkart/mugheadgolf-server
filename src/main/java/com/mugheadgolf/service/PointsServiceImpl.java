package com.mugheadgolf.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.lang.Math;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mugheadgolf.model.Courseholes;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Handicap;
import com.mugheadgolf.model.Points;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.model.Score;
import com.mugheadgolf.model.Scorehole;
import com.mugheadgolf.repository.CourseholesRepository;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.repository.HandicapRepository;
import com.mugheadgolf.repository.PointsRepository;
import com.mugheadgolf.repository.ScheduleRepository;
import com.mugheadgolf.repository.ScoreRepository;
import com.mugheadgolf.repository.ScoreholeRepository;

@Service("pointsService")
public class PointsServiceImpl implements PointsService {

    private static final Logger Logger = LoggerFactory.getLogger(PointsServiceImpl.class);
    private static final int BYE = 0;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private ScoreRepository scoreRepository;

    @Autowired
    private PointsRepository pointsRepository;

    @Autowired
    private GolferRepository golferRepository;

    @Autowired
    private HandicapRepository handicapRepository;

    @Autowired
    private HandicapService handicapService;

    @Autowired
    private ScoreholeRepository scoreholeRepository;

    @Autowired
    private CourseholesRepository courseholesRepository;

    public void calculatePoints(int idschedule, int year) {

        // We are calculating the points on the client so this code is only used when a
        // golfer misses a week.

        Schedule schedule = scheduleRepository.findFirstByIdschedule(idschedule);

        Golfer golfer1 = schedule.getIdgolfer1();
        Golfer golfer2 = schedule.getIdgolfer2();


        Score score1 = scoreRepository.findFirstByIdscheduleAndIdgolfer(schedule, golfer1);
        Score score2 = scoreRepository.findFirstByIdscheduleAndIdgolfer(schedule, golfer2);

        List<Scorehole> strokesG1 = scoreholeRepository.findAllByIdscoreOrderByHole(score1);
        List<Scorehole> strokesG2 = scoreholeRepository.findAllByIdscoreOrderByHole(score2);

        List<Courseholes> courseholes = courseholesRepository.findAllByIdcourseOrderByHole(schedule.getIdcourse());
        BigDecimal total1 = new BigDecimal(0);
        BigDecimal total2 = new BigDecimal(0);
        BigDecimal strokesGained = schedule.getHandicap1().setScale(0, RoundingMode.HALF_UP)
                .subtract(schedule.getHandicap2().setScale(0, RoundingMode.HALF_UP));
        strokesGained = strokesGained.setScale(0, RoundingMode.HALF_UP);
        Logger.debug(" Points given is " + strokesGained);

        if (schedule.isAbsent1() && schedule.isAbsent2()) {
            total1 = new BigDecimal(4);
            total2 = new BigDecimal(4);

            schedule.setPoints1(total1);
            schedule.setPoints2(total2);
            Logger.debug("Points for " + schedule.getIdgolfer1().getFirstname() + ' '
                    + schedule.getIdgolfer1().getLastname() + " is " + schedule.getPoints1());
            Logger.debug("Points for " + schedule.getIdgolfer2().getFirstname() + ' '
                    + schedule.getIdgolfer2().getLastname() + " is " + schedule.getPoints2());

        }
        else if (schedule.isAbsent1()) {
            total1 = new BigDecimal(4);
            total2 = new BigDecimal(6);

            schedule.setPoints1(total1);
            schedule.setPoints2(total2);
            Logger.debug("Points for " + schedule.getIdgolfer1().getFirstname() + ' '
                    + schedule.getIdgolfer1().getLastname() + " is " + schedule.getPoints1());
            Logger.debug("Points for " + schedule.getIdgolfer2().getFirstname() + ' '
                    + schedule.getIdgolfer2().getLastname() + " is " + schedule.getPoints2());

        }
        else if (schedule.isAbsent2()) {
            total1 = new BigDecimal(6);
            total2 = new BigDecimal(4);

            schedule.setPoints1(total1);
            schedule.setPoints2(total2);
            Logger.debug("Points for " + schedule.getIdgolfer1().getFirstname() + ' '
                    + schedule.getIdgolfer1().getLastname() + " is " + schedule.getPoints1());
            Logger.debug("Points for " + schedule.getIdgolfer2().getFirstname() + ' '
                    + schedule.getIdgolfer2().getLastname() + " is " + schedule.getPoints2());
        }
        else if (golfer1.getIdgolfer() == 0) {
            schedule.setPoints2(new BigDecimal(5));

            Logger.debug("Points for " + schedule.getIdgolfer2().getFirstname() + ' '
                    + schedule.getIdgolfer2().getLastname() + " is " + schedule.getPoints2());
        }
        else if (golfer2.getIdgolfer() == 0) {
            schedule.setPoints1(new BigDecimal(5));
            Logger.debug("Points for " + schedule.getIdgolfer1().getFirstname() + ' '
                    + schedule.getIdgolfer1().getLastname() + " is " + schedule.getPoints1());
        }
        else {

            int holeIndex = 9 * schedule.getBacknine();
            int sg = strokesGained.intValue();
            int hcDiff = strokesGained.intValue();
            int sl = 1; // used to change strokes gained into strokes lost
            if (sg < 0) {
                sl = -1;
                sg = Math.abs(sg);
            }
            int terribleGolferAdjustment = 0;
            if (sg > 27) {
                terribleGolferAdjustment = 3;
                sg -= 27;
            } else if (sg > 18) {
                terribleGolferAdjustment = 2;
                sg -= 18;
            } else if (sg > 9) {
                terribleGolferAdjustment = 1;
                sg -= 9;
            }

            for (int x = 0; x < 9; x++) {

                int hcHole = 0;
                if (courseholes.get(x + holeIndex).getHandicap() <= sg) {
                    hcHole = 1;
                }
                int g1 = strokesG1.get(x).getScore();
                int g2 = strokesG2.get(x).getScore();

                if ((g1 != 0) && (g2 != 0)) {
                    int g1AdjScore = g1 - (hcHole + terribleGolferAdjustment) * sl;
                    Logger.debug("S " + g1 + " -> " + g1AdjScore + " " + g2);

                    if (g1AdjScore == g2) {
                        Logger.debug("Halvesies ");

                        total1 = total1.add(new BigDecimal(.5));
                        total2 = total2.add(new BigDecimal(.5));

                    } else if (g1AdjScore > g2) {
                        Logger.debug("Golfer 2 point ");

                        total2 = total2.add(new BigDecimal(1));
                    } else {
                        Logger.debug("Golfer 1 point ");

                        total1 = total1.add(new BigDecimal(1));

                    }
                } else {
                    //			Logger.debug("Holes have no scores for hole " + (x + 1));
                }

            }
            BigDecimal net1 = new BigDecimal(score1.getNet());
            BigDecimal net2 = new BigDecimal(score2.getNet());
            int scoreDiff = net1.compareTo(net2);
            //	Logger.debug("Scorediff " + scoreDiff);

            if (scoreDiff < 0) {
                total1 = total1.add(new BigDecimal(1));
            } else if (scoreDiff > 0) {
                total2 = total2.add(new BigDecimal(1));
            } else {
                total1 = total1.add(new BigDecimal(.5));
                total2 = total2.add(new BigDecimal(.5));
            }

            schedule.setPoints1(total1);
            schedule.setPoints2(total2);
            Logger.debug("Points for " + schedule.getIdgolfer1().getFirstname() + ' '
                    + schedule.getIdgolfer1().getLastname() + " is " + schedule.getPoints1());
            Logger.debug("Points for " + schedule.getIdgolfer2().getFirstname() + ' '
                    + schedule.getIdgolfer2().getLastname() + " is " + schedule.getPoints2());
        }
        scheduleRepository.save(schedule);
    }

}
