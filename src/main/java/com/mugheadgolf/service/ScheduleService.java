package com.mugheadgolf.service;

import java.math.BigDecimal;
import java.util.List;

import com.mugheadgolf.data.WeeklyScheduleData;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Schedule;

public interface ScheduleService {
	void create(Integer year);
	List<WeeklyScheduleData> getYear(Integer year); 
	void swapGolfers(Integer year, Integer week, Integer golfer1, Integer golfer2); 
	Integer getCurrentWeek(Integer year);
	List<Schedule> getWeek(Integer year, Integer week); 
	BigDecimal getGolferWeekHandicap(Golfer golfer, int week, int year);
	void toggleAbsent(Integer idgolfer, Integer idschedule);

}
