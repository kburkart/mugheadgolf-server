package com.mugheadgolf.service;

import com.mugheadgolf.data.WeeklyScheduleData;
import com.mugheadgolf.model.*;
import com.mugheadgolf.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalTime;
import java.util.*;

@Slf4j
@Service("scheduleService")
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private GolferRepository golferRepository;

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private FoursomeRepository foursomeRepository;

    @Autowired
    private SettingsRepository settingsRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private MoneyRepository moneyRepository;

    @Autowired
    private ScoreRepository scoreRepository;

    @Autowired
    private FoursomeService foursomeService;

    @Autowired
    private EmailService emailService;

    private static final int FOX_RUN_GREY = 3;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void create(Integer year) {

        if (scheduleRepository.findAllByYear(year).isEmpty()){
            Settings settings = settingsRepository.findFirstByYear(year);
        int totalMatchesFirstCourse = settings.getNumTeeTimes1() * 2;
        Course course1 = settings.getIdcourse();
        Course course2 = settings.getIdcourse2();
        List<Golfer> golfers = golferRepository.findAllByActive(true);

        if (golfers.size() % 2 != 0) {
            Golfer golfer = new Golfer();
            golfer.setIdgolfer(0);
            golfers.add(golfer); // If odd number of teams add a dummy
        }

        int numGolfers = golfers.size();

        int numWeeks = settings.getTotalweeks(); // Days needed to complete tournament
        int halfSize = numGolfers / 2;

        List<Golfer> tempGolfers = new ArrayList<Golfer>();

        tempGolfers.addAll(golfers); // Add teams to List and remove the first team
        tempGolfers.remove(0);

        int totalGolfers = tempGolfers.size();
        for (int week = 0; week < numWeeks; week++) {

            int idweek = week + 1;

            log.debug("Week " + idweek);

            int teamIdx = week % totalGolfers;

            int idgolfer1 = tempGolfers.get(teamIdx).getIdgolfer();
            int idgolfer2 = golfers.get(0).getIdgolfer();


            log.debug(idgolfer1 + " vs " + idgolfer2);
            Schedule schedule = new Schedule();
            schedule.setYear(year);
            schedule.setIdweek(idweek);

            schedule.setIdcourse(course1);
            Golfer golfer = golferRepository.getOne(idgolfer1);
            schedule.setIdgolfer1(golfer);
            schedule.setHandicap1(new BigDecimal(golfer.getCurrenthandicap()));
            golfer = golferRepository.getOne(idgolfer2);
            schedule.setIdgolfer2(golfer);
            schedule.setPoints1(BigDecimal.valueOf(0));
            schedule.setPoints2(BigDecimal.valueOf(0));
            schedule.setHandicap2(new BigDecimal(golfer.getCurrenthandicap()));
            schedule.setBacknine(0);
            scheduleRepository.save(schedule);
            int matchesScheduled = 1;

            for (int idx = 1; idx < halfSize; idx++) {
                int golfer1 = (week + idx) % totalGolfers;
                int golfer2 = (week + totalGolfers - idx) % totalGolfers;

//		             lastname1 = tempGolfers.get(golfer1).getLastname() == null ? "Bye" : tempGolfers.get(golfer1).getLastname();
//		             lastname2 = tempGolfers.get(golfer2).getLastname() == null ? "Bye" : tempGolfers.get(golfer2).getLastname();

                idgolfer1 = tempGolfers.get(golfer1).getIdgolfer();
                idgolfer2 = tempGolfers.get(golfer2).getIdgolfer();

                log.debug(idgolfer1 + " vs " + idgolfer2);

                schedule = new Schedule();
                schedule.setYear(year);
                schedule.setIdweek(idweek);
                golfer = golferRepository.getOne(idgolfer1);
                schedule.setIdgolfer1(golfer);
                schedule.setHandicap1(new BigDecimal(golfer.getCurrenthandicap()));
                golfer = golferRepository.getOne(idgolfer2);
                schedule.setIdgolfer2(golfer);
                schedule.setHandicap2(new BigDecimal(golfer.getCurrenthandicap()));
                if (matchesScheduled < totalMatchesFirstCourse) {
                    schedule.setIdcourse(course1);
                    log.debug("Course is " + course1.getCourseName());
                } else {
                    schedule.setIdcourse(course2);
                    log.debug("Course is " + course2.getCourseName());

                }
                matchesScheduled++;
                schedule.setBacknine(0);
                schedule.setPoints1(BigDecimal.valueOf(0));
                schedule.setPoints2(BigDecimal.valueOf(0));
                scheduleRepository.save(schedule);


            }
        }
        }
        else{
            log.warn("Schedule already exists for " + year +". A new schedule will not be created");
        }
    }


    @Override
    public List<WeeklyScheduleData> getYear(Integer year) {
        List<WeeklyScheduleData> weeklySchedules = new ArrayList<>();
        try {
            int totalWeeks = settingsRepository.findFirstByYear(year).getTotalweeks();
            for (int x = 1; x <= totalWeeks; x++) {
                WeeklyScheduleData weeklySchedule = new WeeklyScheduleData();
                weeklySchedule.setSchedules(scheduleRepository.findAllByIdweekAndYear(x, year));
                weeklySchedule.setWeek(x);
                weeklySchedules.add(weeklySchedule);
            }
        } catch (NullPointerException x) {

        }

        return weeklySchedules;

    }

    @Override
    public List<Schedule> getWeek(Integer year, Integer week) {
        return scheduleRepository.findAllByIdweekAndYear(week, year);

    }


    @Override
    public Integer getCurrentWeek(Integer year) {
        return moneyRepository.getCurrentWeek(year);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void swapGolfers(Integer year, Integer week, Integer idgolfer1, Integer idgolfer2) {
        log.debug("Swapping Golfers");
        List<Schedule> schedules = scheduleRepository.findAllByIdweekAndYear(week, year);
        Golfer golfer1 = golferRepository.getOne(idgolfer1);
        Golfer golfer2 = golferRepository.getOne(idgolfer2);
        Schedule schedule1 = null;
        Schedule schedule2 = null;
        boolean golfer1isSchedule1Golfer1 = true;
        boolean golfer2isSchedule2Golfer1 = true;
        BigDecimal handicap1 = null;
        BigDecimal handicap2 = null;
        boolean isAbsent1 = true;
        boolean isAbsent2 = true;

        for (Schedule schedule : schedules) {
            if (schedule.getIdgolfer1().getIdgolfer() == idgolfer1) {
                schedule1 = schedule;
                handicap1 = schedule.getHandicap1();
                golfer1isSchedule1Golfer1 = true;
                isAbsent1 = schedule.isAbsent1();
                break;
            } else if (schedule.getIdgolfer2().getIdgolfer() == idgolfer1) {
                schedule1 = schedule;
                handicap1 = schedule.getHandicap2();
                isAbsent1 = schedule.isAbsent2();
                golfer1isSchedule1Golfer1 = false;
                break;
            }
        }
        for (Schedule schedule : schedules) {
            if (schedule.getIdgolfer1().getIdgolfer() == idgolfer2) {
                handicap2 = schedule.getHandicap1();
                schedule2 = schedule;
                isAbsent2 = schedule.isAbsent1();
                golfer2isSchedule2Golfer1 = true;
                break;
            } else if (schedule.getIdgolfer2().getIdgolfer() == idgolfer2) {
                handicap2 = schedule.getHandicap2();
                schedule2 = schedule;
                golfer2isSchedule2Golfer1 = false;
                isAbsent2 = schedule.isAbsent2();
                break;
            }
        }

        Score score1 = scoreRepository.findFirstByIdscheduleAndIdgolfer(schedule1, golfer1);
        Score score2 = scoreRepository.findFirstByIdscheduleAndIdgolfer(schedule2, golfer2);

        updateSchedule(schedule1, golfer2, golfer1isSchedule1Golfer1, handicap2, isAbsent2);
        updateSchedule(schedule2, golfer1, golfer2isSchedule2Golfer1, handicap1, isAbsent1);

        if (score1 != null) {
            log.debug("Saving score 1 on swap");
            score1.setIdschedule(schedule2);
            scoreRepository.save(score1);
        }
        if (score2 != null) {
            log.debug("Saving score 2 on swap");
            score2.setIdschedule(schedule1);
            scoreRepository.save(score2);
        }
    }


    public void updateSchedule(Schedule schedule, Golfer golfer, boolean isGolfer1, BigDecimal handicap, boolean isAbsent) {
        if (isGolfer1) {
            schedule.setIdgolfer1(golfer);
            schedule.setHandicap1(handicap);
            schedule.setAbsent1(isAbsent);
            scheduleRepository.save(schedule);
        } else {
            schedule.setIdgolfer2(golfer);
            schedule.setHandicap2(handicap);
            schedule.setAbsent2(isAbsent);
            scheduleRepository.save(schedule);
        }

    }

    public BigDecimal getGolferWeekHandicap(Golfer golfer, int week, int year) {
        BigDecimal hc = null;
        Schedule schedule = scheduleRepository.getGolferScheduleByWeek(year, week, golfer);
        if (schedule != null) {
            if (golfer.getIdgolfer() == schedule.getIdgolfer1().getIdgolfer()) {
                hc = schedule.getHandicap1();
            } else {
                hc = schedule.getHandicap2();
            }
        }

        return hc;
    }

    @Override
    public void toggleAbsent(Integer idgolfer, Integer idschedule) {
        log.debug("Toggle Absent for id golfer {}", idgolfer);
        Schedule schedule = scheduleRepository.findFirstByIdschedule(idschedule);
        boolean isGolfer1 = true;
        boolean isAbsent = true;
        if (idgolfer.equals(schedule.getIdgolfer1().getIdgolfer())) {
            if (schedule.isAbsent1()) {
                schedule.setAbsent1(false);
                isAbsent = false;
            } else {
                schedule.setAbsent1(true);
            }
        } else {
            isGolfer1 = false;
            if (schedule.isAbsent2()) {
                schedule.setAbsent2(false);
                isAbsent = false;
            } else {
                schedule.setAbsent2(true);
            }
        }
        scheduleRepository.save(schedule);
        updateSchedulesAndTeeTimes(schedule.getYear(), schedule.getIdweek());
        //emailService.sendUpdatedWeeklySchedule();
    }

    private void updateSchedulesAndTeeTimes(Integer year, Integer week) {
        List<Schedule> schedules = scheduleRepository.findAllByIdweekAndYear(week, year);
        Integer golfer1 = null;
        Integer golfer2 = null;
        for (Schedule schedule : schedules) {
            if (schedule.isAbsent1() ^ schedule.isAbsent2()) {
                if (golfer1 == null) {
                    if (schedule.isAbsent1()) {
                        golfer1 = schedule.getIdgolfer1().getIdgolfer();
                    } else {
                        golfer1 = schedule.getIdgolfer2().getIdgolfer();
                    }
                } else {
                    if (schedule.isAbsent1()) {
                        golfer2 = schedule.getIdgolfer2().getIdgolfer();
                    } else {
                        golfer2 = schedule.getIdgolfer1().getIdgolfer();
                    }
                    break;
                }
            }
        }
        if (golfer1 != null && golfer2 != null) {
            swapGolfers(year, week, golfer1, golfer2);
        }
        Settings settings = settingsRepository.findFirstByYear(year);
        Course course1 = settings.getIdcourse();
        Course course2 = settings.getIdcourse2();

        List<Foursome> foursomesCourse1 = foursomeRepository.findAllByIdweekAndYearAndNameOrderByTeetime(week, year, course1.getCourseName());
        NavigableMap<LocalTime, Integer> playingSchedulesCourse1 = new TreeMap<>();
        NavigableMap<LocalTime, Integer> outSchedulesCourse1 = new TreeMap<>();
        for (Foursome foursome : foursomesCourse1) {
            if (foursome.getIdschedule1() != null) {
                if (foursome.getIdschedule1().isAbsent1() && foursome.getIdschedule1().isAbsent2()) {
                    log.debug("we have a swap candidate legendary run schedule 1");
                    outSchedulesCourse1.put(foursome.getTeetime().plusNanos(foursome.getIdschedule1().getIdschedule().longValue()), foursome.getIdschedule1().getIdschedule());
                } else {
                    playingSchedulesCourse1.put(foursome.getTeetime().plusNanos(foursome.getIdschedule1().getIdschedule().longValue()), foursome.getIdschedule1().getIdschedule());
                }
            }
            if (foursome.getIdschedule2() != null) {
                if (foursome.getIdschedule2().isAbsent1() && foursome.getIdschedule2().isAbsent2()) {
                    log.debug("we have a swap candidate legendary run schedule 2");
                    outSchedulesCourse1.put(foursome.getTeetime().plusNanos(foursome.getIdschedule2().getIdschedule().longValue()), foursome.getIdschedule2().getIdschedule());
                } else {
                    playingSchedulesCourse1.put(foursome.getTeetime().plusNanos(foursome.getIdschedule2().getIdschedule().longValue()), foursome.getIdschedule2().getIdschedule());
                }
            }
        }
        List<Foursome> foursomesCourse2 = foursomeRepository.findAllByIdweekAndYearAndNameOrderByTeetime(week, year, course2.getCourseName());
        NavigableMap<LocalTime, Integer> playingSchedulesCourse2 = new TreeMap<>();
        NavigableMap<LocalTime, Integer> outSchedulesCourse2 = new TreeMap<>();
        for (Foursome foursome : foursomesCourse2) {
            if (foursome.getIdschedule1() != null) {
                if (foursome.getIdschedule1().isAbsent1() && foursome.getIdschedule1().isAbsent2()) {
                    log.debug("we have a swap candidate Vinyards schedule 1");
                    outSchedulesCourse2.put(foursome.getTeetime().plusNanos(foursome.getIdschedule1().getIdschedule().longValue()), foursome.getIdschedule1().getIdschedule());
                } else {
                    playingSchedulesCourse2.put(foursome.getTeetime().plusNanos(foursome.getIdschedule1().getIdschedule().longValue()), foursome.getIdschedule1().getIdschedule());
                }
            }
            if (foursome.getIdschedule2() != null) {
                if (foursome.getIdschedule2().isAbsent1() && foursome.getIdschedule2().isAbsent2()) {
                    log.debug("we have a swap candidate Vinyards schedule 2");
                    outSchedulesCourse2.put(foursome.getTeetime().plusNanos(foursome.getIdschedule2().getIdschedule().longValue()), foursome.getIdschedule2().getIdschedule());
                } else {
                    playingSchedulesCourse2.put(foursome.getTeetime().plusNanos(foursome.getIdschedule2().getIdschedule().longValue()), foursome.getIdschedule2().getIdschedule());
                }
            }
        }
        log.debug("Out schedules course 1 count {}", outSchedulesCourse1.size());
        log.debug("Out schedules course 2 count {}", outSchedulesCourse2.size());
        log.debug("Playing schedules course 1 count {}", playingSchedulesCourse1.size());
        log.debug("Playing schedules course 2 count {}", playingSchedulesCourse2.size());
        while (outSchedulesCourse1.size() > 0) {
            Map.Entry<LocalTime, Integer> out = outSchedulesCourse1.firstEntry();
            LocalTime outTeeTime = out.getKey();
            Integer outSchedule = out.getValue();
            if (playingSchedulesCourse1.size() > playingSchedulesCourse2.size()) {
                Map.Entry<LocalTime, Integer> playing = playingSchedulesCourse1.lastEntry();
                LocalTime playingTeeTime = playing.getKey();
                Integer playingSchedule = playing.getValue();
                if (outTeeTime.isBefore(playingTeeTime)) {
                    foursomeService.swapTeeTimes(year, week, outSchedule, playingSchedule);
                }
                playingSchedulesCourse1.remove(playingTeeTime);
            } else {
                Map.Entry<LocalTime, Integer> playing = playingSchedulesCourse2.lastEntry();
                LocalTime playingTeeTime = playing.getKey();
                Integer playingSchedule = playing.getValue();
                foursomeService.swapTeeTimes(year, week, outSchedule, playingSchedule);
                playingSchedulesCourse2.remove(playingTeeTime);
            }
            outSchedulesCourse1.remove(outTeeTime);
        }
        while (outSchedulesCourse2.size() > 0) {
            Map.Entry<LocalTime, Integer> out = outSchedulesCourse2.firstEntry();
            LocalTime outTeeTime = out.getKey();
            Integer outSchedule = out.getValue();
            if (playingSchedulesCourse1.size() > playingSchedulesCourse2.size()) {
                Map.Entry<LocalTime, Integer> playing = playingSchedulesCourse1.lastEntry();
                LocalTime playingTeeTime = playing.getKey();
                Integer playingSchedule = playing.getValue();
                foursomeService.swapTeeTimes(year, week, outSchedule, playingSchedule);
                playingSchedulesCourse1.remove( playingTeeTime);
            } else {
                Map.Entry<LocalTime, Integer> playing = playingSchedulesCourse2.lastEntry();
                LocalTime playingTeeTime = playing.getKey();
                Integer playingSchedule = playing.getValue();
                if (outTeeTime.isBefore(playingTeeTime)) {
                    foursomeService.swapTeeTimes(year, week, outSchedule, playingSchedule);
                }
                playingSchedulesCourse2.remove(playingTeeTime);
            }
            outSchedulesCourse2.remove(outTeeTime);
        }
    }


    public static <K, V extends Comparable<V>> Map<K, V>
    sortByValues(final Map<K, V> map) {
        Comparator<K> valueComparator =
                new Comparator<K>() {
                    public int compare(K k1, K k2) {
                        int compare =
                                map.get(k1).compareTo(map.get(k2));
                        if (compare == 0)
                            return 1;
                        else
                            return compare;
                    }
                };

        Map<K, V> sortedByValues =
                new TreeMap<K, V>(valueComparator);
        sortedByValues.putAll(map);
        return sortedByValues;
    }
}

