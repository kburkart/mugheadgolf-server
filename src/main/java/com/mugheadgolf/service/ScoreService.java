package com.mugheadgolf.service;

import java.math.BigDecimal;
import java.util.List;

import com.mugheadgolf.data.GolferScoreData;
import com.mugheadgolf.data.ScoreData;
import com.mugheadgolf.data.WeeklyScoreData;
import com.mugheadgolf.model.Score;

public interface ScoreService {
	
	public List<ScoreData> getByIdschedule(Integer idschedule);
	public List<ScoreData> getByIdfoursome(Integer idforesome);
	public ScoreData getByIdscheduleAndIdGolfer(Integer idschedule, Integer idgolfer);
	public ScoreData getScoreData(Integer idscore);
	public void saveScores(List<Score> scores, boolean calcHC);
	public void saveScore(Score score, boolean calcHC);
	public List<GolferScoreData> getGolfersScores(Integer year);
	public List<WeeklyScoreData> getWeeklyScores(Integer idweek, Integer year);
	public BigDecimal getGolferAvg(Integer golfer, Integer year);
	public BigDecimal getLeagueAvg(Integer year);

}
