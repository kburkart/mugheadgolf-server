package com.mugheadgolf.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.mugheadgolf.model.*;
import com.mugheadgolf.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mugheadgolf.data.GolferScoreData;
import com.mugheadgolf.data.ScoreData;
import com.mugheadgolf.data.WeeklyScoreData;
import org.springframework.transaction.annotation.Transactional;

import static java.util.stream.Collectors.toCollection;

@Slf4j
@Service("scoreService")
public class ScoreServiceImpl implements ScoreService {
	private static final Logger Logger = LoggerFactory.getLogger(ScoreServiceImpl.class);

	@Autowired
	private ScheduleRepository scheduleRepository;

	@Autowired
	private FoursomeRepository foursomeRepository;

	@Autowired
	private ScoreRepository scoreRepository;

	@Autowired
	private ScoreholeRepository scoreholeRepository;

	@Autowired
	private HandicapService handicapService;

	@Autowired
	private GolferRepository golferRepository;

	@Override
	public List<ScoreData> getByIdschedule(Integer idschedule) {
		List<Score> scores = null;
		List<ScoreData> scoresdataList = new ArrayList<>();
		Schedule schedule = scheduleRepository.getOne(idschedule);
		scores = scoreRepository.findAllByIdscheduleOrderByIdscore(schedule);

		if ((scores == null) || (scores.isEmpty())) {
			Score score1 = new Score();
			Score score2 = new Score();
			score1.setIdschedule(schedule);
			score1.setIdgolfer(schedule.getIdgolfer1());
			score1.setUseforhandicap(1);
			score1.setNet(0);
			score1.setScore(0);
			scoreRepository.save(score1);
			scores.add(score1);
			score2.setIdschedule(schedule);
			score2.setIdgolfer(schedule.getIdgolfer2());
			score2.setUseforhandicap(1);
			score2.setNet(0);
			score2.setScore(0);
			scoreRepository.save(score2);
			scores.add(score2);
			scoreRepository.flush();
		}
		for (Score score : scores) {
			ScoreData scoredata = new ScoreData();
			scoredata.setScore(score);
			List<Scorehole> scoreholes = scoreholeRepository.findAllByIdscoreOrderByHole(score);
			scoredata.setScoreholes(scoreholes);
			scoresdataList.add(scoredata);
		}

		return scoresdataList;
	}

	@Override
	public List<ScoreData> getByIdfoursome(Integer idforesome) {
		Foursome foursome = foursomeRepository.getOne(idforesome);
		List<ScoreData> scoresdataList = getByIdschedule(foursome.getIdschedule1().getIdschedule());
		scoresdataList.addAll(getByIdschedule(foursome.getIdschedule2().getIdschedule()));
		return scoresdataList;
	}

	@Override
	public ScoreData getByIdscheduleAndIdGolfer(Integer idschedule, Integer idgolfer) {
		Score score = null;
		Schedule schedule = scheduleRepository.getOne(idschedule);
		Golfer golfer = golferRepository.getOne(idgolfer);
		score = scoreRepository.findFirstByIdscheduleAndIdgolfer(schedule, golfer);

		if (score == null) {
			score = new Score();
			score.setIdschedule(schedule);
			score.setIdgolfer(golfer);
			score.setUseforhandicap(1);
			score.setNet(0);
			score.setScore(0);
			scoreRepository.save(score);

		}
		ScoreData scoredata = new ScoreData();
		scoredata.setScore(score);
		List<Scorehole> scoreholes = scoreholeRepository.findAllByIdscoreOrderByHole(score);
		scoredata.setScoreholes(scoreholes);
		return scoredata;
	}


	@Override
	@Transactional
	public ScoreData getScoreData(Integer idscore) {
		ScoreData data = new ScoreData();
		Score score = scoreRepository.getOne(idscore);
		List<Scorehole> scoreholes = scoreholeRepository.findAllByIdscoreOrderByHole(score);
		data.setScore(score);
		data.setScoreholes(scoreholes);
		return data;
	}

	@Override
	public void saveScores(List<Score> scores, boolean calcHC) {
		scores.forEach(score -> saveScore(score, calcHC));
	}

	@Override
	public void saveScore(Score score, boolean calcHC) {
		if (score.getIdgolfer().getIdgolfer().intValue() != 0) {
			LocalDate currentDate = LocalDate.now();
			int year = currentDate.getYear();
			Schedule schedule = scheduleRepository.findFirstByIdschedule(score.getIdschedule().getIdschedule());
			int golfer1 = schedule.getIdgolfer1().getIdgolfer();
			int golfer2 = schedule.getIdgolfer2().getIdgolfer();
			int scoreGolfer = score.getIdgolfer().getIdgolfer();

			boolean absent1 = schedule.isAbsent1();
			boolean absent2 = schedule.isAbsent2();
			
			//Logger.debug("Absent1 is " + absent1 + " Absent2 is " + absent2);
			boolean isBye = false;
			boolean isAbsent = false;
			boolean opponentAbsent = false;

			if (golfer1 == scoreGolfer) {
				if (absent1) {
					isAbsent = true;
				}
				if (absent2) {
					opponentAbsent = true;
				}
				if (golfer2 == 0) {
					isBye = true;
				}
			} else {
				if (absent2) {
					isAbsent = true;
				}
				if (absent1) {
					opponentAbsent = true;
				}
				if (golfer1 == 0) {
					isBye = true;
				}
			}
			if (isAbsent) {
				if (golfer1 == scoreGolfer) {
					schedule.setPoints1(new BigDecimal(4));
				} else {
					schedule.setPoints2(new BigDecimal(4));
				}
			} else {

				if (isBye || opponentAbsent) {
//					int handicap;
//					if (golfer1 == scoreGolfer) {
//						handicap = schedule.getHandicap1().setScale(0, RoundingMode.HALF_UP).intValue();
//					} else {
//						handicap = schedule.getHandicap2().setScale(0, RoundingMode.HALF_UP).intValue();
//					}
//					BigDecimal points = new BigDecimal(5);
//					int net = score.getScore() - handicap;
//					int par = score.getIdschedule().getIdcourse().getPar();
//					int netToPar = net - par;
//					if (netToPar > 3) {
//						points = new BigDecimal(3);
//					} else if (netToPar > 0) {
//						points = new BigDecimal(4);
//					} else if (netToPar < -3) {
//						points = new BigDecimal(7);
//					} else if (netToPar < 0) {
//						points = new BigDecimal(6);
//					}
					BigDecimal points = new BigDecimal(6);
					if (golfer1 == scoreGolfer) {
						schedule.setPoints1(points);
					} else {
						schedule.setPoints2(points);
					}
				}
				else {
					schedule.setPoints1(score.getIdschedule().getPoints1());
					schedule.setPoints2(score.getIdschedule().getPoints2());
				}
			}
			scoreRepository.save(score);
			scheduleRepository.save(schedule);
			if (calcHC && !isAbsent) {
				handicapService.calculateHandicaps(score.getIdgolfer());
			}
		}
	}

	@Override
	public List<GolferScoreData> getGolfersScores(Integer year) {
		List<GolferScoreData> golferScores = new ArrayList<>();
		List<Golfer> golfers = golferRepository.findAllByActive(true);
		for (Golfer golfer : golfers) {
			GolferScoreData gsd = new GolferScoreData();
			gsd.setGolfer(golfer);
			List<Score> scores = scoreRepository.getGolferScores(year, golfer);
			gsd.setScores(scores);
			golferScores.add(gsd);
		}
		return golferScores;
	}

	@Override
	public List<WeeklyScoreData> getWeeklyScores(Integer idweek, Integer year) {
		List<Schedule> schedules = scheduleRepository.findAllByIdweekAndYear(idweek, year);
		List<WeeklyScoreData> weekScores = new ArrayList<>();
		int index = 0;
		for (Schedule schedule : schedules) {
			WeeklyScoreData wsd = new WeeklyScoreData();
			wsd.setIndex(index++);
			List<Score> scores = null;
			List<ScoreData> scoresdataList = new ArrayList<>();
			scores = scoreRepository.findAllByIdscheduleOrderByIdscore(schedule);

			if ((scores == null) || (scores.isEmpty())) {
				Score score1 = new Score();
				Score score2 = new Score();
				score1.setIdschedule(schedule);
				score1.setIdgolfer(schedule.getIdgolfer1());
				score1.setUseforhandicap(1);
				score1.setNet(0);
				score1.setScore(0);
				scoreRepository.save(score1);
				scores.add(score1);
				score2.setIdschedule(schedule);
				score2.setIdgolfer(schedule.getIdgolfer2());
				score2.setUseforhandicap(1);
				score2.setNet(0);
				score2.setScore(0);
				scoreRepository.save(score2);
				scores.add(score1);
				scoreRepository.flush();
			}
			for (Score score : scores) {
				ScoreData scoredata = new ScoreData();
				scoredata.setScore(score);
				List<Scorehole> scoreholes = scoreholeRepository.findAllByIdscoreOrderByHole(score);
				if(scoreholes == null || scoreholes.isEmpty()){
					scoreholes = getEmptySccoreholes();
				}
				scoredata.setScoreholes(scoreholes);
				scoresdataList.add(scoredata);

			}
			wsd.setScores(scoresdataList);
			weekScores.add(wsd);
		}
		return weekScores;
	}

	@Override
	public BigDecimal getGolferAvg(Integer golfer, Integer year) {
		return scoreRepository.getGolferAvg(year, golfer).setScale(2, RoundingMode.HALF_UP);
	}

	@Override
	public BigDecimal getLeagueAvg(Integer year) {
		return scoreRepository.getLeaqueAvg(year).setScale(2, RoundingMode.HALF_UP);
	}


	private List<Scorehole> getEmptySccoreholes(){
		List<Scorehole> scoreholes = new ArrayList<>();
		int y = 1;
		for(int x = 0; x < 9; x++){
			Scorehole scorehole = new Scorehole();
			scorehole.setHole(x+1);
			scorehole.setScore(0);
			scorehole.setIdscorehole(y++);
			scoreholes.add(scorehole);
		}
		return scoreholes;
	}

}
