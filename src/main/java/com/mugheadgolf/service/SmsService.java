package com.mugheadgolf.service;

import com.mugheadgolf.model.Golfer;

public interface SmsService {
	
	public String processSMS(String sms);
	public void forgot(Golfer golfer);

}
