package com.mugheadgolf.service;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;

import org.apache.commons.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.PickFour;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.repository.PickFourRepository;
import com.mugheadgolf.repository.ScheduleRepository;
import com.mugheadgolf.repository.SettingsRepository;
import com.mugheadgolf.utilities.Communicate;

@Service("smsService")
@Transactional
public class SmsServiceImpl implements SmsService {

	private static final Logger Logger = LoggerFactory.getLogger(SmsServiceImpl.class);

	static final private String BODY = "Body";
	static final private String FROM = "From";
	static final private String RUNNING_LATE = "RUNNING LATE";
	static final private String PICK_FOUR = "PICK4";
	static final private String PASSWORD = "PASSWORD";
	static final private String OUT = "OUT";
	static final private String IN = "IN";
	static final private String TICKER = "TICKER";
	static final private String TICKEROFF = "TICKEROFF";

	@Autowired
	private GolferRepository golferRepository;

	@Autowired
	private ScheduleRepository scheduleRepository;

	@Autowired
	SettingsRepository settingsRepository;

	@Autowired
	ScheduleService scheduleService;

	@Autowired
	PickFourRepository pickFourRepository;

	@Override
	public String processSMS(String sms) {
		Logger.info("SMS Message recieved " + sms);
		try {
			sms = java.net.URLDecoder.decode(sms, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String responses[] = sms.split("&");
		String from = null;
		String msg = null;
		int size = responses.length;
		for (int x = 0; x < size; x++) {
			String item[] = responses[x].split("=");
			switch (item[0]) {
			case BODY:
				msg = item[1];
				break;
			case FROM:
				from = item[1];
				break;
			default:
				break;

			}
		}
		Logger.debug("SMS MESSAGE IS " + msg);
		int year = LocalDate.now().getYear();

		int currentWeek = scheduleService.getCurrentWeek(year);
		String temp = msg.toUpperCase();
		if (temp.startsWith(RUNNING_LATE)) {
			String phone = from.substring(2);
			Golfer fromGolfer = golferRepository.findFirstByPhone(phone);
			Golfer toGolfer = null;
			Schedule schedule = scheduleRepository.getGolferScheduleByWeek(year, currentWeek, fromGolfer);
			if (schedule.getIdgolfer1().getIdgolfer() == fromGolfer.getIdgolfer()) {
				toGolfer = schedule.getIdgolfer2();
			} else {
				toGolfer = schedule.getIdgolfer1();
			}
			String minutes = msg.substring("Running Late".length());
			String message = fromGolfer.getFirstname() + " " + fromGolfer.getLastname()
					+ " is running late for your match.";
			Logger.debug("SMS MESSAGE SENT " + message + " message is to " + toGolfer.getFirstname() + " "
					+ toGolfer.getLastname());
			Communicate.sendSMS(toGolfer, message);
		} else if (temp.startsWith(TICKEROFF)) {
			String phone = from.substring(2);
			Golfer fromGolfer = golferRepository.findFirstByPhone(phone);
			if (fromGolfer.isAdmin()) {
				String message = msg.substring(6);
				Settings settings = settingsRepository.findFirstByYear(year);
				settings.setMessage(message);
				settings.setShowMessage(false);
				settingsRepository.save(settings);
			}
		} else if (temp.startsWith(TICKER)) {
			String phone = from.substring(2);
			Golfer fromGolfer = golferRepository.findFirstByPhone(phone);
			if (fromGolfer.isAdmin()) {
				String message = msg.substring(6);
				Settings settings = settingsRepository.findFirstByYear(year);
				settings.setMessage(message);
				settings.setShowMessage(true);
				settingsRepository.save(settings);
			}
		} else if (temp.startsWith(OUT)) {
			String phone = from.substring(2);
			Golfer fromGolfer = golferRepository.findFirstByPhone(phone);
			Golfer toGolfer = null;
			Schedule schedule = scheduleRepository.getGolferScheduleByWeek(year, currentWeek, fromGolfer);
			if (schedule.getIdgolfer1().getIdgolfer() == fromGolfer.getIdgolfer()) {
				toGolfer = schedule.getIdgolfer2();
				schedule.setAbsent1(true);

			} else {
				toGolfer = schedule.getIdgolfer1();
				schedule.setAbsent2(true);

			}
			scheduleRepository.save(schedule);
			String message = fromGolfer.getFirstname() + " " + fromGolfer.getLastname()
					+ " is out this week, meet up with another golfer if possible.";
			Logger.debug("SMS MESSAGE SENT " + message + " message is to " + toGolfer.getFirstname() + " "
					+ toGolfer.getLastname());
			Communicate.sendSMS(toGolfer, message);
		} else if (temp.startsWith(IN)) {
			String phone = from.substring(2);
			Golfer fromGolfer = golferRepository.findFirstByPhone(phone);
			Golfer toGolfer = null;
			Schedule schedule = scheduleRepository.getGolferScheduleByWeek(year, currentWeek, fromGolfer);
			if (schedule.getIdgolfer1().getIdgolfer() == fromGolfer.getIdgolfer()) {
				toGolfer = schedule.getIdgolfer2();
				schedule.setAbsent1(false);
			} else {
				toGolfer = schedule.getIdgolfer1();
				schedule.setAbsent2(false);

			}
			scheduleRepository.save(schedule);
			String message = fromGolfer.getFirstname() + " " + fromGolfer.getLastname()
					+ " is back in for your match this week.";
			Logger.debug("SMS MESSAGE SENT " + message + " message is to " + toGolfer.getFirstname() + " "
					+ toGolfer.getLastname());
			Communicate.sendSMS(toGolfer, message);
		} else if (temp.startsWith(PICK_FOUR)) {
			String phone = from.substring(2);
			Golfer fromGolfer = golferRepository.findFirstByPhone(phone);
			String words[] = temp.split(" ");
			if (words.length != 5) {
				Communicate.sendSMS(fromGolfer, "Pick Four Bet Incorrect");
			} else {
				Golfer golfer1 = getGolfer(words[1]);
				Golfer golfer2 = getGolfer(words[2]);
				Golfer golfer3 = getGolfer(words[3]);
				Golfer golfer4 = getGolfer(words[4]);
				if ((golfer1 == null) || (golfer2 == null) || (golfer3 == null) || (golfer4 == null)) {
					Communicate.sendSMS(fromGolfer, "Pick Four Bet Incorrect");
				} else {
					PickFour pickFour = new PickFour();
					pickFour.setIdgolfer(fromGolfer);
					pickFour.setYear(year);
					pickFour.setWeek(currentWeek);
					pickFour.setPick1(golfer1);
					pickFour.setPick2(golfer2);
					pickFour.setPick3(golfer3);
					pickFour.setPick4(golfer4);
					pickFourRepository.deleteByIdgolferAndYearAndWeek(fromGolfer, year, currentWeek);
					pickFourRepository.save(pickFour);
					Communicate.sendSMS(fromGolfer, "Pick Four Wager Successfully Placed");

				}
			}
		} else if (temp.trim().equalsIgnoreCase(PASSWORD)) {
			String phone = from.substring(2);
			Logger.debug("Recieved SMS password request from " + phone);
			Golfer fromGolfer = golferRepository.findFirstByPhone(phone);
			Golfer toGolfer = fromGolfer;
			String message = "Your Username is " + toGolfer.getUsername() + " and your password is "
					+ toGolfer.getPassword();
			Logger.debug("SMS MESSAGE SENT " + message + " message is to " + toGolfer.getFirstname() + " "
					+ toGolfer.getLastname());
			Communicate.sendSMS(toGolfer, message);
		}

		return "";
	}

	@Override
	public void forgot(Golfer golfer) {
		String message = "Your Username is " + golfer.getUsername() + " and your password is " + golfer.getPassword();
		Logger.debug(
				"SMS MESSAGE SENT " + message + " message is to " + golfer.getFirstname() + " " + golfer.getLastname());
		Communicate.sendSMS(golfer, message);
	}

	private Golfer getGolfer(String name) {
		if (name.equalsIgnoreCase("RMeder")) {
			return golferRepository.findFirstByFirstnameAndLastname("Ron", "Meder");
		} else if (name.equalsIgnoreCase("JMeder")) {
			return golferRepository.findFirstByFirstnameAndLastname("Jerry", "Meder");
		} else if (name.equalsIgnoreCase("KJohnson")) {
			return golferRepository.findFirstByFirstnameAndLastname("Kevin", "Johnson");
		} else if (name.equalsIgnoreCase("DJohnson")) {
			return golferRepository.findFirstByFirstnameAndLastname("Don", "Johnson");
		} else if (name.equalsIgnoreCase("MattBirk")) {
			return golferRepository.findFirstByFirstnameAndLastname("Matt", "Birk");
		} else if (name.equalsIgnoreCase("MarkBirk")) {
			return golferRepository.findFirstByFirstnameAndLastname("Mark", "Birk");
		} else if (name.equalsIgnoreCase("EBirk")) {
			return golferRepository.findFirstByFirstnameAndLastname("Eddie", "Birk");
		} else if (name.equalsIgnoreCase("BillyBohl")) {
			return golferRepository.findFirstByFirstnameAndLastname("Billy", "Bohl");
		} else if (name.equalsIgnoreCase("BryanBohl")) {
			return golferRepository.findFirstByFirstnameAndLastname("Brian", "Bohl");
		} else if (name.equalsIgnoreCase("LBirk")) {
			return golferRepository.findFirstByFirstnameAndLastname("Luke", "Birk");
		} else if (name.equalsIgnoreCase("BBirk")) {
			return golferRepository.findFirstByFirstnameAndLastname("Bill", "Birk");
		} else if (name.equalsIgnoreCase("MClines")) {
			return golferRepository.findFirstByFirstnameAndLastname("Mike", "Clines");
		} else if (name.equalsIgnoreCase("JClines")) {
			return golferRepository.findFirstByFirstnameAndLastname("Jeff", "Clines");
		} else {
			name = name.toLowerCase();
			name = WordUtils.capitalize(name);
			return golferRepository.findFirstByLastname(name);
		}
	}

}
