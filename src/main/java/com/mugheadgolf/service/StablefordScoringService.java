package com.mugheadgolf.service;

import com.mugheadgolf.model.Golfer;

import java.util.List;
import java.util.Map;

public interface StablefordScoringService {
    public void calculateStablefordScoring();
    public void calculateStablefordScoring(int year);

    public void calculateGolferStablefordScoring(Golfer golfer);
    public void calculateGolferStablefordScoring(Golfer golfer, int year);

    public int getGolferStabfordWeekTotal(Golfer golfer, int week, int year, boolean isHC);
    public List<Map<String, Object>> getStablefordWeeklyTotals(int week, int year, boolean isHC);
    public Map<Integer,List<Map<String, Object>>> getStablefordYearlyTotals(int year, boolean isHC);

    public Map<String, Map<String, Object>>  getTeamStablefordYearlyTotals(int year, boolean isHC);
}
