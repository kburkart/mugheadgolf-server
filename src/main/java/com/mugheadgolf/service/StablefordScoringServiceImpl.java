package com.mugheadgolf.service;

import com.mugheadgolf.data.GolferScoreData;
import com.mugheadgolf.model.*;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.repository.ScoreRepository;
import com.mugheadgolf.repository.TeamGolferRepository;
import com.mugheadgolf.repository.TeamRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Service
public class StablefordScoringServiceImpl implements StablefordScoringService{
    private GolferRepository golferRepository;
    private ScheduleService scheduleService;
    private ScoreRepository scoreRepository;
    private ScoreholeService scoreholeService;
    private ScoreService scoreService;
    private TeamRepository teamRepository;
    private TeamGolferRepository teamGolferRepository;


    public StablefordScoringServiceImpl(GolferRepository golferRepository, ScheduleService scheduleService, ScoreRepository scoreRepository, ScoreholeService scoreholeService, ScoreService scoreService, TeamRepository teamRepository, TeamGolferRepository teamGolferRepository) {
        this.golferRepository = golferRepository;
        this.scheduleService = scheduleService;
        this.scoreRepository = scoreRepository;
        this.scoreholeService = scoreholeService;
        this.scoreService = scoreService;
        this.teamRepository = teamRepository;
        this.teamGolferRepository = teamGolferRepository;
    }

    @Override
    public void calculateStablefordScoring() {
        List<Golfer> golfers = golferRepository.findAllByActive(true);
        log.debug("number of golfers {}", golfers.size());
        golfers.forEach(golfer -> {
            if (golfer.getIdgolfer() != 0) { //golfer id zero is the BYE golfer
                calculateGolferStablefordScoring(golfer);
            }
        });
    }

    public void calculateStablefordScoring(int year) {
        List<Golfer> golfers = golferRepository.findAllByActive(true);
        log.debug("number of golfers {}", golfers.size());
        golfers.forEach(golfer -> {
            if (golfer.getIdgolfer() != 0) { //golfer id zero is the BYE golfer
                calculateGolferStablefordScoring(golfer, year);
            }
        });
    }

    @Override
    public void calculateGolferStablefordScoring(Golfer golfer) {
        int currentYear = LocalDate.now().getYear();
        calculateGolferStablefordScoring(golfer, currentYear);
    }

    @Override
    public void calculateGolferStablefordScoring(Golfer golfer, int year) {
        List<Score> scores = scoreRepository.getHandicapScoresYear(golfer, 1, year);
        if (scores != null && scores.size() > 0) {
            calculateStablefordScores(golfer, scores);
        }
    }

    @Override
    public int getGolferStabfordWeekTotal(Golfer golfer, int week, int year, boolean isHC) {
        AtomicInteger stableford = new AtomicInteger();
        List<GolferScoreData> yearlyScores = scoreService.getGolfersScores(year);
        GolferScoreData golferScoreData =yearlyScores.stream().filter(gsd ->
            gsd.getGolfer().getIdgolfer() == golfer.getIdgolfer()).findFirst().orElse(null);
        if (golferScoreData != null){
            Score score = golferScoreData.getScores().stream().filter(s -> s.getIdschedule().getIdweek() == week).findFirst().orElse(null);
            if(score != null){
                score.getScoreholeCollection().forEach(sh -> {
                    if(isHC){
                        if(sh != null && sh.getStablefordHC() != null){
                            stableford.addAndGet(sh.getStablefordHC());
                        }
                    }
                    else{
                        if(sh != null && sh.getStableford() != null){
                            stableford.addAndGet(sh.getStableford());
                        }
                    }
                });
            }
        }
        return stableford.get();
    }

    @Override
    public List<Map<String, Object>> getStablefordWeeklyTotals(int week, int year, boolean isHC) {
        List<Map<String, Object>> weeklyStablefordList = new ArrayList<Map<String, Object>>();
        List<Golfer> golfers = golferRepository.findAllByActive(true);
        log.debug("number of golfers {}", golfers.size());
        golfers.forEach(golfer -> {
            HashMap<String, Object> weeklyStableford = new HashMap<String, Object>();
            if (golfer.getIdgolfer() != 0) { //golfer id zero is the BYE golfer
                weeklyStableford.put("golfer", golfer);
                weeklyStableford.put("stableford", getGolferStabfordWeekTotal(golfer, week, year, isHC));
            }
            weeklyStablefordList.add(weeklyStableford);
        });
        return weeklyStablefordList;
    }

    public Map<Object, Integer> getStablefordWeeklyTotals(Golfer golfer, int year, boolean isHC) {
        Map<Object, Integer> weeklyStablefordMap = new HashMap<>();
        int currentWeek = scheduleService.getCurrentWeek(year);
        int total = 0;
        for(int week = 1; week < currentWeek; week ++){
            int score = getGolferStabfordWeekTotal(golfer, week, year, isHC);
            total += score;
            weeklyStablefordMap.put(week, getGolferStabfordWeekTotal(golfer, week, year, isHC));
        }
        weeklyStablefordMap.put("total", total);
        return weeklyStablefordMap;
    }

    @Override
    public Map<Integer, List<Map<String, Object>>> getStablefordYearlyTotals(int year, boolean isHC) {
        Map<Integer, List<Map<String, Object>>> weeklyStablefordMap = new HashMap<>();
        int currentWeek = scheduleService.getCurrentWeek(year);
        for( int x = 1; x < currentWeek; x++) {
            List<Map<String, Object>> weeklyTotals = getStablefordWeeklyTotals(x,year,isHC);
            weeklyStablefordMap.put(x, weeklyTotals);
        }
        return weeklyStablefordMap;
    }

    public Map<String, Map<String, Object>> getTeamStablefordYearlyTotals(int year, boolean isHC){
        Map<String, Map<String, Object>> teamScoresMap = new HashMap<>();
        List<Team> teams = teamRepository.findAllByYear(year);
        teams.forEach(team -> {
            AtomicInteger teamTotal = new AtomicInteger();
            List<TeamGolfer> teamGolfers = teamGolferRepository.findAllByTeamId(team);
            Map<String, Object> golfersStablefordMap = new HashMap<>();
            teamGolfers.forEach( tg -> {
                Map<Object, Integer> weeklyTotalsMap = getStablefordWeeklyTotals(tg.getIdgolfer(),year,isHC);
                golfersStablefordMap.put(tg.getIdgolfer().getFirstname() + " " + tg.getIdgolfer().getLastname(), weeklyTotalsMap);
                teamTotal.addAndGet(weeklyTotalsMap.get("total"));
            });
            golfersStablefordMap.put("teamTotal", teamTotal.get());
            teamScoresMap.put(team.getTeamName(), golfersStablefordMap);
        });
        return teamScoresMap;
    }
    private void calculateStablefordScores(Golfer golfer, List<Score> scores) {
        scores.stream().forEach(score -> {
           Schedule schedule = score.getIdschedule();
           Collection<Scorehole> scoreholes = score.getScoreholeCollection();
           schedule.getIdcourse().getCourseCollection().forEach(hole -> {
               int par = hole.getPar();
               int holeNumber = hole.getHole();
               Scorehole scorehole = scoreholes.stream().filter(s -> s.getHole() == holeNumber).findFirst().orElse(null);
               if(scorehole != null) {
                   scoreholeService.saveWithStablefordCalc(scorehole, par);
               }
           });
        });
    }
}
