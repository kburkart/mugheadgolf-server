package com.mugheadgolf.service;

import java.util.List;

public interface TickerService {
	
	public List<String> getMessages();

}
