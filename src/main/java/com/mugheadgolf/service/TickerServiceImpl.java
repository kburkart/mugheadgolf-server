package com.mugheadgolf.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.MoneyRepository;
import com.mugheadgolf.repository.ScheduleRepository;
import com.mugheadgolf.repository.ScoreRepository;
import com.mugheadgolf.repository.SettingsRepository;

@Service("tickerService")
public class TickerServiceImpl implements TickerService {
	private static final Logger Logger = LoggerFactory.getLogger(TickerServiceImpl.class);
	private static final String PM_530 = "17:30:00";

	@Autowired
	private MoneyRepository moneyRepository;

	@Autowired
	private ScheduleRepository scheduleRepository;

	@Autowired
	private ScoreRepository scoreRepository;

	@Autowired
	private SettingsRepository settingsRepository;

	@Override
	public List<String> getMessages() {

		List<String> messages = new ArrayList<>();
		LocalTime time = LocalTime.now();
		DayOfWeek currentDay = LocalDate.now().getDayOfWeek();
		int year = LocalDate.now().getYear();
		int currentWeek = moneyRepository.getCurrentWeek(year);
		int latestScoreWeek = scoreRepository.latestScoreWeek(year);

		Settings settings = settingsRepository.findFirstByYear(year);
		if (settings != null && settings.isShowMessage()) {
			messages.clear();
			messages.add(settings.getMessage());
		}
		if ((currentWeek == 1) && (latestScoreWeek == 1)) {
			messages.addAll(getScores(1));
		} else if ((currentWeek == 1) || (currentDay == DayOfWeek.SATURDAY) || (currentDay == DayOfWeek.SUNDAY)
				|| ((currentDay == DayOfWeek.MONDAY)) && time.isBefore(LocalTime.parse(PM_530))) {
			messages.addAll(getSchedule(currentWeek));
		} else {
			messages.addAll(getScores(latestScoreWeek));
		}
		// Logger.debug("Getting Ticker Message");

		return messages;
	}

	private List<String> getScores(int week) {

		List<String> schedules = new ArrayList<>();
		schedules.add("Week " + week + " Results:  ");
		int year = LocalDate.now().getYear();
		List<Schedule> matches = scheduleRepository.findAllByIdweekAndYear(week, year);
		for (Schedule match : matches) {
			String score1 = match.isAbsent1() ? "OUT" : match.getPoints1().toPlainString();
			String score2 = match.isAbsent2() ? "OUT" : match.getPoints2().toPlainString();
			String schedule = match.getIdgolfer1().getFirstname() + " " + match.getIdgolfer1().getLastname() + " ("
					+ score1 + ")" + " vs " + match.getIdgolfer2().getFirstname() + " "
					+ match.getIdgolfer2().getLastname() + " (" + score2 + ") ...";
			schedules.add(schedule);
		}
		return schedules;
	}

	private List<String> getSchedule(int week) {
		List<String> schedules = new ArrayList<>();
		schedules.add("Week " + week + " Schedule:  ");
		int year = LocalDate.now().getYear();
		List<Schedule> matches = scheduleRepository.findAllByIdweekAndYear(week, year);
		for (Schedule match : matches) {
			String golfer1HC = match.isAbsent1() ? "OUT" : match.getHandicap1().toPlainString();
			String golfer2HC = match.isAbsent2() ? "OUT" : match.getHandicap2().toPlainString();
			String schedule = match.getIdgolfer1().getFirstname() + " " + match.getIdgolfer1().getLastname() + " ("
					+ golfer1HC + ")" + " vs " + match.getIdgolfer2().getFirstname() + " "
					+ match.getIdgolfer2().getLastname() + " (" + golfer2HC + ") ...";
			schedules.add(schedule);
		}
		return schedules;
	}

	// messages.add("Mughead Golf League starts Monday April 16");
	// messages.add("Vegas Odds:");
	// messages.add("Ruether 6:1");
	// messages.add("Burkart 8:1");
	// messages.add("Clines 9:1");
	// messages.add("Rowe 9:1");
	// messages.add("Neltner 10:1");
	// messages.add("Franke 10:1");
	// messages.add("Nehiesel 10:1");
	// messages.add("Rosberg 10:1");
	// messages.add("Isch 12:1");
	// messages.add("J. Meder 12:1");
	// messages.add("D. Johnson 12:1");
	// messages.add("Arlinghaus 12:1");
	// messages.add("Reis 12:1");
	// messages.add("Crawford 14:1");
	// messages.add("Bohl 14:1");
	// messages.add("K. Johnson 14:1");
	// messages.add("Wolf 14:1");
	// messages.add("R. Meder 15:1");
	// messages.add("Stellman 15:1");
	// messages.add("Dieball 15:1");
	// messages.add("Wagner 16:1");
	// messages.add("Trentman 22:1");
}
