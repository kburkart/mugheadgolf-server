package com.mugheadgolf.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mugheadgolf.model.Foodorder;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Schedule;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.SettingsRepository;
import com.twilio.Twilio;
import com.twilio.type.PhoneNumber;



public class Communicate
{

	private static final Logger Logger = LoggerFactory.getLogger(Communicate.class);

	public static final String ACCOUNT_SID = "ACe2cf1b09560ce816d142ec0152665ebe";
	public static final String AUTH_TOKEN = "0b1a11bfa7202bfbcc906ea98f914182";
	public static final String MUGHEAD_TWILIO_PH_NUMBER = "+18598881697";
	
	public static void main(String[] args) {
		Golfer golfer = new Golfer();
		golfer.setPhone("5136416658");
		golfer.setFirstname("Kevin");
		golfer.setLastname("Burkart");
		String message = "This is a message for " + golfer.getFirstname() + " " + golfer.getLastname();
		sendSMS(golfer,message);
	}

	public static void sendSMS(Golfer golfer, String content) {
	 Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

	 
	 com.twilio.rest.api.v2010.account.Message message = com.twilio.rest.api.v2010.account.Message.creator(new PhoneNumber("+1" + golfer.getPhone()),
	        new PhoneNumber(MUGHEAD_TWILIO_PH_NUMBER), 
	        content).create();

	    Logger.debug("TWILIO SMS MESSAGE SID is " + message.getSid());
	}
	    
	public static void sendFoodOrder(Foodorder order, String kitchenPhone, String kitchenEmail) {
			 Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
			 
			 Golfer golfer = order.getIdgolfer();
			 
			 sendOrderEmail(kitchenEmail, order);

			 com.twilio.rest.api.v2010.account.Message message = com.twilio.rest.api.v2010.account.Message.creator(new PhoneNumber("+1" + kitchenPhone),
				        new PhoneNumber(MUGHEAD_TWILIO_PH_NUMBER), 
				        order.getDescription()).create();

				    Logger.debug("TWILIO SMS MESSAGE SID is " + message.getSid());

			 
			  message = com.twilio.rest.api.v2010.account.Message.creator(new PhoneNumber("+1" + golfer.getPhone()),
			        new PhoneNumber(MUGHEAD_TWILIO_PH_NUMBER), 
			        order.getDescription()).create();

			    Logger.debug("TWILIO SMS MESSAGE SID is " + message.getSid());

	}
 
	public static void sendAbsenteeNotice(Schedule schedule	){
		Golfer absentee = null;
		Golfer recipient = null;
		if(schedule.isAbsent1()) {
			absentee = schedule.getIdgolfer1();
			recipient = schedule.getIdgolfer2();
		}
		else {
			absentee = schedule.getIdgolfer2();
			recipient = schedule.getIdgolfer1();		
		}
		
		String message = absentee.getFirstname() + " " + absentee.getLastname() + " is unable to make it, week " + schedule.getIdweek() + ", and will be absent. Check the website for other orphan golfers and join up with them when you get to the course.";
		String subject = "Opponent Not Available";
		List<String> to = new ArrayList<>();
		to.add(recipient.getEmail());
		sendEmail(to, subject, message, false);
		sendSMS(recipient, message);
	}
	
	public static void sendConfirmationEmail(String golferEmail, String token){
      final String username = "mugheadgolf@gmail.com";
		final String password = "dhmzmijttpxlwpgi";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
                        @Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("from-email@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(golferEmail));
			message.setSubject("Mughead Golf Confirmation");
			message.setText("Please go to this url to confirm your email:,"
				+ "\n\n https://www.mugheadgolf.com/authenticate/" + token);

			Transport.send(message);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
   }

	public static void sendOrderEmail(String kitchenEmail, Foodorder order){
	      final String username = "mugheadgolf@gmail.com";
			final String password = "dhmzmijttpxlwpgi";

			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
	                        @Override
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });

			try {

				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress("from-email@gmail.com"));
				message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(kitchenEmail));
				message.setSubject("Mughead Golf Food Order #" + order.getIdorder());
				message.setText(order.getDescription());

				Transport.send(message);

			} catch (MessagingException e) {
				throw new RuntimeException(e);
			}
	   }

 public static boolean sendEmail(List<String> golfers, String subject, String text, boolean isHTML){
	 boolean result = true;
     final String username = "mugheadgolf@gmail.com";
		final String password = "dhmzmijttpxlwpgi";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
                       @Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

			InternetAddress[] sendTo = new InternetAddress[golfers.size()];
			for(int x = 0; x < golfers.size(); x++) {
				sendTo[x] =	new InternetAddress(golfers.get(x));
 
			}
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress("from-email@gmail.com"));
			msg.setRecipients(Message.RecipientType.TO, sendTo);
			msg.setSubject(subject);
			if(isHTML) {
				msg.setContent(text, "text/html; charset=utf-8");
			}
			else {
				msg.setText(text);
			}

			Transport.send(msg);

			Logger.debug("Email Sent");

		} catch (MessagingException e) {
			Logger.debug("Email Not Sent");
			result = false;
		}
		return result;
  }

}
