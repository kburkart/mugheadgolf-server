package com.mugheadgolf.tests;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.repository.GolferRepository;
import com.mugheadgolf.service.GolferServiceImpl;

public class GolferServiceImplTest {

	@InjectMocks
	private GolferServiceImpl gs;
	
	@Mock
	private GolferRepository golferRepo;
	
	@BeforeAll
	public void init() {
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testLogin() {
		
		Golfer mockup = new Golfer();
		mockup.setIdgolfer(1);
		when(golferRepo.findFirstByUsernameAndPassword("kburkart", "xxxxxxx")).thenReturn(mockup);
		Golfer golfer = gs.login("kburkart", "xxxxxxx");
		
		verify(golferRepo).findFirstByUsernameAndPassword("kburkart", "xxxxxxx");
		//assertEquals(1, settings.getIdsettings().intValue());
		assertThat(golfer.getIdgolfer(), is(1));
	}
	
	

	@Test
	public void testConfirmEmail() {
		Golfer mockup = new Golfer();
		String token = "token";
		when(golferRepo.findFirstByToken(token)).thenReturn(mockup);
		when(golferRepo.saveAndFlush(mockup)).thenReturn(mockup);
		gs.confirmEmail(token);
		
		verify(golferRepo).findFirstByToken(token);
		verify(golferRepo).saveAndFlush(mockup);
		//assertEquals(1, settings.getIdsettings().intValue());
	}

	@Test
	public void testRegister() {
		
	}

	@Test
	public void testGenerateToken1() {
		String token = gs.generateToken(20);
		assertThat(token.length(), is(20));
	}

	@Test
	public void testGenerateToken2() {
		String token = gs.generateToken(16);
		assertThat(token.length(), is(16));
	}

}
