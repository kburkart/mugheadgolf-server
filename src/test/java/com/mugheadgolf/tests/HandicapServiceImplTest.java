package com.mugheadgolf.tests;

import com.mugheadgolf.model.*;
import com.mugheadgolf.repository.*;
import com.mugheadgolf.service.HandicapServiceImpl;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class HandicapServiceImplTest {

	@InjectMocks
	private HandicapServiceImpl hs;
	
	@Mock
	private GolferRepository golferRepository;
	
	@Mock
	private ScoreRepository scoreRepository;
	
	@Mock
	private SettingsRepository settingsRepository;

	@Mock
	private ScheduleRepository scheduleRepository;

	@Mock
	private ScoreholeRepository scoreholeRepository;

	@Mock
	private CourseholesRepository courseholesRepository;

	@Mock
	private HandicapRepository handicapRepository;

	@BeforeAll
	public void init() {
		MockitoAnnotations.initMocks(this);
	}


	
	@Test
	public void testCalculateHandicap() {
		int year = 2018;
		List<Score> scores = getScores11();
		Settings settings = getSettings();
		Handicap handicap = new Handicap();
		Course course = getCourse();
		Golfer golfer = scores.get(0).getIdgolfer();
		List<Schedule> schedules = getSchedules();
		Schedule schedule = scores.get(0).getIdschedule(); 
		List<Courseholes> courseholes = getCourseholes();
		List<Scorehole> scoreholes = getScoreholes();
		//when(scoreRepository.findAllByIdgolferAndUseforhandicap(golfer, 1)).thenReturn(scores);
		when(settingsRepository.findFirstByYear(year)).thenReturn(settings);
		when(handicapRepository.findFirstByIdweekAndIdgolferAndYear(scores.get(0).getIdschedule().getIdweek() + 1, golfer, year)).thenReturn(null);
		when(courseholesRepository.findAllByIdcourseOrderByHole(course)).thenReturn(courseholes);
		when(scoreholeRepository.findAllByIdscoreOrderByHole(scores.get(0))).thenReturn(scoreholes);
		when(scoreRepository.save(scores.get(0))).thenReturn(scores.get(0));
		when(handicapRepository.save(handicap)).thenReturn(handicap);
		when(scheduleRepository.save(schedule)).thenReturn(schedule);
		when(scheduleRepository.getGolferSchedule(year, golfer)).thenReturn(schedules);
		when(golferRepository.save(golfer)).thenReturn(golfer);
		when(scoreRepository.getHandicapScores(golfer, 1)).thenReturn(scores);
		BigDecimal expectedHC = new BigDecimal("5");
		BigDecimal hc = hs.calculateHandicap(golfer,scores);
		//verify(scoreRepository).findAllByIdgolferAndUseforhandicap(golfer, 1);
		verify(settingsRepository).findFirstByYear(year);
		verify(courseholesRepository).findAllByIdcourseOrderByHole(course);
		//verify(handicapRepository).findFirstByIdweekAndIdgolfer(scores.get(0).getIdschedule().getIdweek() + 1, golfer);
		verify(scoreholeRepository).findAllByIdscoreOrderByHole(scores.get(0));
		verify(scoreRepository).save(scores.get(0));
//		verify(scheduleRepository).save(schedule);
		verify(golferRepository).save(golfer);
//		verify(scoreRepository.getHandicapScores(year, golfer, 1, scores.get(0).getIdschedule().getIdweek()));
		assertEquals(expectedHC,hc);

	}

	@SuppressWarnings("deprecation")
	@Test
	public void testGethandicapdifferential18() {

		int slope = 125;
		BigDecimal rating = new BigDecimal(71.5);
		int adjustedScore = 95;
		BigDecimal excpectedHC = new BigDecimal("21.2");
		BigDecimal hcDiff = hs.gethandicapdifferential(slope, rating, adjustedScore, false);
		assertEquals(excpectedHC, hcDiff);

		//assetThat(hcDiff, is(new BigDecimal(21.2)));
	
	}

	@Test
	public void testGethandicapdifferential9() {

		int slope = 125;
		BigDecimal rating = new BigDecimal(71.5);
		int adjustedScore = 47;
		BigDecimal excpectedHC = new BigDecimal("10.2");
		BigDecimal hcDiff = hs.gethandicapdifferential(slope, rating, adjustedScore, true);
		assertEquals(excpectedHC, hcDiff);
		//assetThat(hcDiff, is(10.2));
	
	}

	@Test
	public void testGetAvgHCDifferentials1() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("4.9");
		List<Score> scores = new ArrayList<>();
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);
		assertEquals(expected, hcIndex);

		
	}

	@Test
	public void testGetAvgHCDifferentials2() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("5.2");
		List<Score> scores = new ArrayList<>();
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);
		assertEquals(expected, hcIndex);
		
	}

	@Test
	public void testGetAvgHCDifferentials4() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("5.3");
		List<Score> scores = new ArrayList<>();

		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.7")));
		
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);
		assertEquals(expected, hcIndex);
		
	}

	@Test
	public void testGetAvgHCDifferentials7() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("5.2");
		List<Score> scores = new ArrayList<>();

		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.9")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.4")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.7")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);
		assertEquals(expected, hcIndex);
		
	}

	@Test
	public void testGetAvgHCDifferentials9() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("5.1");
		List<Score> scores = new ArrayList<>();
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.7")));
		
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.9")));
		
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.1")));
		
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.2")));
		
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.4")));
		
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);
		assertEquals(expected, hcIndex);
		
	}

	@Test
	public void testGetAvgHCDifferentials11() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("5.3");
		List<Score> scores = getScores11();
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);	
		assertEquals(expected, hcIndex);
		
	}

	@Test
	public void testGetAvgHCDifferentials13() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("5.3");
		List<Score> scores = getScores13();
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);
		
		assertEquals(expected, hcIndex);
		
	}

	@Test
	public void testGetAvgHCDifferentials15() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("5.3");
		List<Score> scores = getScores15();
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);
		assertEquals(expected, hcIndex);
		
	}

	@Test
	public void testGetAvgHCDifferentials16() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("5.2");
		List<Score> scores = getScores16();
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);
		
		assertEquals(expected, hcIndex);
		
	}
	@Test
	public void testGetAvgHCDifferentials17() {
		Settings setting = getSettings();
		BigDecimal expected = new BigDecimal("5.2");
		List<Score> scores = getScores17();
		BigDecimal hcIndex = hs.getAvgHCDifferentials(setting, scores);
		
		assertEquals(expected, hcIndex);
		
	}

	@Test
	public void testAdjustScore() {
		List<Courseholes> holes = getCourseholes();
		List<Scorehole> scores = getScoreholes();
		Settings setting = getSettings();
		int expected = 46;
		int adjusetedScore = hs.adjustScore(setting, holes, scores, 3, false);
		assertEquals(expected, adjusetedScore);
	}
	

	@Test
	public void testAdjustScore2() {
		List<Courseholes> holes = getCourseholes();
		List<Scorehole> scores = getScoreholes();
		Settings setting = getSettings();
		int expected = 49;
		int adjusetedScore = hs.adjustScore(setting, holes, scores, 6, false);
		assertEquals(expected, adjusetedScore);
	}
	
	@Test
	public void testAdjustScore3() {
		List<Courseholes> holes = getCourseholes();
		List<Scorehole> scores = getScoreholes();
		Settings setting = getSettings();
		int expected = 50;
		int adjusetedScore = hs.adjustScore(setting, holes, scores, 13, false);
		assertEquals(expected, adjusetedScore);
	}

	@Test
	public void testAdjustScore4() {
		List<Courseholes> holes = getCourseholes();
		List<Scorehole> scores = getScoreholes();
		Settings setting = getSettings();
		int expected = 50;
		int adjusetedScore = hs.adjustScore(setting, holes, scores, 18, false);
		assertEquals(expected, adjusetedScore);
	}

	@Test
	public void testAdjustScore5() {
		List<Courseholes> holes = getCourseholes();
		List<Scorehole> scores = getScoreholes();
		Settings setting = getSettings();
		int expected = 50;
		int adjusetedScore = hs.adjustScore(setting, holes, scores, 21, false);
		assertEquals(expected, adjusetedScore);
	}

	@Test
	public void testAdjustScore6() {
		List<Courseholes> holes = getCourseholes();
		List<Scorehole> scores = getScoreholes();
		Settings setting = getSettings();
		int expected = 50;
		int adjusetedScore = hs.adjustScore(setting, holes, scores, 18, true);
		assertEquals(expected, adjusetedScore);
	}

	@Test
	public void testAdjustScore7() {
		List<Courseholes> holes = getCourseholes();
		List<Scorehole> scores = getScoreholes();
		Settings setting = getSettings();
		setting.setMaxallowedhc(8);
		setting.setUseghin(0);
		int expected = 50;
		int adjusetedScore = hs.adjustScore(setting, holes, scores, 18, true);
		assertEquals(expected, adjusetedScore);
	}

	public Score populateTestScoreWithHcDiff(BigDecimal hcDiff) {
		Score score = new Score();
		Schedule schedule = new Schedule();
		schedule.setIdcourse(getCourse());
		schedule.setBacknine(0);
		schedule.setIdweek(1);
		schedule.setYear(2018);
		schedule.setIdgolfer1(getGolfer());
		schedule.setHandicap1(new BigDecimal(5.0));
		score.setIdschedule(schedule);
		score.setIdgolfer(getGolfer());
		score.setScore(50);
		score.setHandicapdifferential(hcDiff);
		return score;
	}

	public List<Schedule> getSchedules(){
		List<Schedule> schedules = new ArrayList<>();
		Schedule schedule = new Schedule();
		schedule.setIdcourse(getCourse());
		schedule.setBacknine(0);
		schedule.setIdweek(1);
		schedule.setIdgolfer1(getGolfer());
		schedule.setHandicap1(new BigDecimal(5.0));
		schedules.add(schedule);
		schedule = new Schedule();
		schedule.setIdcourse(getCourse());
		schedule.setBacknine(0);
		schedule.setIdweek(1);
		schedule.setIdgolfer1(getGolfer());
		schedule.setHandicap1(new BigDecimal(5.0));
		schedules.add(schedule);
		return schedules;
	}
	
	public List<Courseholes> getCourseholes() {
		List<Courseholes> holes = new ArrayList<>();
		Courseholes hole = new Courseholes();
		hole.setHandicap(5);
		hole.setPar(4);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(4);
		hole.setPar(4);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(3);
		hole.setPar(5);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(7);
		hole.setPar(3);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(2);
		hole.setPar(4);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(1);
		hole.setPar(5);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(6);
		hole.setPar(3);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(9);
		hole.setPar(4);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(8);
		hole.setPar(4);
		holes.add(hole);
		hole = new Courseholes();
		hole.setHandicap(5);
		hole.setPar(4);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(8);
		hole.setPar(4);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(7);
		hole.setPar(3);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(3);
		hole.setPar(4);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(2);
		hole.setPar(5);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(6);
		hole.setPar(4);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(9);
		hole.setPar(3);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(4);
		hole.setPar(4);
		holes.add(hole);
        hole = new Courseholes();
		hole.setHandicap(1);
		hole.setPar(5);
		holes.add(hole);
		
		return holes;
	}

	public List<Scorehole> getScoreholes() {
		List<Scorehole> scores = new ArrayList<>();
		Scorehole score = new Scorehole();
		score.setHole(1);
		score.setScore(6);
		scores.add(score);
		score = new Scorehole();
		score.setHole(2);
		score.setScore(4);
		scores.add(score);
		score = new Scorehole();
		score.setHole(3);
		score.setScore(8);
		scores.add(score);
		score = new Scorehole();
		score.setHole(4);
		score.setScore(7);
		scores.add(score);		
		score = new Scorehole();
		score.setHole(5);
		score.setScore(7);
		scores.add(score);		
		score = new Scorehole();
		score.setHole(6);
		score.setScore(4);
		scores.add(score);
		score = new Scorehole();
		score.setHole(7);
		score.setScore(4);
		scores.add(score);
		score = new Scorehole();
		score.setHole(8);
		score.setScore(5);
		scores.add(score);
		score = new Scorehole();
		score.setHole(9);
		score.setScore(5);
		scores.add(score);
		return scores;
	}

	public Settings getSettings() {
		Settings setting = new Settings();
		setting.setUseghin(1);
		setting.setMaxallowedhc(2);
		setting.setMaxallowedpar3(6);
		setting.setMaxallowedpar4(8);
		setting.setMaxallowedpar5(10);
		setting.setPercentage(new BigDecimal(".96"));
		return setting;
	}
	
	public Course getCourse() {
		Course course = new Course();
		course.setRating(new BigDecimal("71.5"));
		course.setSlope(125);
		return course;
	}
	
	public Score getScore() {
		Score score = new Score();
		Schedule schedule = new Schedule();
		schedule.setIdcourse(getCourse());
		schedule.setBacknine(0);
		schedule.setIdweek(1);
		score.setIdschedule(schedule);
		score.setIdgolfer(getGolfer());
		score.setScore(50);
		
		return score;
	}
	
	public Golfer getGolfer() {
		Golfer golfer = new Golfer();
			golfer.setIdgolfer(1);
			golfer.setCurrenthandicap(5);
		return golfer;
	}
	
	public List<Score> getScores11() {
		List<Score> scores = new ArrayList<>();

		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.2")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		return scores;
	}
	
	public List<Score> getScores13() {
		List<Score> scores = new ArrayList<>();

		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.2")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		

		return scores;
	}
	
	public List<Score> getScores15() {
		List<Score> scores = new ArrayList<>();

		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.2")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		

		return scores;
	}

	public List<Score> getScores16() {
		List<Score> scores = new ArrayList<>();
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.2")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.2")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		return scores;
	}

	public List<Score> getScores17() {
		List<Score> scores = new ArrayList<>();

		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.2")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.3")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.5")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.8")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.6")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.2")));
		scores.add(populateTestScoreWithHcDiff(new BigDecimal("5.2")));
		

		return scores;
	}

}
