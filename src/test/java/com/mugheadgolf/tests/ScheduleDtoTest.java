package com.mugheadgolf.tests;

import com.mugheadgolf.dto.ScheduleDto;
import com.mugheadgolf.model.Course;
import com.mugheadgolf.model.Golfer;
import com.mugheadgolf.model.Schedule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("local")
public class ScheduleDtoTest {

    private static final ModelMapper modelMapper = new ModelMapper();

    @Test
    public void checkScheduleMapping() {
        Schedule schedule = new Schedule();
        Course course = new Course();
        Golfer golfer1 = new Golfer();
        Golfer golfer2 = new Golfer();
        schedule.setIdschedule(21);
        schedule.setYear(2021);
        schedule.setIdweek(1);
        schedule.setAbsent1(false);
        schedule.setAbsent2(false);
        schedule.setActive(1);
        schedule.setBacknine(0);
        course.setCourseName("Carnoustie");
        schedule.setIdcourse(course);
        golfer1.setFirstname("Jack");
        golfer2.setFirstname("Tiger");
        schedule.setHandicap1(new BigDecimal(5));
        schedule.setHandicap2(new BigDecimal(4));
        schedule.setPoints1(new BigDecimal(7.5));
        schedule.setPoints2(new BigDecimal(2.5));
        golfer1.setLastname("Nickluas");
        golfer2.setLastname("Woods");
        schedule.setIdgolfer1(golfer1);
        schedule.setIdgolfer2(golfer2);

        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        ScheduleDto dto = modelMapper.map(schedule, ScheduleDto.class);
        assertEquals(dto.getActive(), schedule.getActive());
        assertEquals(dto.getBacknine(), schedule.getBacknine());
        assertEquals(dto.getCourseName(), schedule.getIdcourse().getCourseName());
        assertEquals(dto.getFirstname1(), schedule.getIdgolfer1().getFirstname());
        assertEquals(dto.getLastname1(), schedule.getIdgolfer1().getLastname());
        assertEquals(dto.getHandicap1(), schedule.getHandicap1());
        assertEquals(dto.getHandicap2(), schedule.getHandicap2());
        assertEquals(dto.getIdschedule(), schedule.getIdschedule());
        assertEquals(dto.getIdweek(), schedule.getIdweek());
        assertEquals(dto.getFirstname2(), schedule.getIdgolfer2().getFirstname());
        assertEquals(dto.getLastname2(), schedule.getIdgolfer2().getLastname());
        assertEquals(dto.getYear(), schedule.getYear());
        assertEquals(dto.getPoints1(), schedule.getPoints1());
        assertEquals(dto.getPoints2(), schedule.getPoints2());
        assertEquals(dto.isAbsent1(), schedule.isAbsent1());
        assertEquals(dto.isAbsent2(), schedule.isAbsent2());


    }
}
