package com.mugheadgolf.tests;



import static org.hamcrest.CoreMatchers.is;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.mugheadgolf.controller.SettingsController;
import com.mugheadgolf.model.Settings;
import com.mugheadgolf.repository.SettingsRepository;

public class SettingsControllerTest {
	
	@InjectMocks
	private SettingsController sc;
	
	@Mock
	private SettingsRepository sr;
	
	@BeforeAll
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetSettings() {
		Settings mockup = new Settings();
		mockup.setIdsettings(1);
		when(sr.findFirstByYear(2018)).thenReturn(mockup);
		Settings settings = sc.getSettings(2018);
		
		verify(sr).findFirstByYear(2018);
		//assertEquals(1, settings.getIdsettings().intValue());
		assertThat(settings.getIdsettings(), is(1));
	}

}
